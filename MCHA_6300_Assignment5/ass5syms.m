clc;
clear;
close all;

m = 0.742453953067092;
l= 0.010479832883254;
fc = 0.000000000546431;
fp = 0.000180435058525;
Jp = 0.002845044685282;
c1 = 12.942119630860724;
c2 = -1.999999214754792;
param.g = 9.81;

param.a1 = Jp/(m*l);
param.a2 = 1/l;
param.b = Jp/(m*l^2);
param.k1 = c1/(m*l);
param.k2 = (fc - c2)/(m*l);
param.k3 = fp/(m*l);
param.d = 1/40;

h = param.d;
func = @(t,x,u,param) pendModel(t,x,u,param);

N = 100;
tspan = [0 param.d];
x0 = [0; 90*pi/180; 0; 0];
C = eye(4);
X = zeros(N,4);
y = X;

%% MPC
M = 60; %Horizon
u_h = zeros(M,1);
ud = u_h;
x_h = x0;

    %% SQP
    
    %Q and R
    Q = diag([1 10 1e-4 1e-4]);
    R = 0.1;
        
    %initial state and input guess
    xd = [-3;2;2;-2;-2]/10;
    vd = zeros(3,1);

    x = sym('x',[4,1]);
    v = sym('v',[4,1]);
        
    %lagrange multiplier
    lamda = diag(Q);
  
          

    %%
    Lb = -0.7*ones(4,1);
    Ub = 0.7*ones(4,1);

    %cost function to minimise
    %Add loop over horizon for optimised u vector?
    V = x'*Q*x + u'*R*u;

    % active constraints
    c = [x(1) + 0.7;
         x(1) - 0.7;
         u(1) - 0.5;
         u(1) + 0.5];

    disp = 0;
    %lagrangian function
    L = V + sum(lamda.*c);

    dV = gradient(V,x);
    
    grad = gradient(L,x);
    H = hessian(L,x);
    J_c = jacobian(c,x);

    dA = double(subs(J_c,x,xd(:,1)));
    gradV = double(subs(dV,x,xd(:,1)));
    V = double(subs(V,x,xd(:,1)));
    %---> min 0.5*p'*Lxx*p + p'*Lx
    % s.t. c(x) + p'*c_x(x)
    % where Lxx is the hessian of L and Lx is the gradient of L

    %Need L_i and k_i inequality constraints
    L_i = [eye(length(Ub)); 
          -eye(length(Lb))];
    k_i = [Ub;-Lb];

    for i = 1:M
    %Quadprog minimises 0.5*x'*H*x + f*x, ooo that looks familiar now eh?
    %find the 'search direction' p_start and lagrange multipliers
    [p_star, lm, ~] = quadProgIP(H,grad,L_i,k_i,[],[],disp);

    %merit function required instead of slack varible here for something
    %MERIT FUNCTION 
      %Find alpha
      alp = 1;
      del = 0.001;
      lam = max(abs(vd + p(6:end)));
      mu = lam + del;
      mo = double(subs(V,x,xd)) + (mu*sum(abs(double(subs(c,x,xd)))));
      for k=1:50
        xn = xd + alp*p_star(1:5);
        mn = double(subs(V,x,xn)) + mu*(sum(abs(double(subs(c,x,xn)))));
        if mn < mo + 0.01*alp*(gradf.'*p_star(1:5) - (mu*sum(abs(double(subs(c,x,xd))))))
          break;
        end
        alp = alp/2;
      end

      %Compute new iterates
      ud = ud + alp*p_star;
      lam = lam + alp*p_star;
    end
