clear;
close all;
clc;
clear;

%starting point
xd = [-1.4;1.3;1.8;-0.8;-0.8];
vd = zeros(3,1);

x = sym('x', [5,1]);
v = sym('v', [3,1]);

f = exp(prod(x)) - 0.5*(x(1)^3 + x(2)^3 +1)^2; %minimise this function

%active constraints
c = [x(1)^2 + x(2)^2 + x(3)^2 + x(4)^2 + x(5)^2 - 10; 
    x(2)*x(3) - 5*x(4)*x(5);
    x(1)^3 + x(2)^3 +1];

%lagrange
L = f + sum(v.*c);

df = gradient(f,x);
dL = gradient(L,x);
d2L = hessian(L,x);
A = jacobian(c,x);

dA = double(subs(A,x,xd));
gradf = double(subs(df,x,xd));

for i = 1:100
    %compute search direction
   gradL = double(subs(dL,[x,v],[xd,vd]))
   
   HessL = double(subs(subs(d2L,x,xd),v,vd));
  
   %Null matrix for dA
   Z = null(dA);
   
   if norm(gradL) < 1e-8 && sum(abs(double(subs(c,x,xd)))) < 1e-10 && min(eig(Z'*HessL*Z)) > - eps
       break;
   end
   
   %newton direction
   p = [HessL dA'; dA zeros(3)]\[-gradL; -double(subs(c,x,xd))];
   
   %Find alpha
   alp = 1;
   
   %Compute new iterates
   xd = xd + alp*p(1:5)
   vd = vd + alp*p(6:end)
   
   gradf = double(subs(df,x,xd));
   dA =  double(subs(A,x,xd));
end
