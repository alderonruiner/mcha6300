function dx = pendModel(t,x,u,param)
% x  = [p, theta, v, omega]'
w1 = param.k1*u - x(4)^2*sin(x(2)) - param.k2*x(3);
w2 = param.g*sin(x(2)) - param.k3*x(4);
d = param.b - (cos(x(2)))^2;

dx = zeros(4,1);

dx(1) = x(3);
dx(2) = x(4);
dx(3) = (param.a1*w1 + w2*cos(x(2)))/d;
dx(4) = (w1*cos(x(2)) + param.a2*w2)/d;
