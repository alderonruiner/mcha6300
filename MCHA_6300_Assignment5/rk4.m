function dx = rk4(t,x,u,h,param)

F1 = h*pendModel(t,x,u,param);
F2 = h*pendModel((t + 0.5*h),(x + 0.5*F1), u, param);
F3 = h*pendModel((t + 0.5*h) ,(x + 0.5*F2), u, param);
F4 = h*pendModel((t + h),(x + F3), u, param);

dx = x + (F1 + 2*F2 + 2*F3 + F4)/6;
