import numpy as np
import matplotlib.pyplot as plt
import math as mt

def pendModel(t,x,u):
    a1 = Jp/(m*l);
    a2 = 1/l;
    b = Jp/(m*l**2);
    k1 = c1/(m*l);
    k2 = (fc - c2)/(m*l);
    k3 = fp/(m*l);
    g = 9.81;

    w1 = k1*u - x[3]**2*np.sin(x[1]) - k2*x[2];
    w2 = g*np.sin(x[1]) - k3*x[3];
    d = b - (np.cos(x[1]))**2;
    
    dx = np.zeros((4,1));
    
    dx[0] = x[2];
    dx[1] = x[3];
    dx[2] = (a1*w1 + w2*np.cos(x[1]))/d;
    dx[3] = (w1*np.cos(x[2]) + a2*w2)/d;

    return dx

def rk4(t,x,u,h):

    F1 = h*pendModel(t,x,u);
    F2 = h*pendModel((t + 0.5*h),(x + 0.5*F1), u);
    F3 = h*pendModel((t + 0.5*h) ,(x + 0.5*F2), u);
    F4 = h*pendModel((t + h),(x + F3), u);
    
    dx = x + (F1 + 2*F2 + 2*F3 + F4)/6;
    
    return dx


#%% 

m = 0.742453953067092
l= 0.010479832883254
fc = 0.000000000546431
fp = 0.000180435058525
Jp = 0.002845044685282
c1 = 12.942119630860724
c2 = -1.999999214754792


h = 1/40
u = 0.0
x = np.array([[0],[mt.pi/180],[0],[0]])
X = np.array([])
N = 5
T = np.array([0])

for i in range(N):
    t = float(i)
    x = X[i]
    T[i] = np.append(t)
    X[i] = np.append(rk4(t,x,u,h))

plt.figure(1)
plt.plot(T,X[1])
plt.show()

#%% 
a = 0.0
a = np.expand_dims(a,1)
for i in range(5):
    a = np.append(a,np.matrix([[i]]), axis=0)
print(a)