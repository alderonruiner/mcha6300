function [u, param] = invertPendControlelse(yn,param)
% OUTPUTS:
% u - a vector containing the control actions over the prediction
% horizon.the input u(1) denotes the next control action to be applied at
% time t*d
% param - structure containing extra variables added for the purpose of
% calculating the control action at the next time sample
%https://www.youtube.com/watch?v=AubJS7oWaWo

% INPUTS:
% y - vector containing the current measurement of cart position (y(1) in
% metres) and pendulum angle (y(2) in  radians)
% param.t - the current time of the simulation corresponding to
% measurements y
% param.d - the delta amount of time until the next measurement will be
% available. The input being calculated by your routine will be applied at
% t+d (next sample time)

% M.x1 = param.x_old;
% M.P1 = param.Q;
% M.R = param.R;
% M.A = nan;
% M.B = nan;
% M.C = eye(4);
% M.D = 0;
% 
% Z.u = nan;
% Z.y = nan;
% Z.N = NaN;
% 
% G = EKF(Z,M);
% 
% xp = G.xp;
% xf = G.xf;
% param.xp = xp;

% Unpack variables
M = param.M;
Q = param.Q;
R = param.R;
h = param.d;
t = param.t;
u = 0;

disp = 0;
% Initialisation
c1 = 0.01; % wolfe condition param

%MPC Horizon thing?
dx = rk4(t,yn,u,h,param);
param.xh(:,1) = dx;
% e = zeros(5*M,1);

for t = 1:M
    dx = rk4(t,param.xh(:,t),u,h,param);
    param.xh(:,t+1) = dx;
    U(t) = 0;
%    e(t:t+4,:)= param.xh(:,t+1);
end

% for i = 1:M
%     e1(:,i) = sqrt(Q)*param.xh(:,i+1);
%     e2(:,i) = sqrt(R)*U(i);
% end
% e = [reshape(e1,[4*M,1]);
%      e2'];

xhh = param.xh;


% Cost restraints
xd = zeros(5,1); %[x1;x2;x3;x4;u]
vd = zeros(4,1); %same length as v sym

% Use symbolic toolbox... (params to be optimised)
x = sym('x',[5,1]);
v = sym('v',[4,1]); %same length as active constraints

% active constraints
c = [x(1) + 0.7;
     x(1) - 0.7;
     x(5) - 0.5;
     x(5) + 0.5];
         
% Create cost function, and gradient of function
zz = [sqrt(Q) zeros(length(Q),length(R));
      zeros(length(R),length(Q)) sqrt(R)];
 
e = x;

% V_cost = e'*e;
V_cost = e'*zz*e;
% V_cost = x'*Q*x + u'*R*u;

dV = gradient(V_cost,x); %gradient of cost

% Lagrangian, lagrangian gradient and lagrangian hessian
L = V_cost + sum(v.*c);
dL = gradient(L,x);
H = hessian(L,x);

% Jacobian of constraints
J_c = jacobian(c,x);

% Sub values in to J_c and dV
gradV = double(subs(dV,x,[yn(:,1);0]));
dJ  = double(subs(J_c,x,[yn(:,1);0])); %double J

V = double(subs(V_cost,x,yn(:,1)));

% Lb = -0.7*ones(5,1);
% Ub = 0.7*ones(5,1);
% 
% L_i = [eye(length(Ub)); 
%        -eye(length(Lb))];
% k_i = [Ub;-Lb];

L_i = [1 0 0 0 0;
       -1 0 0 0 0;
       0 0 0 0 1;
       0 0 0 0 -1];

k_i = [0.7;0.7;0.5;0.5];

% For loop for event horizon
for i = 1:M
    xx(:,i) = xd;
    gradL = double(subs(dL,[x;v],[[xhh(:,i+1); U(:,i)];vd]));
    HL = double(subs(subs(H,x,[xhh(:,i+1); U(:,i)]),v,vd));%double(subs(H,[x;u],yn(:,i)));
    Z = null(dJ);
    
    %first exit condition
    if norm(gradL)<1e-8  &&  sum(abs(double(subs(c,x,[xhh(:,i+1); U(:,i)]))))<1e-10 && min(eig(Z'*HessL*Z))>-eps
        break;
    end
  
    % Sum  of cost function
    V = V + double(subs(V_cost,[x;u],yn(:,i)));

    % Call QP to optimise cost function and find search direction if exit
    % condition not met
   [p_star, lm, ~] = quadProgIP(HL, gradL, L_i, k_i, [], [], disp);
    
   %Lagrange multipliers
    u_k = lm.equality;  
    v_k = lm.inequality;

    %p = [p_star; u_k; v_k];
    p_lm = [u_k; v_k];

   
    % Find Alpha
    alpha = 1;
    delta = param.d; % Small step delta

    lam = max(abs(vd + p_lm)); % Lagrange multiplier (max inf norm)
    mu = lam + delta;
    merit_old = double(subs(V,x,xd)) + (mu*sum(abs(double(subs(c,x,xd))))); % Old merit function

    %back tracking linesearch
    for k = 1:50
        xn = xd + alpha*p_star;
        merit_new = double(subs(V,x,xn)) + (mu*sum(abs(double(subs(c,x,xn))))); % New merit function

        if merit_new < merit_old + c1*alpha*(gradV.'*p_star - (mu*sum(abs(double(subs(c,x,xd)))))) % Check wolfe condition (gradient of merit funciton)
            break;
        end
        alpha = alpha/2;
    end

    % Compute new iterates
    xd = xd + alpha*p_star;
    vd = vd + alpha*p_lm;

    gradV = double(subs(dV,x,xd));
    dJ  = double(subs(J_c,x,[xhh(:,i+1); U(:,i)])); %double J

    i

end



u = xx(end,1:end);





