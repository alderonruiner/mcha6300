Q = diag([1 10 1e-4 1e-4]);
R = 0.1;

% Initialisation
c1 = 0.01; % wolfe condition param
disp = 0;

% Initial approximations
xd = zeros(5,1); %[x1;x2;x3;x4;u]
vd = zeros(4,1); %same length as v sym

% Use symbolic toolbox... (params to be optimised)
x = sym('x',[5,1]);
v = sym('v',[4,1]); %same length as active constraints

V_cost = x'*Q*x + u'*R*u;

% active constraints
c = [x(1) + 0.7;
     x(1) - 0.7;
     x(5) - 0.5;
     x(5) + 0.5];

%Lagrangian
L = V + sum(v.*c);

dV = gradient(V_cost,x); %gradient of cost

% Lagrangian, lagrangian gradient and lagrangian hessian
dL = gradient(L,x);
H = hessian(L,x);

% Jacobian of constraints
J_c = jacobian(c,x);

% Sub values in to J_c and dV
dJ  = double(subs(J_c,x,[yn(:,1);0])); %double J
gradV = double(subs(dV,x,[yn(:,1);0]));

%inequality constraits for QP
L_i = [1 0 0 0 0;
       -1 0 0 0 0;
       0 0 0 0 1;
       0 0 0 0 -1];

k_i = [0.7;0.7;0.5;0.5];


for i = 1:50
   
    %Recompute lagrange gradient and hessian
    gradL = double(subs(dL,[x;v],[xd;vd]));
    HL = double(subs(subs(H,x,xd),v,vd));%double(subs(H,[x;u],yn(:,i)));
    Z = null(dJ); %Null matrix for dJ
    
    %first exit condition
    if norm(gradL)<1e-8  &&  sum(abs(double(subs(c,x,xd))))<1e-10 && min(eig(Z'*HessL*Z))>-eps
        break;
    end
  
    % Call QP to optimise cost function and find search direction if exit
    % condition not met
   [p_star, lm, ~] = quadProgIP(HL, gradL, L_i, k_i, [], [], disp);
    
   %Unpack and repack langrange multipliers
   %Lagrange multipliers
    u_k = lm.equality;  
    v_k = lm.inequality;

    %p = [p_star; u_k; v_k];
    p_lm = [u_k; v_k];

    %MERIT FUNCTION
    % Find Alpha
    alpha = 1;
    delta = 0.001; % Small step delta
    lam = max(abs(vd + p_lm)); % Lagrange multiplier (max inf norm)
    mu = lam + delta;
    merit_old = double(subs(V,x,xd)) + (mu*sum(abs(double(subs(c,x,xd))))); % Old merit function

    %back tracking linesearch
    for k = 1:50
        xn = xd + alpha*p_star;
        merit_new = double(subs(V,x,xn)) + (mu*sum(abs(double(subs(c,x,xn))))); % New merit function
        if merit_new < merit_old + c1*alpha*(gradV.'*p_star - (mu*sum(abs(double(subs(c,x,xd)))))) % Check wolfe condition (gradient of merit funciton)
            break;
        end
        alpha = alpha/2;
    end
    alpha
    % Compute new iterates
    xd = xd + alpha*p_star;
    vd = vd + alpha*p_lm;

    gradV = double(subs(dV,x,xd));
    dJ  = double(subs(J_c,x,xd)); 

    i

end



u = xx(end,1:end);





