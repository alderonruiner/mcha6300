clc;
clear;

%% Define Parameters
param.m  = 0.742453953067092;
param.l  = 0.010479832883254;
param.fc = 0.000000000546431;
param.fp =  0.000180435058525;
param.Jp = 0.002845044685282;
param.c1 = 12.942119630860724;
param.c2 = -1.999999214754792;
param.g  = 9.81;
param.t  = 0;
param.d  = 1/40;
param.M  = 50; %horizon
param.Q  = diag([1 10 1e-4 1e-4]);
param.R  = 0.1;
param.a1 = param.Jp/(param.m*param.l);
param.a2 = 1/param.l;
param.b = param.Jp/(param.m*param.l^2);
param.k1 = param.c1/(param.m*param.l);
param.k2 = (param.fc - param.c2)/(param.m*param.l);
param.k3 = param.fp/(param.m*param.l);
param.d = 1/40;
param.xh = zeros(4,param.M);
param.uh = zeros(1,param.M);
param.x_old = [0;0;0;0];
param.u_old = 0;
param.h = param.d;
tspan = [0 param.d];

T = 100;
%% Control system for t timesteps
yn = [0;0];
x = param.x_old;
for t = 1:T
    [u(t),param] = invertPendControl(yn,param);
    param.x_old = x(:,t);
    %send to plant and get next states
    xref = [x(:,1);0 - x(:,2);x(:,3);x(:,4)];
    U = u(t);
    [T, dx] = ode45(@(t,x) pendModel(t,xref,U,param), tspan, param.x_old);
    x(:,t+1) = dx(length(dx),:)';
    t
end