clear;
close all;
clc;

%xOpt = [-1.71;1.59;1.82;-0.763;-0.763];
%xd = [-1.4;1.3;1.8;-0.8;-0.8];

xd = [-3;2;2;-2;-2]/10; %initial parameters guess
vd = zeros(3,1); %lagrange multipliers

x = sym('x',[5,1]);
v = sym('v',[3,1]);

f = exp(prod(x)) - 0.5*(x(1)^3 + x(2)^3 + 1)^2; %function to minimise

%active constraints
c = [sum(x .* x) - 10; 
     x(2)*x(3) - 5*x(4)*x(5) ; 
     x(1)^3 + x(2)^3 + 1];
 
 %lagrange
L = f + sum(v.*c);

df = gradient(f,x);
dL = gradient(L,x);
d2L = hessian(L,x);
A  = jacobian(c,x);

dA  = double(subs(A,x,xd)); %double A
gradf = double(subs(df,x,xd));

for i=1:100
  %Compute search direction
  gradL = double(subs(dL,[x;v],[xd;vd]))  %lagrangian gradient
  
  HessL = double(subs(subs(d2L,x,xd),v,vd));   %hessian of lagrangian
  
  %Null matrix for dA
  Z = null(dA); 
  
  if norm(gradL)<1e-8  &&  sum(abs(double(subs(c,x,xd))))<1e-10 && min(eig(Z'*HessL*Z))>-eps
    break;
  end
  
  
  %Newton direction
  %FIND THE SEARCH DIRECTION
  p = [HessL dA';dA zeros(3)]\[-gradL;-double(subs(c,x,xd))];  
  
  %MERIT FUNCTION
  %Find alpha
  alp = 1;
  del = 0.001;
  lam = max(abs(vd + p(6:end)));
  mu = lam + del;
  mo = double(subs(f,x,xd)) + (mu*sum(abs(double(subs(c,x,xd)))));
  for k=1:50
    xn = xd + alp*p(1:5);
    mn = double(subs(f,x,xn)) + mu*(sum(abs(double(subs(c,x,xn)))));
    if mn < mo + 0.01*alp*(gradf.'*p(1:5) - (mu*sum(abs(double(subs(c,x,xd))))))
      break;
    end
    alp = alp/2;
  end
  
  alp  
  
  %Compute new iterates
  xd = xd + alp*p(1:5)
  vd = vd + alp*p(6:end)  
  
  gradf = double(subs(df,x,xd));
  dA  = double(subs(A,x,xd));  
  
  %vd  = -dA.'\gradf;
end