function [U param] = invertPendControl(yn,param)

%Compute a single time step of control

%Estimate other states
% P = param.Q;
[x,P] = EKF(param.x_old,yn,param.u_old,param.Q,param);
%x = param.x_old;

%Estimate states over horizon
for t = 1:param.M
    dx = rk4(t,x(:,t),param.u_old,param.h,param);
    x(:,t+1) = dx;
end

%initialise u
u = zeros(param.M+1,1);

%optimise u over horizon
for k = 1:param.M
   [u(k+1),param] = SQP(x(:,k),param.u_old,param);
   k
end

%return first u for control action
U = u(2);
param.u_old = U;