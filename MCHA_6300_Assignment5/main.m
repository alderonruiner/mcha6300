% Alderon Ruiner
% c3279096 - MCHA6300


clear;
clc;
close all;

%% Define Parameters
param.m  = 0.742453953067092;
param.l  = 0.010479832883254;
param.fc = 0.000000000546431;
param.fp =  0.000180435058525;
param.Jp = 0.002845044685282;
param.c1 = 12.942119630860724;
param.c2 = -1.999999214754792;
param.g  = 9.81;
param.t  = 0;
param.d  = 1/40;
param.M  = 50; %horizon
param.Q  = diag([1 10 1e-4 1e-4]);
param.R  = 0.1;
param.a1 = param.Jp/(param.m*param.l);
param.a2 = 1/param.l;
param.b = param.Jp/(param.m*param.l^2);
param.k1 = param.c1/(param.m*param.l);
param.k2 = (param.fc - param.c2)/(param.m*param.l);
param.k3 = param.fp/(param.m*param.l);
param.d = 1/40;
param.xh = zeros(4,param.M);
param.uh = zeros(1,param.M);
param.x_old = 0;
param.xp = 0;
param.u_old = 0;
param.y_old = 0;
h = param.d;

func = @(t,x,u,param) pendModel(t,x,u,param);


%% Testing dynamic model
tspan = [0 param.d];
x0 = [0; pi; 0; 0];
C = eye(4);
x = x0;
y = zeros(2,1);

[u, param] = invertPendControl_new(y, param);

param.x_old = param.xp;
param.u_old = u;
param.y_old = y;

