function [x, P] = EKF(x,y,u,P,param)
% Alderon Ruiner
% c3279096 - MCHA6300

%previous time step state x
%current time step measurement y
%input u
%P is covariance on states

%params
m = param.m;
l = param.l;
f_c = param.fc;
f_p = param.fp;
J_p = param.Jp;
c_1 = param.c1;
c_2 = param.c2;
g  = 9.81;
nx = length(x);
ny = length(y);

Q = diag([1, 1, 1, 1]);
R = diag([1, 1]);

%initial state covariance
% P;

% M.R and M.Q are upper triangular cholesky factors

%measurement model y_hat = h(x,u)
%jacobian of h is C
h = [x(1);x(2)];
J = [1 0 0 0;
     0 1 0 0];
C = J;

[~,r] = qr([R zeros(ny,nx); P*C' P]);

%all probably the wrong size 
cal_R11 = r(1:ny,1:ny);
cal_R12 = r(1:ny,ny+1:nx+ny);
cal_R22 = r(ny+1:nx+ny,ny+1:nx+ny);

e = cal_R11'\(y - h); 
xf = x + cal_R12'*e;
t = 0;
%transition model RK4
x_hat = rk4(t,x,u,param.h,param);
%x_hat = f(x,u) = RK4
A = [ 0,                                                                                                                                                                                                                                                                                                                                                                                                               0,                                                   1,                                                                     0;
      0,                                                                                                                                                                                                                                                                                                                                                                                                               0,                                                   0,                                                                     1;
	  0,                                                                   -(l*(J_p^2*x(4)^2*cos(x(2)) - J_p*f_p*x(4)*sin(x(2)) + g*l^3*m^2*cos(x(2))^2 + J_p*g*l*m - 2*J_p*g*l*m*cos(x(2))^2 + J_p*c_1*l*u*sin(2*x(2)) + J_p*c_2*l*x(3)*sin(2*x(2)) - J_p*f_c*l*x(3)*sin(2*x(2)) - f_p*l^2*m*x(4)*(sin(x(2)) - sin(x(2))^3) - 2*J_p*l^2*m*x(4)^2*cos(x(2)) + J_p*l^2*m*x(4)^2*cos(x(2))^3))/(J_p - l^2*m*cos(x(2))^2)^2,     (J_p*(c_2 - f_c))/(m*(J_p - l^2*m*cos(x(2))^2)), -(l*(f_p*cos(x(2)) + 2*J_p*x(4)*sin(x(2))))/(J_p - l^2*m*cos(x(2))^2);
	  0, (l*(l^3*m^2*x(4)^2*cos(x(2))^2 - 2*g*l^2*m^2*cos(x(2)) + J_p*g*m*cos(x(2)) + J_p*l*m*x(4)^2 - J_p*c_1*u*sin(x(2)) - J_p*c_2*x(3)*sin(x(2)) + J_p*f_c*x(3)*sin(x(2)) + g*l^2*m^2*cos(x(2))^3 - 2*J_p*l*m*x(4)^2*cos(x(2))^2 - c_1*l^2*m*u*cos(x(2))^2*sin(x(2)) - c_2*l^2*m*x(3)*cos(x(2))^2*sin(x(2)) + f_c*l^2*m*x(3)*cos(x(2))^2*sin(x(2)) + 2*f_p*l*m*x(4)*cos(x(2))*sin(x(2))))/(J_p - l^2*m*cos(x(2))^2)^2, (l*cos(x(2))*(c_2 - f_c))/(J_p - l^2*m*cos(x(2))^2),             -(m*x(4)*sin(2*x(2))*l^2 + f_p)/(J_p - l^2*m*cos(x(2))^2)];
 
%jacobian of transition model is A

[~,r] = qr([P*A;
            Q]);
%probably the wrong size 
cal_R_bar = r(1:nx,1:nx);

P = cal_R_bar;
% xp = f
% set new initial conditions and P
x = x_hat;
