function dx = rk4f(t,x,u,h,fcn,param)

F1 = h*fcn(t,x,u,param);
F2 = h*fcn((t + 0.5*h),(x + 0.5*F1), u, param);
F3 = h*fcn((t + 0.5*h) ,(x + 0.5*F2), u, param);
F4 = h*fcn((t + h),(x + F3), u, param);

dx = x + (F1 + 2*F2 + 2*F3 + F4)/6;