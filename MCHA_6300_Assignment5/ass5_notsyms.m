clc;
clear;
close all;

m = 0.742453953067092;
l= 0.010479832883254;
fc = 0.000000000546431;
fp = 0.000180435058525;
Jp = 0.002845044685282;
c1 = 12.942119630860724;
c2 = -1.999999214754792;
param.g = 9.81;

param.a1 = Jp/(m*l);
param.a2 = 1/l;
param.b = Jp/(m*l^2);
param.k1 = c1/(m*l);
param.k2 = (fc - c2)/(m*l);
param.k3 = fp/(m*l);
param.d = 1/40;

h = param.d;
func = @(t,x,u,param) pendModel(t,x,u,param);

N = 100;
tspan = [0 param.d];
x0 = [0; 90*pi/180; 0; 0];
C = eye(4);
X = zeros(N,4);
y = X;

% for T = 1:N
%    u = 0;%0.5*cos(T*param.d);
%    [~, x] = ode45( @(t,x) pendModel(t,x,u,param), tspan, x0);
%    X(T,:) = x(length(x),:);
%    x0 = X(T,:);
%    x = 0;
%    
%    y(T,:) = C*X(T,:)';
% end
%% RK4
% 
% X1 = zeros(N,4);
% h = param.d;
% x = [0;pi/180;0;0];
% 
% func = @(t,x,u,param) pendModel(t,x,u,param);
% 
% for t = 1:N
%     u = 0;%0.01*sin(T*param.d);
% %     dx = rk4(t,x,u,h,param)';
%     dx = rk4f(t,x,u,h,func,param)';
%     X1(t,:) = dx;
%     x = X1(t,:)';
%     
% %     H = J'*J;
%     
% end

%% MPC
M = 60; %Horizon
u_h = zeros(M,1);
ud = u_h;
x_h = x0;
for t = 1:N
   
    % Determine horizon for timestep
    for i = 1:M
        u_h = ud;
        dx = rk4f(i,x_h,u_h(i),h,func,param)';
        X1_h(i,:) = dx;
        x_h = X1_h(i,:)';
    end

    %% SQP
    
    %Q and R
    Q = diag([1 10 1e-4 1e-4]);
    R = 0.1;
        
    %initial state and input guess
    ud = u_h;
    xd = X1_h;
        
    %lagrange multiplier
    lamda = diag(Q);
  
          
    for i = 1:M
    %%
        Lb = -0.7*ones(4,1);
        Ub = 0.7*ones(4,1);
       
        x = xd;
        u = ud;
        %cost function to minimise
        %Add loop over horizon for optimised u vector?
        V = x(i,:)*Q*x(i,:)' + u(i,:)*R*u(i,:)';

        % active constraints
        c = [x(1) + 0.7;
             x(1) - 0.7;
             u(1) - 0.5;
             u(1) + 0.5];

        disp = 0;
        %lagrangian function
        L = V + sum(lamda.*c);

        dV = gradient(V);
        grad = gradient(L);
%         H = hessian(L);
        J = 0;
        H = J'*J;
      %  jac  = jacobian(L);

        %---> min 0.5*p'*Lxx*p + p'*Lx
        % s.t. c(x) + p'*c_x(x)
        % where Lxx is the hessian of L and Lx is the gradient of L

        %Need L_i and k_i inequality constraints
        L_i = [eye(length(Ub)); 
              -eye(length(Lb))];
        k_i = [Ub;-Lb];

        %Quadprog minimises 0.5*x'*H*x + f*x, ooo that looks familiar now eh?
        %find the 'search direction' p_start and lagrange multipliers
        [p_star, lm, ~] = quadProgIP(H,grad,L_i,k_i,[],[],disp);

        %merit function required instead of slack varible here for something
        %MERIT FUNCTION 
          %Find alpha
          alp = 1;
          del = 0.001;
          lam = max(abs(vd + p(6:end)));
          mu = lam + del;
          mo = V + (mu*sum(abs(double(subs(c,x,xd)))));
          for k=1:50
            xn = xd + alp*p(1:5);
            mn = V + mu*(sum(abs(double(subs(c,x,xn)))));
            if mn < mo + 0.01*alp*(gradf.'*p(1:5) - (mu*sum(abs(double(subs(c,x,xd))))))
              break;
            end
            alp = alp/2;
          end

          %Compute new iterates
          ud = ud + alp*p;
          lam = lam + alp*p;
    end
end
   %%
     
    %SEND OPTIMISED U TO SYSTEM AND SYSTEM MODEL
    
   [~, x] = ode45( @(t,x) pendModel(t,x,u,param), tspan, x);
   X(T,:) = x(length(x),:);
   x = X(T,:);
    
   %GET ESTIMATED STATES FROM SYSTEM TO SEND TO SYSTEM MODEL
    M.x1 = x;
    M.P1 = Q;
    M.R = R;
    Z.N = t;
    M.A = jac;
    M.B = [1; 1; 1; 1];
    M.C = [1; 1; 1; 1];
    M.D = 0;
    xp = EKF(Z,M);
    
    %ESTIMATE SHIT IN SYSTEM MODEL
    dx = rk4f(t,xp,u_star,h,func,param)';
    X1(t,:) = dx;
    x = X1(t,:)';
    
    %SEND SHIT BACK TO OPTIMISER


%% plot discrete outputs
% figure(1);
% plot(linspace(1,100,length(y)),y(:,1))
% hold on
% plot(linspace(1,100,length(y)),y(:,2))
% plot(linspace(1,100,length(y)),y(:,3))
% plot(linspace(1,100,length(y)),y(:,4))
% legend('displacement','angle','velocity','angular velocity')
% title('Output')
% 
% hold off
figure(2)
%plot states
subplot(2,1,1)
plot(linspace(1,100,length(X)),X(:,1))
hold on
plot(linspace(1,length(X),length(X)),X(:,2))
plot(linspace(1,100,length(X)),X(:,3))
plot(linspace(1,100,length(X)),X(:,4))
legend('x1','x2','x3','x4')
title('ODE45 States')

%plot states
subplot(2,1,2)
plot(linspace(1,100,length(X1)),X1(:,1))
hold on
plot(linspace(1,length(X1),length(X1)),X1(:,2))
plot(linspace(1,100,length(X1)),X1(:,3))
plot(linspace(1,100,length(X1)),X1(:,4))
legend('x1','x2','x3','x4')
title('RK4 States')

% %%
% X = X';
% % Plot the pendulum
% P1=[X(1,:);
%     zeros(1,length(X(1,:)))];
% 
% P2=P1+[sin(-X(2,:));
%        cos(-X(2,:))];
%    
%    figure(3)
% for i=1:size(P1,2)    
%     
%     pause(0.1)
%     hold on    
%     if i==1       
%         p1 = plot(P1(1,i),P1(2,i),'b^','MarkerSize',10,'LineWidth',3);
%         p2 = plot(P2(1,i),P2(2,i),'bo','MarkerSize',10,'LineWidth',3);
%         p3 = plot([P1(1,i);P2(1,i)],[P1(2,i);P2(2,i)],'b','LineWidth',3);
%         p4 = plot([-5 5],[0;0],'b','LineWidth',3);
%     else
%         set(p1,'xdata',P1(1,i),'ydata',P1(2,i))
%         set(p2,'xdata',P2(1,i),'ydata',P2(2,i))
%         set(p3,'xdata',[P1(1,i);P2(1,i)],'ydata',[P1(2,i);P2(2,i)])
%         set(p4,'xdata',[-5 5],'ydata',[0;0])
%     end
% 
%     axis equal
%     xlim([-5 5])
%     ylim([-5 5])
%     hold off
%     drawnow
% end
% disp('Done')
% 
% 
