%% MCHA6300 - Assignment 4
%% Alderon Ruiner - c3279096

%% quadratic programming solver routine

function [x,lm,err] = quadProgIP(H,f,L,k,A,b,disp)

%INPUTS:
% H : a symmetric, positive semi-definite n x n matrix (must be supplied);
% 
% f : an n x 1 vector (must be supplied);
% 
% L : an m x n matrix that together with k represents inequality
% constraints (they must be supplied together and can both
% be empty matrices if required);
%     
% k : an m x 1 vector that together with L represents inequality
% constraints (they must be supplied together and can both
% be empty matrices if required);
% 
% A : a p x n matrix that together with b represents equality
% constraints (they must be supplied together and can both
% be empty matrices if required);
% 
% b : an p x 1 vector that together with A represents equality
% constraints (they must be supplied together and can both
% be empty matrices if required);
% 
% disp: a scalar that if greater than zero will force the
% routine to display iteration information;
% 
%OUTPUTS:
% x : an n x 1 vector of the solution (note that if err isnon-zero then x may not be feasible or optimal);
% 
% lm : a structure with lm.inequality, which is an m x 1 vector
% of Lagrange multipliers for the inequality constraints, and
% lm.equality, which is an p x 1 vector of Lagrange
% multipliers for equality constraints ;
% 
% err : an integer error code that indicates if the algorithm
% terminated correctly (err=0), or in error (err!=0);

%VARIABLES:
%s - slack variable
%z - lagrangian multiplier
%y - lagrangian multiplier
%Z - diag(z)
%S - diag(s)
%e - vector of ones
%mu - the 'target'

%CHECK FOR VALID CONSTRAINTS
if((isempty(A) ~= 1) && isempty(b))

    fprintf('\n Invalid equality constraints. Ensure they are either both empty or both full. b is empty. \n')
    x = NaN;
    lm = NaN;
    err = 7;
    
elseif( (isempty(b) ~= 1)&& isempty(A))
    
    fprintf('\n Invalid inequality constraints. Ensure they are either both empty or both full. A is empty. \n')
    x = NaN;
    lm = NaN;
    err = 7;
elseif((isempty(L) ~= 1) && isempty(k))
    
    fprintf('\n Invalid inequality constraints. Ensure they are either both empty or both full. k is empty.  \n')
    x = NaN;
    lm = NaN;
    err = 8;
  
elseif((isempty(k) ~= 1)&& isempty(L))
    
    fprintf('\n Invalid inequality constraints. Ensure they are either both empty or both full. L is empty.  \n')
    x = NaN;
    lm = NaN;
    err = 8;
else


%INITIALISATION
%vectorise all inputs to determine initial points
inp_vec = [reshape(H,[1,numel(H)]),reshape(f,[1,numel(f)]),reshape(L,[1,numel(L)]),reshape(k,[1,numel(k)]),reshape(A,[1,numel(A)]),reshape(b,[1,numel(b)])];
eta_inf = max(abs(inp_vec));
[M,~] = size(L);
[N,~] = size(H);
[P,~] = size(A);
x = zeros(N,1);
y = zeros(P,1);
e = ones(M,1); %must be same length as s
z = sqrt(eta_inf)*e;
s = sqrt(eta_inf)*e;

vpk = inf;
vdk = inf;
err = 0;

% z_max = max(-z);
% s_max = max(-s);
% veco = [z_max'; s_max';0];
% beta = 1000 + 2*max(veco);
% z = z + beta*e;
% s = s + beta*e;

eps_c = 1e-8;
eps_r = 1e-8;
eps_mu = 1e-8;
iteration = 0;
upper_lim = 1000;

tol = 1e-6;
mu = z'*s/M; %initial mu
mu_c = 0;
gamma = 0.01;
%ALGORITHM
% 1. Check termination conditions
while(mu>tol)
    % 2. Obtain preditor direction
    
    iteration = iteration + 1;
    
    Z = diag(z);
    S = diag(s);

    [U,~] = size(Z);
    [M,~] = size(L);
    
    %eliminate empty matrix warning with checks
    if((isempty(A) == 1) && (isempty(L)==1))
    
        matrix_p = [H  zeros(N,M);
                zeros(U,N)   Z];
            
    elseif((isempty(A) == 1) && (isempty(L)~=1))
        
        matrix_p = [H L' zeros(N,M);
                    L zeros(M,M)  eye(M,M);
                    zeros(U,N) S  Z];
                
    elseif((isempty(A) ~= 1) && (isempty(L)==1))
        
        matrix_p = [H A' zeros(N,M);
                A zeros(P,P) zeros(P,M);
                zeros(U,N) zeros(U,P) Z];
           
    else
        
        matrix_p = [H A' L' zeros(N,M);
                    A zeros(P,P)  zeros(P,M)  zeros(P,M);
                    L zeros(M,P)  zeros(M,M)  eye(M,M);
                    zeros(U,N) zeros(U,P)  S  Z];
    end
    


    m = length(z);
    r1  = -H*x - f;
    r2 = b;
    
    if M > 0
        r1  = r1 - L'*z;
        r2 = r2; 
    end
    
    if P > 0
       r1 = r1 - A'*y;
       r2 = r2 - A*x;
    end
    r3 = k - L*x - s;
    r4 = mu_c*e - Z*S*e;
    
    %solve for deltas
    delta = matrix_p\[r1;r2;r3;r4]; 
    delta_xp = delta(1:N,:);
    delta_yp = delta(N+1:N+P,:);
    delta_zp = delta(N+P+1:N+P+M,:);
    delta_sp = delta(N+P+M+1:end,:);

    % 3. Define the distance to the boundary alpha along the predictor direction
    max1 = max(-delta_zp/z);
    max2 = max(-delta_sp/s);
    maxVec = [max1';max2';1];
    alpha_p = 1/(max(maxVec));

    % 4. Define the adaptive centering parameter sigma
    mu = z'*s/m;
    sigma = ((abs((1/m)*(z+alpha_p*delta_zp)'*(s + alpha_p*delta_sp)-mu_c))/mu)^3;

    % 5. Obtain the combined centering-corrector direction parameter sigma
    delta_Zp = diag(delta_zp);
    delta_Sp = diag(delta_sp);
    matrix_cc = matrix_p;
    r4cc = (mu_c + sigma*mu)*e - delta_Zp*delta_Sp*e;
    
    delta_cc = matrix_cc\[zeros(N,1);zeros(P,1);zeros(M,1);r4cc]; %zeros may have to be coloumn vectors of zero
    %the lengths need to be changed
    delta_xcc = delta_cc(1:N,:);
    delta_ycc = delta_cc(N+1:N+P,:);
    delta_zcc = delta_cc(N+P+1:N+P+M,:);
    delta_scc = delta_cc(N+P+M+1:end,:);
    
    % 6. Define the distance to the boundary alpha along the the combined predictor and centering coreector ddirections
    max3 = max((-delta_zp - delta_zcc)/z);
    max4 = max((-delta_sp - delta_scc)/s);
    maxVec2 = [max3';max4';1];
    alpha_pcc = 1/(max(maxVec2));

    % 7. Define scaling parameter sigmapcc
    sigma_pcc = ((abs((1/m)*(z+alpha_pcc*(delta_zp+delta_zcc))'*(s + alpha_pcc*(delta_sp + delta_scc)) - mu_c))/mu)^3;

    % 8. if sigma pcc <= sigmap
    if (sigma_pcc <= sigma) 
        delta_x = delta_xp + delta_xcc;
        delta_y = delta_yp + delta_ycc;
        delta_z = delta_zp + delta_zcc;
        delta_s = delta_sp + delta_scc;

       
    % 9. else
    else
        r4i = mu_c*e - sigma_pcc*mu - Z*S*e;
        delta = matrix_p\[r1;r2;r3;r4i];
        delta_x = delta(1:N,:);
        delta_y = delta(N+1:N+P,:);
        delta_z = delta(N+P+1:N+P+M,:);
        delta_s = delta(N+P+M+1:end,:);

    % 10. end
    end
    % 11. Obtain the final step length alphai. Choose a positive gammaf
    AIZ = 0;
    AIS = 0;
    for j = 1: length(z)
            temp = max(1,-delta_z(j)/z(j));
            if temp > AIZ
                AIZ = temp;
                j_z = j;
            end
            temp = max(1,-delta_s(j)/s(j));
            if temp > AIS
                AIS = temp;
                j_s = j;
            end
    end
    alpha_z = 1/AIZ;
    alpha_s = 1/AIS;
   
    
%     alpha_z = 1\(max(1, max(([-delta_z/z]))));
%     alpha_s = 1\(max(1, max((-delta_s/s))));
%     [~,j_z] = max((-delta_z/z));
%     [~,j_s] = max((-delta_s/s));
    minVec= [alpha_z'; alpha_s';1];
    alpha_f = min(minVec);
    mu_f = abs((1/m)*(z + alpha_f*delta_z)'*(s + alpha_f*delta_s) - mu_c);

    if (min(alpha_z,alpha_s)>=1)
        alpha = 1;
    elseif(alpha_z<alpha_s)
        alpha = ((gamma*mu_f)/(s(j_z) + alpha_f*delta_s(j_z)) - z(j_z))/(delta_z(j_z));
    else 
        alpha = ((gamma*mu_f)/(z(j_s) + alpha_f*delta_z(j_s)) - s(j_s))/(delta_s(j_s));
    end
    
    if(alpha < ((1-gamma)*alpha_f))
        alpha = ((1-gamma)*alpha_f);
    else
        alpha = (1-gamma)*alpha;
    end

    % 12. Form new interations of x,y,z,s
    x = x + alpha*delta_x;
    y = y + alpha*delta_y;
    z = z + alpha*delta_z;
    s = s + alpha*delta_s;
    mu = z'*s/M;
    %stopping criteria
%     max(abs(r1(:)))


    gamma_k = max(abs((Z*S*e - mu_c)));
    vk = (max(abs([r1(:);r2(:);r3(:)]))/eta_inf);
    vpk_old = vpk;
    vpk = max(abs([r2(:);r3(:)]))/eta_inf;
    vdk_old = vdk;
    vdk = max(abs(r1(:)))/eta_inf ;
    
    if ((vk <= eps_r) && (abs(mu - mu_c)<eps_mu) && (gamma_k <= eps_c))
        err = 0;
        break;
    elseif( vpk > eps_r && vpk >= vpk_old)
        err = 1;
        break;
    elseif(vdk > eps_r && vdk >= vdk_old)
        err = 2;
        break;
    elseif(iteration > upper_lim)
        err = 3;
        break;
    else
    end
    
end

lm.inequality = z;
lm.equality = y;

    if(disp > 0)
        fprintf('Iterations to solve: %0.0f \n',iteration);
        fprintf('Relative residual: %0.0f \n',vk);
        fprintf('Primal residual: %0.0f \n',vpk);
        fprintf('Dual residual: %0.0f \n',vdk);
        fprintf('Complimentary measure: %0.0f \n',mu);
        if(isempty(A) ==1)
            fprintf('Equality constraints\n');
        end
        
        if(isempty(A) ~= 1)
            fprintf('No equality constraints\n');
        end

        if(isempty(L) ==1)
            fprintf('Inequality constraints\n');
        end
        
        if(isempty(L) ~=1)
            fprintf('No inequality constraints\n');
        end
    end
end