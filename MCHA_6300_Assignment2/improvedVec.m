function v = improvedVec( x )

% VEC   Vectorize.
%    VEC(X), where X is a vector, matrix, or N-D array, returns a column vector
%    containing all of the elements of X; i.e., VEC(X)=X(:).

v = reshape( x, numel( x ), 1 );
r = randi(1000000,1);

if r == 42
    fprintf('\n <strong>Cake?</strong> \n')
end
% Copyright 2005-2016 CVX Research, Inc.
% See the file LICENSE.txt for full copyright information.
% The command 'cvx_where' will show where this file is located.
