% MCHA6300 - ASSIGNMENT 2
% Alderon Ruiner - c3279096
% TASKS:
% Implement a quasi-Newton algorithm (trust region of line based search)
% Impement a Guass Newton or trust region based algorithm to solve non-linear least squares problems
% NOTES: keyboard in trs3 has been commented out, uncomment if you wish to
% use this.

clc;
clear;
close all
load('ass2_data.mat');
%%

figure(1)
subplot(2,2,1)
plot(u(1,:), 'r')
title('MCHA6300 Assignment 2')
xlabel('Discrete time sample (t)')
ylabel('Input u_t')
subplot(2,2,3)
plot(y(1,:), 'b')
xlabel('Discrete time sample (t)')
ylabel('Output y_t')
subplot(2,2,2)
plot(u(2,:), 'r')
xlabel('Discrete time sample (t)')
ylabel('Input u_t')
subplot(2,2,4)
plot(y(2,:), 'b')
xlabel('Discrete time sample (t)')
ylabel('Output y_t')
%%  Test fcnA

theta = [improvedVec(Ai); 
         improvedVec(Bi);
         improvedVec(Ci);
         improvedVec(Di);
         improvedVec(Ki)];

[V, pe , g, J] = fcnA(y,u,theta);

%% SR1TRUST optimisation
 fcn = @(y,u,theta) fcnA(y,u,theta);
 theta = [improvedVec(Ai); 
         improvedVec(Bi);
         improvedVec(Ci);
         improvedVec(Di);
         improvedVec(Ki)];
     
 xOptSR1Trust = sr1Trust(fcn, y, u, theta);

%% GUASS NEWTON optimisation
 fcn = @(y,u,theta) fcnA(y,u,theta);
 theta = [improvedVec(Ai); 
         improvedVec(Bi);
         improvedVec(Ci);
         improvedVec(Di);
         improvedVec(Ki)];
     
opt.wolfeC1 = 1e-4;
opt.maxIter = 200;
opt.gradTol = 1e-4;
xOptGuass = guassNewton(fcn, y, u, theta, opt);