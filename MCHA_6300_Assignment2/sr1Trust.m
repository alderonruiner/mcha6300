function xOpt = sr1Trust(fcn,y,u,x)
% Assignment 2 version - some augmentation was required for fcn inputs and
% outputs

% fcn - matlab function handle
% x - initial variable estimate
% pars -  structure of important variables

k = 0; % initialise k
Delta = 0.05;
Delta_bar = 1e3;
eta = 0.1;
[~, ~, g_x, ~] = fcn(y,u,x);
x = x(:);
n = length(x);
H = -eye(n); %initial hessian guess


while norm(g_x) > sqrt(eps)
    X = x;
    % Step 3 
    [f_x,~, g_x,~] = fcn(y,u,x);
    pk = trs3(H,g_x,Delta);
    [f_x_p,~, g_x_p, ~] = fcn(y,u,x + pk);
    
    %Step 4 - determine rho
    
    rhok = (f_x - f_x_p)/(-pk'*g_x - 0.5*pk'*H*pk);
    
    %Step 5 - determine y
    
    yk = g_x_p - g_x;
    
    %Print useful update
    fprintf('Iteration Number = %5i, Cost = %10.2e, Newton Decrement = %10.2e, Delta = %10.2e\n',k,f_x,pk'*g_x,Delta);
    
    %Step 6 - 16 - checks for rho to determine delta
    if (rhok >= 0.75)
       
        if norm(pk) < 0.8*Delta
            Delta = Delta;
        else 
            Delta = min(2*Delta, Delta_bar);
        end
        
    elseif (rhok >= 0.1 && rhok <= 0.75)
        Delta = Delta;
    else
        Delta = 0.5*Delta;
    end
    

%Step 17-21 - rho checks to determine x
if rhok > eta
    x = x + pk;
else 
    x = x;
end

%Step 22 - 26
sk = pk;
if  abs(sk'*(yk-H*sk)) > sqrt(eps)*norm(sk)*norm(yk-H*pk) 
    H = H + ((yk - H*sk)*(yk - H*sk)')/(sk'*(yk - H*sk));
else 
    H = H;
end

k = k + 1;
end
xOpt = x;