function [V,pe,g,J] = fcnA(y,u,theta)
%theta = vector of A,B,C,D,K
A = theta(1:64);
B = theta(65:80);
C = theta(81:96);
D = theta(97:100);
K = theta(101:length(theta));

m = sqrt(length(A));
n = m;
p = 2;
A = unVec(A,n,n);
B = unVec(B,8,2);
C = unVec(C,2,8);
D = unVec(D,2,2);
K = unVec(K,8,2);
N = length(u);

dTheta = eye(length(theta)); % initiliase dTheta columns
dx = zeros(8,length(theta)); % initiliase dx

x = zeros(n, N+1);
for t = 1:N
    % STM - x(t+1) = (A - K*C)*x + (B - K*D)*u + K*y - [needed to include error term]
    x(:,t+1) = (A - K*C)*x(:,t) + (B - K*D)*u(:,t) + K*y(:,t);
    y_bar(:,t) = C*x(:,t) + D*u(:,t);
    error(:,t) = y(:,t) - y_bar(:,t);
    
    for i = 1:length(theta)

        dA = dTheta(1:64,i);
        dA = unVec(dA,n,n);
        dB = dTheta(65:80,i);
        dB = unVec(dB,8,2);
        dC = dTheta(81:96,i);
        dC = unVec(dC,2,8);
        dD = dTheta(97:100,i);
        dD = unVec(dD,2,2);
        dK = dTheta(101:length(theta),i);
        dK = unVec(dK,8,2);

        dedTheta(:,i) = -dC*x(:,t) - C*dx(:,i) -dD*u(:,t);
        dx(:,i) = dA*x(:,t) + A*dx(:,i) + dB*u(:,t) + dK*error(:,t) + K*dedTheta(:,i); 
    end

    J(t,:) = dedTheta(1,:);
    J(t+length(y),:) = dedTheta(2,:);
    
end
error = improvedVec(error');

V = error'*error; %get and return cost function V
g = J'*error; %get and return gradient g
pe = error; %get and return error pe
% x(t+1) = Ax + Bu + Ke
% y = Cx + Du + e

% y_bar = C*x + D*u
% e = y - y_bar



% f = y
% g = jacobian*something'