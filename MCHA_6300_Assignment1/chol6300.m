function C = chol6300(A)

flag = issymmetric(A); %check if matrix symmetric
if flag == 1
    fprintf("Matrix is symmetric \n")
    eigen = eig(A);
    
    flag2 =  all(eigen > 0);
    
    if  flag2 == 1 %check if matrix is semi-positive definite
    fprintf("Matrix is semi-positive definite \n")
    
    %Cholesky routine
    %https://en.wikipedia.org/wiki/Cholesky_decomposition - algorithm
    %breakdown
    n = length(A); %get length of matrix to initialise matrix C
    C = zeros(n,n); %initialise matrix of zeros n x n size
    for i=1:n
        C(i,i) = sqrt(A(i,i) - C(i,:)*C(i,:)'); % C*C' - vectorised version of sum(C^2)
        for j = (i+1):n
            C(j,i) = (A(j,i) - C(i,:)*C(j,:)')/C(i,i);% C*C' - vectorised version of sum(C*C')
        end
    end
    
    else 
    fprintf("Matrix is not semi-positive definite \n")
    C = [];
    end
else 
   fprintf("Matrix is not symmetric \n")
   C = [];
end
end
