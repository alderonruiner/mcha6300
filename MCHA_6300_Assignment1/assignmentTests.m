% Test cases to test cholesky, givens and householder routines

function tests = assignmentTests
tests = functiontests(localfunctions);
end

function testChol6300(testCase)
A = [4 12 -16; 
    12 37 -43; 
    -16 -43 98];

actual_C = chol6300(A);
expected_C = chol(A, 'lower');
assertEqual(testCase, actual_C, expected_C, 'AbsTol', 10*eps, 'Expected your cholesky to match');
end

function testGivens6300(testCase)
A = [1  2  3;
     4  5  6;
     7  8  9;
     10 11 12;
     13 14 16];

[Q_bar, R_bar] = givens6300(A);
actual_A = Q_bar*R_bar;
% check that givens routine works
assertEqual(testCase, actual_A, A, 'AbsTol', 36*eps, 'Expected your norm to match')

end

function testHouseholder6300(testCase)
A = [1  2  3;
     4  5  6;
     7  8  9;
     10 11 12;
     13 14 16];

[Q_bar, R_bar] = householder6300(A);
actual_A = Q_bar*R_bar;
% check that householder routine works
assertEqual(testCase, actual_A, A, 'AbsTol', 40*eps, 'Expected A matrix  to match');

end

function testHouseholder6300Triu(testCase)
A = [1  2  3;
     4  5  6;
     7  8  9;
     10 11 12;
     13 14 16];

[~, R_bar] = householder6300(A);

% is triu unfortunately only works for square matrices
actual_tri = 1;
for j = 1:size(A,2)
    for i = j:size(A,1)
        if i~=j && abs(R_bar(i,j)) >= 1e-8
            tri = 0; % Lower triangle is not zero
        end
    end
end

expected_tri = 1;

% check if R_bar is upper triangular
assertEqual(testCase, actual_tri, expected_tri, 'AbsTol', 100*eps, 'Expected R_bar to be upper triangular');
% R is Upper Triangular

end

function testHouseholder6300Orthogonal(testCase)
A = [1  2  3;
     4  5  6;
     7  8  9;
     10 11 12;
     13 14 16];

[Q_bar, ~] = householder6300(A);
[m,~] = size(Q_bar);
actual_O = Q_bar*Q_bar';
expected_O = eye(m);


% check if Q_bar is orthogonal
assertEqual(testCase, actual_O, expected_O, 'AbsTol', 10*eps, 'Expected Q_bar to be orthogonal');
% R is Upper Triangular

end

function testGivens6300Triu(testCase)
A = [1  2  3;
     4  5  6;
     7  8  9;
     10 11 12;
     13 14 16];

[~, R_bar] = givens6300(A);

% is triu unfortunately only works for square matrices
actual_tri = 1;
for j = 1:size(A,2)
    for i = j:size(A,1)
        if i~=j && abs(R_bar(i,j)) >= 1e-8
            tri = 0; % Lower triangle is not zero
        end
    end
end

expected_tri = 1;

% check if R_bar is upper triangular
assertEqual(testCase, actual_tri, expected_tri, 'AbsTol', 100*eps, 'Expected R_bar to be upper triangular');
% R is Upper Triangular

end

function testGivens6300Orthogonal(testCase)
A = [1  2  3;
     4  5  6;
     7  8  9;
     10 11 12;
     13 14 16];

[Q_bar, ~] = givens6300(A);
[m,~] = size(Q_bar);
actual_O = Q_bar*Q_bar';
expected_O = eye(m);


% check if Q_bar is orthogonal
assertEqual(testCase, actual_O, expected_O, 'AbsTol', 10*eps, 'Expected Q_bar to be orthogonal');
% R is Upper Triangular

end
