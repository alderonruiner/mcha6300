function [c,s] = givensRot(a,b)
% rotate vector to later form Givens matrix

theta = atan2(a,b);
c = cos(theta);
s = sin(theta);

end