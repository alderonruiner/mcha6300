function [Q,R] = givens6300(A)
%https://en.wikipedia.org/wiki/QR_decomposition#Using_Givens_rotations

% initialise
[m,n] = size(A); %get size of matrix A
R = A; %set R = A
Q = eye(m); %set Q to be identity matrix m x m

%Givens routine

  for j = 1:n
    for i = m:-1:(j+1)
       G = eye(m); %initialise Givens matrix as identity matrix
      [c,s] = givensRot( R(i-1,j),R(i,j) ); %rotate matrix givensRot(a,b) such x = [a;b] for G*x = [r;0]
      G([i-1, i],[i-1, i]) = [c -s; %form Givens matrix
                              s c];
      R = G'*R;
      Q = Q*G;
    end
  end
end



