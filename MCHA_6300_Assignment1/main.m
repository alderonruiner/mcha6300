% Alderon Ruiner
% C3279096
% MCHA6300 - Assignment 1
% Included: Test functions for cholesky and QR factorisations. See
% assignmentTests.m
% Due: 21/08/2020 - 23:59
% Included functions: householder6300, givens6300, givensRot, chol6300
% Notes: Givens6300 is computationaly slow and intensive, good luck getting
% it to run through a 10000 long vector, guess you could say it needs some
% advanced optimisation. Use at your own risk.
clc;
clear;
close all;
load('ass1_data.mat');
%% 1. Identification of a Dynamic System
figure(1)
subplot(2,1,1)
plot(u, 'r')
title('MCHA6300 Assignment 1')
xlim([0 10000])
ylim([-50 100])
xlabel('Discrete time sample (t)')
ylabel('Input u_t')
subplot(2,1,2)
plot(y, 'b')
xlim([0 10000])
ylim([-1000 1500])
xlabel('Discrete time sample (t)')
ylabel('Output y_t')


%% 2. Cholesky and QR Factorisations
%2.1 Write Cholesky factorisation
% See chol6300

%2.2 Write QR factorisation functions for givens and householder
% See givens6300 and householder6300

runtests('assignmentTests.m')

%% 3. Solution to  least Squares Problem 
% 3.1 Construct least squares coefficient matrix A

% Choose your order: (glhf)
% n = 1;
% n = 2;
n = 3;
% n = 4;
% n = 5;
% n = 6;


% get matrix A
A = ones(length(y)-n, 1);
for i = n:-1:1
    A(:,end+1) = y(i:length(y)-n+i-1);
end

for i = n+1:-1:1
    A(:,end+1) = u(i:length(y)-n+i-1);
end

%get vector b
b = -y(n+1:end);

%theta = A \ b; %calculate theta where theta = b*betterInv(A)
% 3.2 Assuming n=3, compute predicted ys for Cholesky and QR methods

%NOTE: theta = {den coefficients, ..., num coefficients} - coefficients of
%transfer function G(q)

% CHOLESKY LEAST SQUARES
H = A'*A; %form matrix H = A'*A
d = A'*b; % form matrix d = A'*b
C = chol6300(H);
theta_chol = C'\(C\d); % compute theta from cholesky factor
y_pred_chol = A*theta_chol; %compute predicted y


% HOUSEHOLDER LEAST SQUARES
[m,n] = size(A);
[Q_h, R_h] = householder6300(A);
Q1_h = Q_h(1:m,1:n); % Form Q1
R1_h = R_h(1:n,1:n); % Form R1
d_h = Q1_h'*b;
theta_house = R1_h\d_h;
y_pred_house = A*theta_house;


% % GIVENS LEAST SQUARES
% [Q_g, R_g] = givens6300(A);
% Q1_g = Q_g(1:m,1:n); % Form Q1
% R1_g = R_g(1:n,1:n); % Form R1
% d_g = Q_g1'*b;
% theta_givens = R_g\d_g;
% y_pred_givens = A*theta_givens;

% 3.3 Determine if n=3 is most suitable model order, justify

fprintf('<strong>3.3:  Somewhat inconclusive if n=3 is the most suitable model order \n others orders have very similar residual errors. \n Perhaps the givens routine is the key answering this \n </strong>')


% Calculate residual errors (err = predicted_y - y)
y_house_err = y_pred_house - y(1:length(A));
y_chol_err = y_pred_chol - y(1:length(A)); 
% y_givens_err = y_pred_givens - y(1:length(A));

% Plots
% NOTE: did something funky somewhere and had to flip y_predicted values
% (possibly with transposing)
figure(2)
subplot(3,1,1)
plot(y, 'LineWidth', 2)
hold on
plot(-y_pred_chol, 'LineWidth', 2);
plot(-y_chol_err)
legend('Output y','predicted output', 'Residual Error')
title('Cholesky prediction')
hold off

subplot(3,1,2)
plot(y, 'LineWidth', 2)
hold on
plot(-y_pred_house, 'LineWidth', 2);
plot(-y_house_err)
legend('Output y','predicted output', 'Residual Error')
xlabel('Time')
ylabel('Output')
title('Householder prediction')
hold off

% subplot(3,1,3)
% scatter(x,y, 5,'x')
% hold on
% plot(y_pred_givens, 'LineWidth', 2);
% plot(-y_givens_err)
% legend('Output y','predicted output', 'Residual Error')
% title('Givens prediction')
% hold off



% 3.4 Generate Bode plot using bode and tf
% from estimated parameter vector (f and b)
% y = G*u - G is transfer function

% split theta into numerator and denominator assuming that half are num and
% half den
den_chol = theta_chol(1:length(theta_chol)/2,1); %assign den values from indice 2 onwards as at i = 1 f = 1;
den_chol(1) = 1;
num_chol = theta_chol(length(theta_chol)/2 + 1:length(theta_chol),1);
sys_chol = tf(num_chol',den_chol', -1); %calculate transfer function

den_house = theta_house(1:length(theta_house)/2,1);
den_house(1) = 1;
num_house = theta_house(length(theta_house)/2 + 1:length(theta_house),1);
sys_house = tf(num_house',den_house', -1); %calculate transfer function

% den_givens = theta_givens(2:length(theta_givens)/2 + 1,1);
% den_givens(1) = 1;
% num_givens = theta_givens(length(theta_givens)/2 + 1:length(theta_givens),1);
% sys_givens = tf(num_givens',den_givens', -1);  %calculate transfer function

figure(3)
subplot(3,1,1)
bode(sys_chol)
title('Cholesky solved system')

subplot(3,1,2)
bode(sys_house)
title('Householder solved system')

% subplot(3,1,3)
% bode(sys_givens)
% title('Givens solved system')