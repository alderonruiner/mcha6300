function [Q,R] = householder6300(A)

% Get dimensions
[m,n] = size(A);

% Initialise
Q = eye(m); %initialise Q as identity matrix m x m
R = A;      %let R = A 
v=zeros(m,1); %initialise householder reflection vector as m x 1 of zeros


for i=1:n
    v(:) = 0; %Householder reflection vector
    v(i:m,1) = R(i:m,i);   % norm of vector - e1
    v(i) = v(i) - norm(v); %vector minus the euclidean norm of the vector
    
    w = v/norm(v); %normalised vector
    Q = Q-2*Q*w*w'; 
    R = R-2*w*w'*R;
    
end

end
