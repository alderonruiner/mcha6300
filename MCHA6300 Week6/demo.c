//mex -O demo.c 

#include "mex.h"

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
    int        i;
    
    /* Examine input (right-hand-side) arguments. */
    mexPrintf("\nThere are %d right-hand-side argument(s).", nrhs);
    for (i=0; i<nrhs; i++)  {
        mexPrintf("\n\tInput Arg %i is of type:\t%s ",i,mxGetClassName(prhs[i]));
    }
    
    /* Examine output (left-hand-side) arguments. */
    mexPrintf("\n\nThere are %d left-hand-side argument(s).\n", nlhs);
    if (nlhs > nrhs)
        mexErrMsgIdAndTxt( "MATLAB:mexfunction:inputOutputMismatch",
                "Cannot specify more outputs than inputs.\n");
    
    for (i=0; i<nlhs; i++)  {
        plhs[i]=mxCreateDoubleMatrix(1,1,mxREAL);
        *mxGetPr(plhs[i])=(double)mxGetNumberOfElements(prhs[i]);
    }     

}






//     
//     
//     int m = mxGetM(prhs[0]);
//     int n = mxGetN(prhs[0]);
//     double *A = mxGetPr(prhs[0]);
//     
//     plhs[0] = mxCreateDoubleMatrix(m,n,mxREAL);
//     double *An = mxGetPr(plhs[0]);
//     
//     
//     for(i=0;i<m*n;i++){
//         An[i] = A[i];
//     }
// 
//     
//     for(i=0;i<m;i++){
//         An[i + 2*m] = 3*An[i + 2*m];
//     }
// 
// 


