//mex -O House_mex.c 

#include "mex.h"
#include <math.h>

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
    int i;
    int m = mxGetM(prhs[0]);
    if(mxGetN(prhs[0]) != 1){
        mexErrMsgIdAndTxt( "MATLAB:mexfunction:inputOutputMismatch",
                "Not a column vector!.\n");
    }
    
    double *x = mxGetPr(prhs[0]);
    
    plhs[0] = mxCreateDoubleMatrix(m,1,mxREAL);
    double *v = mxGetPr(plhs[0]);
    double nx = 0.0;
    
    
    for(i=0;i<m;i++){
        v[i] = x[i];
        nx   = nx + x[i] * x[i];
    }
    v[0] = v[0] - sqrt(nx);
}




