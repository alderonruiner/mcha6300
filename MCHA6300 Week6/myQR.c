//mex -O myQR.c -lmwblas -lmwlapack
#include "mex.h"

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
    //Use these variable types so that Fortran plays nice!
    mwSignedIndex n, m, lwork, itau;
	mwSignedIndex info;
    
    //Variables in order: A
    m            = mxGetM(prhs[0]);
    n            = mxGetN(prhs[0]);
    double *A    = mxGetPr(prhs[0]);
    
    double optWork;
    if(m<n){itau = m;}else{itau = n;}
    double *tau = (double*)malloc(itau*sizeof(double));
    
    //Now use LAPACK dgeqrf
    lwork=-1;
    dgeqrf_(&m,&n,A,&m,tau,&optWork,&lwork,&info);
    if(info==0){
        lwork = (mwSignedIndex)optWork;
        double *work = (double*)malloc(lwork*sizeof(double));
        dgeqrf_(&m,&n,A,&m,tau,work,&lwork,&info);
        free(work);
    }
    
    free(tau);
}








