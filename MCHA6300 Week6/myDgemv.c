//mex -O myDgemv.c -lmwblas -lmwlapack
#include "mex.h"

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
    //Use these variable types so that Fortran plays nice!
    mwSignedIndex n, m, nm;
	mwSignedIndex ione = 1;
    
    //Variables in order: alpha, A, x, beta, z
    double alpha = (mxGetPr(prhs[0]))[0];
    m            = mxGetM(prhs[1]);
    n            = mxGetN(prhs[1]);
    double *A    = mxGetPr(prhs[1]);
    double *x    = mxGetPr(prhs[2]);
    double beta  = (mxGetPr(prhs[3]))[0];
    double *z    = mxGetPr(prhs[4]);
    
    //Create output 
    plhs[0]      = mxCreateDoubleMatrix(n,1,mxREAL);
    double *y    = mxGetPr(plhs[0]);
    
    //Now use BLAS dgemv to perform the operation
    dcopy_(&n,z,&ione,y,&ione);
    dgemv_("T",&m,&n,&alpha,A,&m,x,&ione,&beta,y,&ione);
}








