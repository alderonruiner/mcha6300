//mex -O House_mex.c 

#include "mex.h"
#include <math.h>

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
    int i,j;
    int m = mxGetM(prhs[0]);
    int n = mxGetN(prhs[0]);
    
    if(m != mxGetM(prhs[1])){
        mexErrMsgIdAndTxt( "MATLAB:mexfunction:inputOutputMismatch",
                "Matrix and vector dimensions do not agree.\n");
    }
    if(mxGetN(prhs[1]) != 1){
        mexErrMsgIdAndTxt( "MATLAB:mexfunction:inputOutputMismatch",
                "Not a column vector.\n");
    }
    
    double *w = malloc(n*sizeof(double));
    
    double *A = mxGetPr(prhs[0]);
    
    plhs[0] = mxCreateDoubleMatrix(m,n,mxREAL);
    double *An = mxGetPr(plhs[0]);
    for(i=0;i<m*n;i++){
        An[i] = A[i];
    }
    
    double *v = mxGetPr(prhs[1]);
        
    for(i=0;i<n;i++){
        w[i] = 0.0;
        for(j=0;j<m;j++){
            w[i] += v[j] * An[j + m*i];
        }
    }
    
    double nv = 0.0;
    for(i=0;i<m;i++){
        nv += v[i]*v[i];
    }
    
    double b = 2.0/nv;
    
    for(i=0;i<m;i++){
        for(j=0;j<n;j++){
            An[i + j*m] = An[i + j*m] - b*v[i]*w[j];
        }
    }
    
    free(w);
}




