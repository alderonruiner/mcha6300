//mex -O demo2.c 

#include "mex.h"

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
    int i;
    int m = mxGetM(prhs[0]);
    int n = mxGetN(prhs[0]);
    double *A = mxGetPr(prhs[0]);
    
    for(i=0;i<m*n;i++){
        mexPrintf("A[%i] = %10.2e\n",i,A[i]);
    }
    
    plhs[0] = mxCreateDoubleMatrix(m,n,mxREAL);
    double *An = mxGetPr(plhs[0]);
    
    
    for(i=0;i<m*n;i++){
        An[i] = 2.0 * A[i];
    }

    
    for(i=0;i<m;i++){
        An[i + 2*m] = 3*An[i + 2*m];
    }
}

