//mex -O copyVec.c -lmwblas -lmwlapack
#include "mex.h"

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
    //Use these variable types so that Fortran plays nice!
    mwSignedIndex n, m, nm;
	mwSignedIndex ione = 1;
    
    //Extract the input size and pointer
    m = mxGetM(prhs[0]);
    n = mxGetN(prhs[0]);
    double *x = mxGetPr(prhs[0]);
    
    //Create output the same size as input
    plhs[0]   = mxCreateDoubleMatrix(m,n,mxREAL);
    double *y = mxGetPr(plhs[0]);
    
    //Now use BLAS dcopy to copy n*m doubles from x to y
    nm = n*m;
    dcopy_(&nm,x,&ione,y,&ione);
}








