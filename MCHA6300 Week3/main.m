clc;
clear;
% close all;
% %%
% x = 2;
% fun = @(x) x^2;
% out = testFun(@fcn1, x);
%% Steepest Descent Algorith

% 1.1 Write your own algorithm using steepest descent direction for p and
% any method for choosing alpha > 0. Ensure algorithm doesn't terminate
% until a stationary point is obtained
pars = 0;
x = [1,1]';
func = @(x) fcn1(x);
opt1 = sdAlg(func, x, pars);
%% Numerical Gradient Check

% 2.1 Modify  your algoritm to allow for a numerical gradient check when the
%user requests it
opt2 = sdAlg(func, x, pars);
%% Algorithm Validation
 
% 3.1 Test your algorithm using the .p fcns
func = @(x) fcn1(x);
xOpt1 = sdAlg(func, x, pars);
func = @(x) fcn2(x);
xOpt2 = sdAlg(func, x, pars);
func = @(x) fcn3(x);
xOpt3 = sdAlg(func, x, pars);
func = @(x) fcn4(x);
xOpt4 = sdAlg(func, x, pars);