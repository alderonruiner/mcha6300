function [xOpt, g_x] = sdAlg(fcn, x , pars)
% fcn - matlab function handle that accepts x and pars and returns cost f
% and gradient column vector g: [f,g] = fcn(x, param)
% x - initial variable estimate
% param - structure containing parameters that are important to the cost
% function and gradient calculation
% xOpt - the optimal solution

eps = 0.1;
[f_x, g_x] = fcn(x);
H = eye(2); %for steepest descent assume hessian is identity matrix
p = -H*g_x; %descent direction
alpha = 1; %prescaler for descent direction
k = 1;

while norm(g_x) > sqrt(eps)

x(:,k+1) = x(k) + alpha*p;

k = k + 1;
alpha = alpha/2;
X(:,k) = x(:,k);
[f_x, g_x] = fcn(X);
p = -H*g_x;

if k == length(x)
    break
end
end 
xOpt = min(X);
end