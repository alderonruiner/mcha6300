//mex -O sqrtKFGivens.c
#include "mex.h"
#include "math.h"
#define varint mwSignedIndex
#include "blas.h"

typedef struct{
    int N; //number of  (measurements) time samples 
    int nx; //number of states
    int nu; //number of inputs
    int ny; //number of outputs
    double *u; // input matrix
    double *y; // output matrix
    double *A; //system matrix A
    double *B; //system matrix B
    double *C; //system matrix C
    double *D; //system matrix D
    double *x1; //initial predicted state mean
    double *P1; //upper triangular matrix such that M.P1'*M.P1 provides initial state covariance
    double *xp; //predicted state 
    double *xf; //filtered state
    double *Pp; //a 3D matrix where Pp(:,:,t) is the square-root of the predicted state covariance
    double *Pf; //a 3D matrix where Pf(:,:,t) is the square-root of the filtered state covariance
    double *Q; //upper triang chol factors of process noise
    double *R; //upper triang chol factors of measurement noise
}zData;

/* Define c function */

void kf(zData *zdat){
 int N = zdat->N;
 int nx = zdat->nx;
 int nu = zdat->nu;
 int ny = zdat->ny;
 double u = zdat->u;
 double y = zdat->y;
 
 double A = zdat->A;
 double B = zdat->B;
 double C = zdat->C;
 double D = zdat->D;
 
 double x1 = zdat->x1;
 double P1 = zdat->P1;
 double xp = zdat->xp;
 double xf = zdat->xf;
 double Pp = zdat->Pp;
 double Pf = zdat->Pf;
 
 //initialise counting integers
 int i,j,k,t;
 
 //Initial state and covariance
 x = x1;         
 k = 0;
     for (i=1;i<11;++i){
         for (i=1;i<11;++j){
             P[i][j] = M.P1[k];
         k = k + 1;
         }
     }


 for (t=1;t<(N+1);++t)
 {
     
     /* Useful LAPACK/BLAS functions */
     drotg() // setup givens rotations
     drotg(DA,DB,C,S)
     drot() // apply givens rotations
     drot(N,DX,INCX,DY,INCY,C,S)
     dgemv() // matrix vector multiply
     dgemv('TRANS', M,N,ALPHA, A,LDA,XLINCX, BETA,Y,INCY)
     dgemm() // matrix matrix multiply
     dgemm('T','N',num_rows_A, num_columns_B, num_rows_B/num_cols_a, scalar_ALPHA, A, first_dim_A, B, first_dim,B, scalar_BETA, first_dim_C)
     dpotrf() //cholesky factoristion 
     dpotrf('UPLO',N,A,LDA,INFO)
     
     /* Reform matrices and vectors from memory */
     
     // form vector A
     k = 0;
     for (i=(10*t-9);i<(100*t+1);++i){
         k=k+1;
         Av[k] = A[i];
     }
     
     // form matrix A
     k = 0;
     for (i=1;i<11;++i){
         for (i=1;i<11;++j){
             At[i][j] = Av[k];
         k = k + 1;
         }
     }
     
          //form vector B
     k = 0;
     for (i=(20*t-19);i<(20*t+1);++i){
         Bv[k] = B[i];
         k = k + 1;
     }
     
     // form matrix B
     k = 0;
     for (i=1;i<3;++i){
         for (i=1;i<11;++j){
             Bt[i][j] = Bv[k];
         k = k + 1;
         }
     }
     
     //form vector C
     k = 0;
     for (i=(20*t-19);i<(20*t+1);++i){
         Cv[k] = C[i];
         k = k + 1;
     }
     
     // form matrix C
     k = 0;
     for (i=1;i<3;++i){
         for (i=1;i<11;++j){
             Ct[i][j] = Cv[k];
         k = k + 1;
         }
     }
     
     //form vector D
     k = 0;
     for (i=(4*t-3);i<(4*t+1);++i){
         Dv[k] = D[i];
         k = k + 1;
     }
     
     // form matrix D
     k = 0;
     for (i=1;i<3;++i){
         for (i=1;i<3;++j){
             Dt[i][j] = Dv[k];
         k = k + 1;
         }
     }
     
     
     /* Kalman Filter Algoritm starts here */
     
     /* Do QR of first thing */
        
     /* Determine PC' */
     
     PC = dgemm('N','T',10,10,10,1, P, 10, Ct, 10,1,10);
     
     /* form cal_R matrix */
     
     //Populate 2x2 R
     cal_R[1][1] = R[1];
     cal_R[1][2] = R[2];
     cal_R[2][1] = R[3];
     cal_R[2][2] = R[4];
     
     //Populate 2x10 zeros
     for(i=1;i<3;++i){
         for(j=3;j<13;++j){
             cal_R[i][j] = 0;
         }
     }

     //Populate 10x2 PC' matrix
     for(i=3;i<13;++i){
         for(j=1;j<3;++j){
             cal_R[i][j] = PC[i-2][j];
         }
     }
     
     //Populate 10x10 P matrix
     for(i=3;i<13;++i){
         for(j=3;j<13;++j){
             cal_R[i][j] = PC[i-2][j-2];
         }
     }
     
     // apply cholesky factorisation
     qrA = dpotrf('U', 12, cal_R, 12, INFO);
     
     
     
     
     /*Get cal_Rs */ 
     cal_R11 = //r(1:2,1:2);
     cal_R12 = //r(1:2,3:12);
     cal_R22 = //r(3:12,3:12));
     
     /* Pretend to understand what is going on with QR */
     
     /* Assign G.Pf */
     G.Pf = cal_R22;
     
     /* Find e */
     inv_cal_R22 = 
     yt = y(t)
    
     
     //Form C*x
     Cx = dgemm('N','N',numRowsA,numColsB,numColsA,1, Ct, LDA, x, LDB,1,LDC)

     ut = u[t];
     
     //Form D*u
     Du = dgemm('N','N',numRowsA,numColsB,numColsA,1, Dt, LDA, u, LDB,1,LDC)
     
     B = yt - Cx - Du;
     e = dgemm('T','N',numRowsA,numColsB,numColsA,1, inv_cal_R22, LDA, B, LDB,1,LDC)
     
     /* Find and assign G.xf */
     G.xf = x + dgemm('T','N',numRowsA,numColsB,numColsA,1, cal_R22, LDA, e, LDB,1,LDC)
     
     /* Do QR of other thing */
     qrA = 
     dpotrf('U', 12, qrA, 12, INFO)
     
     /* Get cal_R_bar */
     cal_R_bar = 
     /* Find and assign G.Pp */
     cal_R_bar = //q(1:10,1:10);
     G.Pp = cal_R_bar;
     /* Find and assign G.xp */
     

     
     G.xp = dgemm('T','N',numRowsA,numColsB,numColsA,1, A, LDA, xf, LDB,1,LDC) + dgemm('T','N',numRowsA,numColsB,numColsA,1, Bt, LDA, u, LDB,1,LDC);
     
     /* Set new initial state and P1 */
     x = G.xp;
     P = cal_R_bar;
 }
}
/*-------------------------------------------------------------------------*/
double * add3d(mxArray *structure, char *name, varint size1, varint size2, varint size3) 
{
  double *ptr; 
  mwSize dims[3], numd;
  const mwSize *dimd;   
  mxArray *tmp;
  tmp = mxGetField(structure, 0, name);
  if (tmp!=NULL){
    ptr = mxGetPr(tmp); 
    numd = mxGetNumberOfDimensions(tmp);
    if (numd==3) {
      dimd = mxGetDimensions(tmp);
      if((size1!=dimd[0]) || (size2!=dimd[1]) || (size3!=dimd[2])){
        mxDestroyArray(tmp);
        dims[0] = size1; dims[1] = size2; dims[2] = size3;
        tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
        ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
      }
    } else {
      mxDestroyArray(tmp);
      dims[0] = size1; dims[1] = size2; dims[2] = size3;
      tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
      ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
    }
  }
  else {
    mxAddField(structure, name);
    dims[0] = size1; dims[1] = size2; dims[2] = size3;
    tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
    ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
  }
  return ptr;
}
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
double * add2d(mxArray *structure, char *name, varint size1, varint size2) 
{
  double *ptr;
  mxArray *tmp;
  tmp = mxGetField(structure, 0, name);
  if (tmp!=NULL){
    ptr = mxGetPr(tmp);
    if((size1!=mxGetM(tmp)) || (size2!=mxGetN(tmp))){
      mxDestroyArray(tmp);
      tmp = mxCreateDoubleMatrix(size1, size2, mxREAL);
      ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
    }
  }
  else {
    mxAddField(structure, name);
    tmp = mxCreateDoubleMatrix(size1, size2, mxREAL);
    ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
  }
  return ptr;
}
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
void printvector(varint size, const double * vec, const char * name) {
  varint i;
  if (size > 0) {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf(" = \n\n");
    for(i=0;i<size;i++){
      if (vec[i] >= 0.0){mexPrintf("     %3.5e\n", vec[i]);}
      else {mexPrintf("     %3.5e\n", vec[i]);}
    }
    mexPrintf("\n\n");
  }
  else {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf("= []");
  }
}
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
void printmatrix(varint m, varint n, const double * A, varint lda, const char * name) {
  varint i, j;
  if (m*n > 0) {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf(" = \n\n�);
    for(i=0;i<m;i++){
      for(j=0;j<n;j++){
        if (A[i+j*lda]>0.0){mexPrintf("  %3.4e�, A[i+j*lda]);}
        else if (A[i+j*lda]<0.0){mexPrintf(" %3.4e�, A[i+j*lda]");}
        else {mexPrintf("  0     ");}
      }
      mexPrintf("\n");
    }
    mexPrintf("\n\n");
  }
  else {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf("= []");
  }
}
/*-------------------------------------------------------------------------*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  //Input variables 
  double *y, *u, *A, *B, *C, *D, *Q, *R, *P1, *x1;
  //Output variables
  double *xp, *xf, *Pp, *Pf;
  varint numda, numdb, numds, numdq, ldh, itmp1;
  mwSize dims[3], numd;
  const mwSize *dimd;
  varint nu; //Number of inputs
  varint ny; //Number of outputs
  varint nx; //Number of states
  varint N;  //Number of measurements
  mxArray *tmp, *ss, *ssg;
  /* Check that we have the correct number of arguments */
  if(nrhs != 2){
    mexErrMsgTxt("There must be two arguments to this function.");
    return;
  }
  if(nlhs > 1){
    mexErrMsgTxt("Too many output arguments.");
    return;
  }
  /* Get output Data */
  tmp = mxGetField(prhs[0], 0, "y");
  if (tmp!=NULL){
    y = mxGetPr(tmp); 
    ny = mxGetM(tmp); 
    N = mxGetN(tmp);
  }
  else {
    mexErrMsgTxt("No such field: Z.y"); return;
  }
  /* Get input Data */
  tmp = mxGetField(prhs[0], 0, "u");
  if (tmp!=NULL){
    u = mxGetPr(tmp); 
    nu = mxGetM(tmp);
  } else {
    nu=0;
  }
  if (ny<1){
    mexErrMsgTxt("According to the data, there are no outputs. Nothing to do."); return;
  }
  /* Now the model variables */
  /* First thing is to duplicate M into G, and then manipulate G */
  plhs[0] = mxDuplicateArray(prhs[1]);
  //Get pointer to G
  ss = plhs[0];
  tmp = mxGetField(ss, 0, "A");
  if (tmp!=NULL){A = mxGetPr(tmp); nx = (varint)mxGetM(tmp);}
  else {mexErrMsgTxt("No such field: M.A); return;}
  numd = mxGetNumberOfDimensions(tmp);
  numda = (varint)numd;
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=N){
      mexErrMsgTxt("The A matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "B");
  if (tmp!=NULL){B = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.B"); return;}
  numd = mxGetNumberOfDimensions(tmp);
  numdb = (varint)numd;
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=N){
      mexErrMsgTxt("The B matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "C");
  if (tmp!=NULL){C = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.C"); return;}
  numd = mxGetNumberOfDimensions(tmp);
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=N){
      mexErrMsgTxt("The C matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "D");
  if (tmp!=NULL){D = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.D"); return;}
  numd = mxGetNumberOfDimensions(tmp);
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=N){
      mexErrMsgTxt("The D matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "Q");
  if (tmp!=NULL){Q = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.Q"); return;}
  numd = mxGetNumberOfDimensions(tmp);
  numdq = (varint)numd;
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=N){
      mexErrMsgTxt("The Q matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "R");
  if (tmp!=NULL){R = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.R"); return;}
  numd = mxGetNumberOfDimensions(tmp);
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=N){
      mexErrMsgTxt("The R matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "P1");
  if (tmp!=NULL){P1 = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.P1"); return;}
  tmp = mxGetField(ss, 0, "x1");
  if (tmp!=NULL){x1 = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.ss.x1"); return;}
  /* Add fields to various structures */
  xp = add2d(ss, "xp", nx, N+1);
  xf = add2d(ss, "xf", nx, N);
  Pp = add3d(ss, "Pp", nx, nx, N+1);
  Pf = add3d(ss, "Pf", nx, nx, N);
  
  zData zdat;
  zdat.N = N;
  
  
  /* WRITE KALMAN FILTER KFg*/
  kf(&zdat);//y,u,A,B,C,D,x1,P1,XP,XF,pP,pF
  
}