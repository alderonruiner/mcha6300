//mex -O sqrtKFrewrap.c -lmwblas -lmwlapack
#include "mex.h"
#include "math.h"
#include "blas.h"
#include "lapack.h"
#define varint mwSignedIndex
void printmatrix(varint m, varint n, const double * A, varint lda, const char * name);
void QRGivens(double *arr, varint col, varint row);

// Create struct and fill with the parameters
typedef struct {
    int N;
    varint nx;
    varint nu;
    varint ny;
    double *u;
    double *y;
    double *A, *B, *C, *D, *x1, *P1, *xp, *xf, *Pp, *Pf;
    double *Q, *R;
} zData;
        
 
 
/*-------------------------------------------------------------------------*/
double * add3d(mxArray *structure, char *name, varint size1, varint size2, varint size3) 
{
  double *ptr;
  mwSize dims[3], numd;
  const mwSize *dimd;   
  mxArray *tmp;
  tmp = mxGetField(structure, 0, name);
  if (tmp!=NULL){
    ptr = mxGetPr(tmp); 
    numd = mxGetNumberOfDimensions(tmp);
    if (numd==3) {
      dimd = mxGetDimensions(tmp);
      if((size1!=dimd[0]) || (size2!=dimd[1]) || (size3!=dimd[2])){
        mxDestroyArray(tmp);
        dims[0] = size1; dims[1] = size2; dims[2] = size3;
        tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
        ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
      }
    } else {
      mxDestroyArray(tmp);
      dims[0] = size1; dims[1] = size2; dims[2] = size3;
      tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
      ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
    }
  }
  else {
    mxAddField(structure, name);
    dims[0] = size1; dims[1] = size2; dims[2] = size3;
    tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
    ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
  }
  return ptr;
}
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
double * add2d(mxArray *structure, char *name, varint size1, varint size2) 
{
  double *ptr;
  mxArray *tmp;
  tmp = mxGetField(structure, 0, name);
  if (tmp!=NULL){
    ptr = mxGetPr(tmp);
    if((size1!=mxGetM(tmp)) || (size2!=mxGetN(tmp))){
      mxDestroyArray(tmp);
      tmp = mxCreateDoubleMatrix(size1, size2, mxREAL);
      ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
    }
  }
  else {
    mxAddField(structure, name);
    tmp = mxCreateDoubleMatrix(size1, size2, mxREAL);
    ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
  }
  return ptr;
}
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
void printvector(varint size, const double * vec, const char * name) {
  varint i;
  if (size > 0) {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf(" = \n\n");
    for(i=0;i<size;i++){
      if (vec[i] >= 0.0){mexPrintf("     %3.5e\n", vec[i]);}
      else {mexPrintf("     %3.5e\n", vec[i]);}
    }
    mexPrintf("\n\n");
  }
  else {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf("= []");
  }
}
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
void printmatrix(varint m, varint n, const double * A, varint lda, const char * name) {
  varint i, j;
  if (m*n > 0) {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf(" = \n\n");
    for(i=0;i<m;i++){
      for(j=0;j<n;j++){
        if (A[i+j*lda]>0.00000001){mexPrintf("  %3.4e", A[i+j*lda]);}
        else if (A[i+j*lda]<-0.00000001){mexPrintf(" %3.4e", A[i+j*lda]);}
        else {mexPrintf("  0     ");}
      }
      mexPrintf("\n");
    }
    mexPrintf("\n\n");
  }
  else {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf("= []");
  }
}



// Create sqrt kalman filter ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void kf(zData *zdat){
    
 int N = zdat->N;
 varint nx = zdat->nx;
 varint nu = zdat->nu;
 varint ny = zdat->ny;
 double *u = zdat->u;
 double *y = zdat->y;
 
 double *A = zdat->A;
 double *B = zdat->B;
 double *C = zdat->C;
 double *D = zdat->D;
 
 double *R = zdat->R;
 double *Q = zdat->Q; 
 
 double *x1 = zdat->x1;
 double *xp = zdat->xp;
 double *xf = zdat->xf;
 
 double *P1 = zdat->P1;
 double *Pp = zdat->Pp;
 double *Pf = zdat->Pf;
 
 double alp = 1.0, beta = 0.0;
 
 //initialise counting integers
 int i,j,k,t;
 varint length = (nx+ny);
 varint num_row = 1;
 varint lda = (nx+ny);

//initialise matrices as doubles
double *PC = malloc((nx*ny)*sizeof(double)); //arr_Pp_C
double *cal_R = malloc(((ny+nx)*(ny+nx))*sizeof(double)); //arr1
double *PA = malloc(((nx*nx))*sizeof(double)); //pA
double *bar = malloc(((nx+nx)*nx)*sizeof(double)); //arr2
double *cal_R11  = malloc((ny*ny)*sizeof(double)); //R11
double *cal_R12  = malloc((nx*ny)*sizeof(double)); //R12
double *Cx  = malloc((ny)*sizeof(double)); //Cx
double *Du  = malloc((ny)*sizeof(double)); //Du
double *Ax  = malloc((nx*sizeof(double))); //Ax
double *Bu  = malloc((nx*sizeof(double))); //Bu
double *S = malloc((nx*sizeof(double))); //R12e
double *zz  = malloc((ny*sizeof(double))); //err

 
//initialiase chars
const char T = 'T';
const char U = 'U';
const char n = 'N';


// initialise xp with x1
 for(i=0; i<nx; ++i){
	 xp[i] = x1[i];
 }
  
  
//initialise Pp with P1
 for(i=0;i<nx;++i)
    {
        for(j=0;j<nx;++j)
        {
            Pp[i+j*nx] = P1[i+j*nx];
        }
    }


/*********************************************************** KF LOOP **************************************************/
/* Kalman Filter Algorithm starts here */
 for (t=0;t<N;++t)
 {     
	// Determine the values to offset the matrix by. Get the 'depths' of the matrix
        int off1 = t*nx*nx;
        int off2 = t*nx*ny;
        int off3 = t*num_row*nu; 
        int off4 = t*nu*ny; 
        int off5 = t*nx;
        int off6 = t*nx*nu; 
		int off7 = (t + 1)*nx;
        int off8 = (t + 1)*nx*nx;
        
     /* Do QR of first thing */
     /* Determine PC' */
     dgemm(&n,&T,&nx,&ny,&nx,&alp, Pp + off1, &nx, C + off2, &ny, &beta, PC, &nx); 
	 
     /* Form QR matrix */
	 //Populate with zeros
	 for(i=0; i<(lda*lda); ++i){
          cal_R[i] = 0.0;
        }
	 
     //Populate 2x2 R
     
     k = 0;
     for(i=0;i<ny;++i){
         for(j=0;j<ny;++j){
             cal_R[j+lda*i] = R[k + t*ny*ny];
             k = k + 1;
         }
     }
     
     
     //Populate 10x2 PC' matrix
     k = 0;
     for(i=0;i<ny;++i){
         for(j=0;j<nx;++j){
             cal_R[ny+j+lda*i] = PC[k];
			 k = k + 1;
         }
     }
     
     //Populate 10x10 P matrix
     k = 0;
     for(i=0;i<nx;++i){
         for(j=0;j<nx;++j){
             cal_R[ny*lda+ny+j+lda*i] = Pp[k + t*nx*nx];
             k = k + 1;
         }
     }
	 
	 
     //printmatrix(nx, num_row, cal_R, nx, "cal_R");
	 
     // Qless QR factorisation with givens rotation
     QRGivens(cal_R, length, length);
	
     /*Get cal_Rs from qrA*/
     
     //Cal_R11 2x2
     k = 0;
     for(i=0;i<ny;++i){
         for(j=0;j<ny;++j){
             cal_R11[k] = cal_R[j + i*lda];
             k = k + 1;
         }
     }
     
     
     //Cal_R12 2x10
     k = 0;
     for(i=ny;i<nx+ny;++i){
         for(j=0;j<ny;++j){
             cal_R12[k] = cal_R[k + t*ny*nx];
             k = k + 1;
         }
     }

     
     /* Assign G.Pf */
    //G.Pf = cal_R22;
	k = 0;
	for(int i=ny; i<nx+ny; ++i)
        {
            for(int j=ny; j<nx+ny; ++j)
            {
                Pf[k + off1] = cal_R[j + i*lda];
                k = k + 1;
            }
        }  
		
      
     //Form D*u
     dgemm(&n,&n,&ny,&num_row,&nu,&alp, D+off4, &ny, u+off3, &nu,&beta, Du,&ny); 
    
	//Form C*x
     dgemm(&n, &n, &ny, &num_row, &nx, &alp, C+off2, &ny, xp+off5, &nx, &beta, Cx, &nx); 
	 
	 
     //Form e - use backsolving 
     //zz = yt - Cx - Du;
	 for(i=0;i<ny;++i){
		 zz[i] = y[i+t*ny] - Cx[i] - Du[i]; 
	 } 
	 
     dtrsv(&U, &T, &n, &ny, cal_R11, &ny, zz, &num_row); 
	 
	 dgemm(&T, &n, &nx, &num_row, &ny, &alp, cal_R12, &ny, zz, &ny, &beta, S, &nx); 
	 
	 //determine xf
     for(i=0;i<nx;++i){
		 xf[i+t*nx] = xp[i+t*nx] + S[i];
 	 }
	 
	 //Form PA
     dgemm(&n, &T, &nx, &nx, &nx, &alp, Pf + off1, &nx, A + off1, &nx, &beta, PA, &nx);
	 
     /* Form bar matrix for QR */
     k = 0;
     for(i=0;i<nx;++i){
         for(j=0;j<nx;++j){
             bar[j+(nx+nx)*i] = PA[k];
             k = k + 1;
         }
     }
	 
	 k = 0;
     for(i=0;i<nx;++i){
         for(j=nx;j<(nx+nx);++j){
             bar[j+i*(nx+nx)] = Q[k + off1];
             k = k + 1;
         }
     }
	 
	 /* Do QR of other thing */
     QRGivens(bar, (nx+nx), nx);
	 
     /* Find and assign G.xp */
            
     dgemm(&n,&n,&nx,&num_row,&nx,&alp, A + off1, &nx, xf + off5, &nx, &beta, Ax,&nx); 
	 dgemm(&n,&n,&nx,&num_row,&nu,&alp, B + off6, &nx, u + off3, &nu,&beta, Bu, &nx); 
     
    
     for(i=0;i<nx;++i){
		xp[i+off7] =  Ax[i] + Bu[i];
	 }
    
     /* Find and assign G.Pp */
     for(	i=0; i<nx; ++i){
          for(int j=0; j<nx; ++j){
            Pp[j+ (i*nx) + off8] = bar[j + i*(nx+nx)];
          }
        }
     
	}
return;
}

void QRGivens(double *arr, varint num_row, varint num_col){
    // Declare size of varibles that are passed into the givens rotation
    double x, y, c, s;

    // Start from the bottom of the array and go up the rows and rotate each index of the array until you get to the top
    for(int i=0; i<num_col; ++i){
        for(int j=num_row-1; j>i; --j){
            // Make the x and y 'vector' to go into the givens rotation
            x = arr[j-1 + i*num_row];
            y = arr[j + i*num_row];

            // Calculate the number of elements in input vector
            varint col_idx = num_col-i;

            // drotg sets up the rotation 
            drotg(&x, &y, &c, &s);
            
            // drot does the rotation
            drot(&col_idx, &arr[j-1 + i*num_row], &num_row, &arr[j + i*num_row], &num_row, &c, &s);
        }
    }
}

/*-------------------------------------------------------------------------*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  //Output variables
  varint numda, numdb, numds, numdq, ldh, itmp1;
  mwSize dims[3], numd;
  const mwSize *dimd;
  
  mxArray *tmp, *ss, *ssg;
  
  /* Check that we have the correct number of arguments */
  if(nrhs != 2){
    mexErrMsgTxt("There must be two arguments to this function.");
    return;
  }
  if(nlhs > 1){
    mexErrMsgTxt("Too many output arguments.");
    return;
  }
  // Structure identifiers
   zData Z;
  
  /* Get output Data */
  tmp = mxGetField(prhs[0], 0, "y");
  if (tmp!=NULL){
    Z.y = mxGetPr(tmp); 
    Z.ny = mxGetM(tmp); 
    Z.N = mxGetN(tmp);
  }
  else {
    mexErrMsgTxt("No such field: Z.y"); return;
  }
  
  /* Get input Data */
  tmp = mxGetField(prhs[0], 0, "u");
  if (tmp!=NULL){
    Z.u = mxGetPr(tmp); 
    Z.nu = mxGetM(tmp);
  } else {
    Z.nu = 0;
  }
  if (Z.ny<1){
    mexErrMsgTxt("According to the data, there are no outputs. Nothing to do."); return;
  }
  
  /* Now the model variables */
  /* First thing is to duplicate M into G, and then manipulate G */
  plhs[0] = mxDuplicateArray(prhs[1]);
  
  //Get pointer to G
  ss = plhs[0];
  tmp = mxGetField(ss, 0, "A");
  
  if (tmp!=NULL){
    Z.A = mxGetPr(tmp); 
    Z.nx = (varint)mxGetM(tmp);
  }
  else {mexErrMsgTxt("No such field: M.A"); 
    return;
  }
  numd = mxGetNumberOfDimensions(tmp);
  numda = (varint)numd;
  
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=Z.N){
      mexErrMsgTxt("The A matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  
  tmp = mxGetField(ss, 0, "B");
  
  if (tmp!=NULL){Z.B = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.B"); return;}
  
  numd = mxGetNumberOfDimensions(tmp);
  numdb = (varint)numd;
  
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=Z.N){
      mexErrMsgTxt("The B matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "C");
  
  if (tmp!=NULL){Z.C = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.C"); return;}
  
  numd = mxGetNumberOfDimensions(tmp);
  
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=Z.N){
      mexErrMsgTxt("The C matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  
  tmp = mxGetField(ss, 0, "D");
  
  if (tmp!=NULL){Z.D = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.D"); return;}
  
  numd = mxGetNumberOfDimensions(tmp);
  
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=Z.N){
      mexErrMsgTxt("The D matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  
  tmp = mxGetField(ss, 0, "Q");
  
  if (tmp!=NULL){Z.Q = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.Q"); return;}
  numd = mxGetNumberOfDimensions(tmp);
  numdq = (varint)numd;
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=Z.N){
      mexErrMsgTxt("The Q matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "R");
  if (tmp!=NULL){Z.R = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.R"); return;}
  
  numd = mxGetNumberOfDimensions(tmp);
  
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=Z.N){
      mexErrMsgTxt("The R matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "P1");
  if (tmp!=NULL){Z.P1 = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.P1"); return;}
  tmp = mxGetField(ss, 0, "x1");
  if (tmp!=NULL){Z.x1 = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.ss.x1"); return;}
          
  /* Add fields to various structures */
  Z.xp = add2d(ss, "xp", Z.nx, Z.N+1);
  Z.xf = add2d(ss, "xf", Z.nx, Z.N);
  Z.Pp = add3d(ss, "Pp", Z.nx, Z.nx, Z.N+1);
  Z.Pf = add3d(ss, "Pf", Z.nx, Z.nx, Z.N);
  
  // call sqrt kalman filter - call BLAS routines GROT and GROTD
 
  kf(&Z);
  
}