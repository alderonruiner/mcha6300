function [A] = qlqrgr(A)
%QRHR QR Decomposition using Givens Rotations.
%   [Q,R] = QRHR(A) performs a qr decomposition on m-by-n matrix A such
%   that A = Q*R. The factor R is an m-by-n upper triangular matrix and Q
%   is an m-by-m unitary matrix.
%

[m,n] = size(A);
for i = 1:n
    for j = m:-1:i+1
        % G = givens(A(j-1,i),A(j,i));
        G = givens(A(j-1,i),A(j,i));
        A(j-1:j,:) = G*A(j-1:j,:);
    end
end