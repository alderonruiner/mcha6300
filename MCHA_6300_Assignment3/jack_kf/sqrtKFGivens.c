//clc; mex -O sqrtKFGivens.c -lmwblas -lmwlapack

#include "mex.h"
#include "blas.h"
#include "lapack.h"
#include <math.h>

typedef struct {
    double *N, *u, *y;
    mwSignedIndex nx, nu, ny, nxy, nxx;   // Number of States, Inputs and Outputs and lda
    
    // Variables for dgemm 
    double alpha, beta;
    mwSignedIndex one, zero, temp; 
} Data;

typedef struct {
    double *A, *B, *C, *D, *Q, *R, *P1, *x1;
    
    mwSignedIndex numda, numdb, numds, numdq;
} Model;

typedef struct {
    double *Pp, *Pf, *xp, *xf;
} Output;

/* Add 3D matrix to structure */
double *add3d(mxArray *structure, char *name, mwSignedIndex size1, mwSignedIndex size2, mwSignedIndex size3) {
    double *ptr;
    mwSize dims[3], numd;
    const mwSize *dimd;
    mxArray *tmp;
    tmp = mxGetField(structure, 0, name);
    if (tmp != NULL) {
        ptr = mxGetPr(tmp);
        numd = mxGetNumberOfDimensions(tmp);
        if (numd == 3) {
            dimd = mxGetDimensions(tmp);
            if ((size1 != dimd[0]) || (size2 != dimd[1]) || (size3 != dimd[2])){
                mxDestroyArray(tmp);
                dims[0] = size1; dims[1] = size2; dims[2] = size3;
                tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
                ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
            }
        } 
        else {
            mxDestroyArray(tmp);
            dims[0] = size1; dims[1] = size2; dims[2] = size3;
            tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
            ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
        }
    }
    else {
        mxAddField(structure, name);
        dims[0] = size1; dims[1] = size2; dims[2] = size3;
        tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
        ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
    }
    return ptr;
}

/* Add 2D matrix to structure */
double *add2d(mxArray *structure, char *name, mwSignedIndex size1, mwSignedIndex size2) {
    double *ptr;
    mxArray *tmp;
    tmp = mxGetField(structure, 0, name);
    if (tmp != NULL) {
        ptr = mxGetPr(tmp);
        if ((size1 != mxGetM(tmp)) || (size2 != mxGetN(tmp))) {
            mxDestroyArray(tmp);
            tmp = mxCreateDoubleMatrix(size1, size2, mxREAL);
            ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
        }
    }
    else {
        mxAddField(structure, name);
        tmp = mxCreateDoubleMatrix(size1, size2, mxREAL);
        ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
    }
    return ptr;
}

/* Print matrix from c to matlab */
void printmatrix(mwSignedIndex m, mwSignedIndex n, const double * A, mwSignedIndex lda, const char * name) {
    mwSignedIndex i, j;
    if (m*n > 0) {
        mexPrintf("\n ");
        mexPrintf(name);
        mexPrintf(" = \n\n");
        for(i = 0; i < m; i++) {
            for(j = 0; j < n; j++) {
                if (A[i + j*lda] > 0.000000001) {
                    mexPrintf("  %3.4e", A[i + j*lda]);
                }
                else if (A[i + j*lda] < -0.000000001) {
                    mexPrintf(" %3.4e", A[i + j*lda]);
                }
                else {
                    mexPrintf("  0     ");
                }
            }
        mexPrintf("\n");
        }   
    mexPrintf("\n\n");
    }
    else {
        mexPrintf("\n ");
        mexPrintf(name);
        mexPrintf("= []");
    }
}

/* Print vector from c to matlab */
void printvector(mwSignedIndex size, const double * vec, const char * name) {
    mwSignedIndex i;
    if (size > 0) {
        mexPrintf("\n ");
        mexPrintf(name);
        mexPrintf(" = \n\n");
        for (i = 0; i < size; i++) {
            if (vec[i] >= 0.0) {
                mexPrintf("     %3.5e\n", vec[i]);
            }
            else {
                mexPrintf("     %3.5e\n", vec[i]);
            }
        }
        mexPrintf("\n\n");
    }
    else {
        mexPrintf("\n ");
        mexPrintf(name);
        mexPrintf("= []");
    }
}

/* Qless QR Decomposition Using Givens Rotation*/
void qlqrgivens(double *A, mwSignedIndex row, mwSignedIndex col) {
    double a, b, c, s;
    mwSignedIndex n;
    for(int i = 0; i < col; i++) {
        for(int j = row - 1; j > i; j--) {
            a = A[j-1+i*row];
            b = A[j+i*row];
            n = col - i;
            drotg(&a, &b, &c, &s);
            drot(&n, &A[j-1+i*row], &row, &A[j+i*row], &row, &c, &s);
        }
    }
}

/* Square root Kalman Filter Using Givens Rotation QR Factorisation */
void sqrtKFGivens(Data *data, Model *model, Output *output) {
    // Initialise initial variables
    for (int i = 0; i < data->nx; i++) {
        for (int j = 0; j < data->nx; j++) {
            output->Pp[j + i*(data->nx)] = model->P1[j + i*(data->nx)];
        }
    }
    
    for (int i = 0; i < data->nx; i++) {
        output->xp[i] = model->x1[i];
    }
    
    // Variables
    int tmp, i, j, t;
    data->nxy = data->nx + data->ny;
    data->nxx = data->nx + data->nx;
    
    tmp = (data->nxy)*(data->nxy);
    double *Rm = calloc(tmp, sizeof(double));
    
    tmp = (data->nx + data->nx)*(data->nx);
    double *Rp = calloc(tmp, sizeof(double));
    
    tmp = (data->nxx)*(data->nxx);
    double *tempmat = calloc(tmp, sizeof(double));
    
    tmp = (data->ny)*(data->ny);
    double *R11 = calloc(tmp, sizeof(double));
    
    tmp = (data->ny)*(data->nx);
    double *R12 = calloc(tmp, sizeof(double));
    
    tmp = (data->nx)*(data->nx);
    double *R22 = calloc(tmp, sizeof(double));
    
    // dgemm variables
    data->alpha = 1.0;
    data->beta  = 0.0;
    data->one   = 1;
    data->zero  = 0;
    
    // Time loop
    for (t = 0; t < *data->N; t++) {
           
        // Calculate memory bias relative to time
        int bias1 = t*(data->nx)*(data->nx);
        int bias2 = t*(data->nx)*(data->ny);
        int bias3 = t*(data->ny)*(data->ny);
        int bias4 = t*(data->ny);
        int bias5 = t*(data->nx);
        int bias6 = (t + 1)*(data->nx)*(data->nx);
        int bias7 = (t + 1)*(data->nx);
        int bias8 = t*(data->ny)*(data->nu);
        int bias9 = t*(data->nu);
        int bias10 = t*(data->nu)*(data->nx);
        
        // Intialise R measurents with zeros
        for (i = 0; i < data->nxy; i++) {
            for (j = 0; j < data->nxy; j++) {
                Rm[j + i*data->nxy] = 0.0;
            }
        }
        
        /* Measurement Update Step */
        
        // Assign R to upper left quadrant
        for (i = 0; i < data->ny; i++) {
            for (j = 0; j < data->ny; j++) {
                Rm[j + i*data->nxy] = model->R[j + i*data->ny + bias3];
            }
        }
        
        // Calculate Pp*C'
        dgemm("N", "T", &data->nx, &data->ny, &data->nx, &data->alpha, output->Pp + bias1, &data->nx, model->C + bias2, &data->ny, &data->beta, tempmat, &data->nx);
        
        // Assign Pp*C' to lower left quadrant
        for (i = 0; i < data->ny; i++) {
            for (j = 0; j < data->nx; j++) {
                Rm[data->ny + j + i*data->nxy] = tempmat[j + i*data->nx];
            }
        }
        
        // Assign Pp to lower right quadrant
        for (i = 0; i < data->nx; i++) {
            for (j = 0; j < data->nx; j++) {
                Rm[data->ny + j + (data->ny + i)*data->nxy] = output->Pp[j + i*data->nx + bias1];
            }
        }
        
        // Qless QR Decomposition of Rm
        qlqrgivens(Rm, data->nxy, data->nxy);
        
        // Create R11
        for (i = 0; i < data->ny; i++) {
            for (j = 0; j < data->ny; j++) {
                R11[j + i*data->ny] = Rm[j + i*data->nxy];
            }
        }
        
        // Create R12
        for (i = 0; i < data->nx; i++) {
            for (j = 0; j < data->ny; j++) {
                R12[j + i*data->ny] = Rm[j + (i + data->ny)*data->nxy];
            }
        }
        
        // Assign R22 to Pf
        for (i = 0; i < data->nx; i++) {
            for (j = 0; j < data->nx; j++) {
                output->Pf[j + i*data->nx + bias1] = Rm[(j + data->ny) + (i + data->ny)*data->nxy];
            }
        }
        
        // Calculate e = R11'\(Z.y(:,t) - M.C(:,:,t)*G.xp(:,t) - M.D(:,:,t)*Z.u(:,t));
        // C = M.D(:,:,t)*Z.u(:,t)
        dgemm("N", "N", &data->ny, &data->one, &data->nu, &data->alpha, model->D + bias8, &data->ny, data->u + bias9, &data->nu, &data->beta, tempmat, &data->ny);
        
        // C = M.C(:,:,t)*G.xp(:,t) + C
        dgemm("N", "N", &data->ny, &data->one, &data->nx, &data->alpha, model->C + bias2, &data->ny, output->xp + bias5, &data->nx, &data->alpha, tempmat, &data->nx);
        
        // C = Z.y(:,t) - C
        for(i = 0; i < data->ny; i++) {
            tempmat[i] = data->y[i + bias4] - tempmat[i];
        }
        
        // R11'\C
        dtrsv("U", "T", "N", &data->ny, R11, &data->ny, tempmat, &data->one);
        
        // G.xf(:,t) = G.xp(:,t) + R12'*e
        for (i = 0; i < data->nx; i++) {
            output->xf[i+bias5] = output->xp[i+bias5];
        }
        dgemm("T", "N", &data->nx, &data->one, &data->ny, &data->alpha, R12, &data->ny, tempmat, &data->ny, &data->alpha, output->xf + bias5, &data->nx);
        
        /* Prediction Update Step */
        
        // Assign G.Pf(:,:,t)*M.A(:,:,t)' to Rp
        dgemm("N", "T", &data->nx, &data->nx, &data->nx, &data->alpha, output->Pf + bias1, &data->nx, model->A + bias1, &data->nx, &data->beta, Rp, &data->nxx);
        
        // Assign M.Q(:,:,t) to Rp
        for (i = 0; i < data->nx; i++) {
            for (j = 0; j < data->nx; j++) {
                Rp[(j + data->nx) + i*data->nxx] = model->Q[j + i*data->nx];
            }
        }
        
        // Qless QR Decomposition of Rp
        qlqrgivens(Rp, data->nxx, data->nx);
        
        // G.Pp(:,:,t + 1) = R(1:nx, 1:nx)
        for (i = 0; i < data->nx; i++) {
            for (j = 0; j < data->nx; j++) {
                output->Pp[j + i*data->nx + bias6] = Rp[j + i*data->nxx];
            }
        }
        
        // G.xp(:,t + 1) = M.B(:,:,t)*Z.u(:,t)
        dgemm("N", "N", &data->nx, &data->one, &data->nu, &data->alpha, model->B + bias10, &data->nx, data->u + bias9, &data->nu, &data->beta, output->xp + bias7, &data->nx);

        // G.xp(:,t + 1) = M.A(:,:,t)*G.xf(:,t) + G.xp(:,t + 1)
        dgemm("N", "N", &data->nx, &data->one, &data->nx, &data->alpha, model->A + bias1, &data->nx, output->xf + bias5, &data->nx, &data->alpha, output->xp + bias7, &data->nx);
    }
    return;
}

/* Mex Function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    /* Check for proper inputs and outputs */
    if(nrhs != 2) {
        mexErrMsgIdAndTxt("MATLAB:sqrtKFGivens:invalidNumInputs",
                          "Two inputs required.");
        return;
    }
    
    if(nlhs > 1) {
        mexErrMsgIdAndTxt("MATLAB:sqrtKFGivens:maxlhs",
                          "Too many output arguments.");
        return;
    }
    
    if(!mxIsStruct(prhs[0]) || !mxIsStruct(prhs[1])) {
        mexErrMsgIdAndTxt("MATLAB:sqrtKFGivens:inputNotStruct",
                          "Inputs must be a structure.");
        return;
    }
    
    /* Declarations */    
    //Output variables
    mwSignedIndex ldh, itmp1;
    mwSize dims[3], numd;
    const mwSize *dimd;
    mxArray *tmp;
    
    // Structures
    Data Z;
    Model M;
    Output G;
    
    /* Mex Function Inputs */
    /* Import Z structure data */
    tmp = mxGetField(prhs[0], 0, "N");
    if (tmp != NULL) {
        Z.N = mxGetPr(tmp);
    }
    else {
        mexErrMsgTxt("No such field: Z.N");
        return;
    }
    
    // Input data
    tmp = mxGetField(prhs[0], 0, "u");
    if (tmp != NULL) {
        Z.u  = mxGetPr(tmp);
        Z.nu = mxGetM(tmp);
    }
    else {
        mexErrMsgTxt("No such field: Z.u");
        return;
    }
    
    // Output data
    tmp = mxGetField(prhs[0], 0, "y");
    if (tmp != NULL) {
        Z.y  = mxGetPr(tmp);
        Z.ny = mxGetM(tmp);
        if (Z.ny < 1) {
            mexErrMsgTxt("According to the data, there are no outputs. Nothing to do."); return;
        }
    }
    else {
        mexErrMsgTxt("No such field: Z.y");
        return;
    }
    
    /* Import M structure data */  
    // A Matrix
    tmp = mxGetField(prhs[1], 0, "A");
    if (tmp != NULL) {
        M.A  = mxGetPr(tmp);
        Z.nx = (mwSignedIndex)mxGetM(tmp);
    }
    else {
        mexErrMsgTxt("No such field: M.A");
        return;
    }
    numd = mxGetNumberOfDimensions(tmp);
    M.numda = (mwSignedIndex)numd;
    if (numd == 3) {
        dimd = mxGetDimensions(tmp);
        if (dimd[2] != *Z.N) {
            mexErrMsgTxt("The A matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
        }
    }
    
    // B Matrix
    tmp = mxGetField(prhs[1], 0, "B");
    if (tmp != NULL) {
        M.B = mxGetPr(tmp);
    }
    else {
        mexErrMsgTxt("No such field: M.B");
        return;
    }
    numd = mxGetNumberOfDimensions(tmp);
    M.numdb = (mwSignedIndex)numd;
    if (numd == 3) {
        dimd = mxGetDimensions(tmp);
        if (dimd[2] != *Z.N) {
            mexErrMsgTxt("The B matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
        }
    }
    
    // C Matrix
    tmp = mxGetField(prhs[1], 0, "C");
    if (tmp != NULL) {
        M.C = mxGetPr(tmp);
    }
    else {
        mexErrMsgTxt("No such field: M.C");
        return;
    }
    numd = mxGetNumberOfDimensions(tmp);
    if (numd == 3) {
        dimd = mxGetDimensions(tmp);
        if (dimd[2] != *Z.N) {
            mexErrMsgTxt("The C matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
        }
    }
    
    // D Matrix
    tmp = mxGetField(prhs[1], 0, "D");
    if (tmp != NULL) {
        M.D = mxGetPr(tmp);
    }
    else {
        mexErrMsgTxt("No such field: M.D");
        return;
    }
    numd = mxGetNumberOfDimensions(tmp);
    if (numd == 3) {
        dimd = mxGetDimensions(tmp);
        if (dimd[2] != *Z.N) {
            mexErrMsgTxt("The D matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
        }
    }
    
    // Q Matrix
    tmp = mxGetField(prhs[1], 0, "Q");
    if (tmp != NULL) {
        M.Q = mxGetPr(tmp);
    }
    else {
        mexErrMsgTxt("No such field: M.Q");
        return;
    }
    numd = mxGetNumberOfDimensions(tmp);
    M.numdq = (mwSignedIndex)numd;
    if (numd == 3) {
        dimd = mxGetDimensions(tmp);
        if (dimd[2] != *Z.N) {
            mexErrMsgTxt("The Q matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
        }
    }
    
    // R Matrix
    tmp = mxGetField(prhs[1], 0, "R");
    if (tmp != NULL) {
        M.R = mxGetPr(tmp);
    }
    else {
        mexErrMsgTxt("No such field: M.R");
        return;
    }
    numd = mxGetNumberOfDimensions(tmp);
    if (numd == 3) {
        dimd = mxGetDimensions(tmp);
        if (dimd[2] != *Z.N) {
            mexErrMsgTxt("The R matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
        }
    }
    
    // Initial State Covariance
    tmp = mxGetField(prhs[1], 0, "P1");
    if (tmp != NULL) {
        M.P1 = mxGetPr(tmp);
    }
    else {
        mexErrMsgTxt("No such field: M.P1");
        return;
    }
    
    // Initial Predicted State Mean
    tmp = mxGetField(prhs[1], 0, "x1");
    if (tmp != NULL) {
        M.x1 = mxGetPr(tmp);
    }
    else {
        mexErrMsgTxt("No such field: M.x1");
        return;
    }
    
    /* Ouput */
    const char *fieldnames[] = {"xp", "xf", "Pp", "Pf"};
    plhs[0] = mxCreateStructMatrix(1, 1, 4, fieldnames);
    
    /* Add fields to various structures */
    G.xp = add2d(plhs[0], "xp", Z.nx, *Z.N+1);  
    G.xf = add2d(plhs[0], "xf", Z.nx, *Z.N);
    G.Pp = add3d(plhs[0], "Pp", Z.nx, Z.nx, *Z.N+1);
    G.Pf = add3d(plhs[0], "Pf", Z.nx, Z.nx, *Z.N);
    
    /* Call Routine */
    sqrtKFGivens(&Z, &M, &G);
    
    /* Free memory */
    
}