function G = srkf(Z, M)

% Initialise
[ny, ~] = size(Z.y);
[nx, ~] = size(M.x1);

G.xp = nan(nx, Z.N + 1);
G.xf = nan(nx, Z.N);
G.Pp = nan(nx, nx, Z.N + 1);
G.Pf = nan(nx, nx, Z.N);

% Setup Variables
G.Pp(:,:,1) = M.P1;
G.xp(:,1) = M.x1;

for t = 1:Z.N
    
    % Measurement Update Step
%     [~, R] = qr([M.R(:,:,t), zeros(ny,nx);
%                  G.Pp(:,:,t)*M.C(:,:,t)', G.Pp(:,:,t)]);
    R = qlqrgr([M.R(:,:,t), zeros(ny,nx);
                G.Pp(:,:,t)*M.C(:,:,t)', G.Pp(:,:,t)]);
    
    R11 = R(1:ny, 1:ny);
    R12 = R(1:ny, ny + 1:nx + ny);
    R22 = R(ny + 1:nx + ny, ny + 1:nx + ny);
    G.Pf(:,:,t) = R22;
    e = R11'\(Z.y(:,t) - M.C(:,:,t)*G.xp(:,t) - M.D(:,:,t)*Z.u(:,t));
    G.xf(:,t) = G.xp(:,t) + R12'*e;
    
    % Prediction Update Step
%     [~, R] = qr([G.Pf(:,:,t)*M.A(:,:,t)';
%                  M.Q(:,:,t)]);
    R = qlqrgr([G.Pf(:,:,t)*M.A(:,:,t)';
                M.Q(:,:,t)]);
    
    R = R(1:nx, 1:nx);
    G.Pp(:,:,t + 1) = R;
    G.xp(:,t + 1) = M.A(:,:,t)*G.xf(:,t) + M.B(:,:,t)*Z.u(:,t);
end