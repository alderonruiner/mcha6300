//mex -O sqrtKFGivens.c -lmwblas -lmwlapack
//MCHA6300 Assignment3
//Alderon Ruiner - c3279096
#include "mex.h"
#include "math.h"
#define varint mwSignedIndex
#include "blas.h"

typedef struct{
    int N; //number of  (measurements) time samples 
    varint nx; //number of states
    varint nu; //number of inputs
    varint ny; //number of outputs
    double *u; // input matrix
    double *y; // output matrix
    double *A; //system matrix A
    double *B; //system matrix B
    double *C; //system matrix C
    double *D; //system matrix D
    double *x1; //initial predicted state mean
    double *P1; //upper triangular matrix such that M.P1'*M.P1 provides initial state covariance
    double *xp; //predicted state 
    double *xf; //filtered state
    double *Pp; //a 3D matrix where Pp(:,:,t) is the square-root of the predicted state covariance
    double *Pf; //a 3D matrix where Pf(:,:,t) is the square-root of the filtered state covariance
    double *Q; //upper triang chol factors of process noise
    double *R; //upper triang chol factors of measurement noise
}zData;

/* Define c function */

void kf(zData *zdat){
 int N = zdat->N;
 varint nx = zdat->nx;
 varint nu = zdat->nu;
 varint ny = zdat->ny;
 double *u = zdat->u;
 double *y = zdat->y;
 
 double *A = zdat->A;
 double *B = zdat->B;
 double *C = zdat->C;
 double *D = zdat->D;
 
 double *R = zdat->R;
 double *Q = zdat->Q; 
 double *x1 = zdat->x1;
 double *P1 = zdat->P1;
 double *xp = zdat->xp;
 double *xf = zdat->xf;
 double *Pp = zdat->P1;
 double *Pf = zdat->Pf;
 double alp = 1.0, beta = 1.0;
 
 //initialise counting integers
 int i,j,k,t, lda;
 
 //initialise matrices as doubles
 
double  At[nx*nx], Bt[nx*ny], Ct[nx*ny],Dt[ny*nx],PC[nx*nx],PA[2*nx*nx], cal_R[(ny+nx)*(ny+nx)], bar[nx*nx], cal_R_bar[nx*nx], cal_R11[ny*ny], cal_R12[nx*ny], cal_R22[nx*nx], inv_cal_R11[ny*ny], qrA[nx*nx], qrB[nx*nx], Cx[ny*nx], Du[ny*nx],e[ny],yt[ny],ut[ny], Ax[nx], Bu[nx*ny], zz[ny], S[nx];
 
//initialiase chars
const char T = 'T';
const char U = 'U';
const char n = 'N';


 //Initial state and covariance
 double x;         
 double P;
 x = *x1;
 P = *P1;
 for (t=0;t<(N);++t)
 {     
     /* Reform matrices into vectors from memory */
    
     // form matrix A 10x10
     k = 0;
     for (i=0;i<nx;++i){
         for (j=0;j<nx;++j){
             At[j+lda*i] = A[k];
             k = k + 1;
         }
     }
     
     // form matrix B 10x2
     k = 0;
     for (i=0;i<ny;++i){
         for (j=0;j<nx;++j){
             Bt[j+lda*i] = B[k];
             k = k + 1;
         }
     }

     // form matrix C 2x10
     k = 0;
     for (i=0;i<nx;++i){
         for (j=0;j<ny;++j){
             Ct[j+lda*i] = C[k];
         k = k + 1;
         }
     }
     
     // form matrix D 2x2
     k = 0;
     for (i=0;i<ny;++i){
         for (j=0;i<ny;++j){
             Dt[j+lda*i] = D[k];
         k = k + 1;
         }
     }
     
     
     /* Kalman Filter Algoritm starts here */
     
     /* Do QR of first thing */
        
     /* Determine PC' */
     
     dgemm(&n,&T,&nx,&ny,&nx,&alp, Pp, &nx, Ct, &ny, &beta, &PC, &nx);
     
     /* Form matrix */
     //Populate 2x2 R
     
     k = 0;
     lda = ny;
     for(i=0;i<ny;++i){
         for(j=0;j<ny;++j){
             cal_R[j+lda*i] = R[k + t*ny*ny];
             k = k + 1;
         }
     }
     
     //Populate 2x10 zeros
     k = 0;
     lda = ny;
     for(i=ny;i<ny+nx;++i){
         for(j=ny;j<nx;++j){
             cal_R[j+lda*i] = 0;
             
         }
     }

     //Populate 10x2 PC' matrix
     k = 0;
     lda = nx;
     for(i=ny;i<ny+nx;++i){
         for(j=0;j<ny;++j){
             cal_R[j+lda*i] = PC[k + t*ny*nx];
         }
     }
     
     //Populate 10x10 P matrix
     k = 0;
     for(i=ny;i<ny+nx;++i){
         for(j=ny;j<ny+nx;++j){
             cal_R[j+lda*i] = Pp[k + t*nx*nx];
             k = k + 1;
         }
     }
     
     // Qless QR factorisation with givens rotation
     varint lda = nx+ny;
     double a, b, c, s;
     for(j=0;j<lda;j++){
         for(i=lda-1;i>j;j--){
         a = cal_R[i-1+j*lda];
         b = cal_R[i+j*lda];
         
         drotg(&a,&b,&c,&s);
         varint row = (nx+ny)-j;
         drot(&row, &cal_R[i-1+j*lda], &lda, &cal_R[i+j*lda],&lda,&c,&s);
         }
     }
       
     
     /*Get cal_Rs from qrA*/
     
     //Cal_R11 2x2
     k = 0;
     lda = ny;
     for(i=0;i<ny;++i){
         for(j=0;j<ny;++j){
             cal_R11[j+lda*i] = qrA[k + t*ny*ny];
             k = k + 1;
         }
     }
     
     //Cal_R12 2x10
     k = 0;
     lda = ny;
     for(i=0;i<nx;++i){
         for(j=0;j<ny;++j){
             cal_R12[j+lda*i] = qrA[k + t*ny*nx];
             k = k + 1;
         }
     }
     
     //Cal_R22 10x10
     k = 0;
     lda = nx;
     for(i=0;i<nx;++i){
         for(j=0;j<nx;++j){
             cal_R22[j+lda*i] = qrA[k + t*nx*nx];
             k = k + 1;
         }
     }

     
     /* Pretend to understand what is going on with QR */
     
     /* Assign G.Pf */
    //G.Pf = cal_R22;

     /* Assign yt */
     k = 0;
     lda = ny;
     for(i=0;i<1;++i){
         for(j=0;j<ny;++j){
             yt[j+lda*i] = y[k + t*ny*ny];
             k = k + 1;
         }
     }


     /* Assign ut */
     k = 0;
     lda = ny;
     for(i=0;i<1;++i){
         for(j=0;j<ny;++j){
             ut[j+lda*i] = u[k + t*ny*ny];
             k = k + 1;
         }
     }
     
     //Form D*u
     dgemm(&n,&n,&ny,&nu,&ny,&alp, Dt, &ny, u, &ny,&beta, &Du,&ny);
     
     // Form Z to calculate e later
     Z = yt - Cx - Du; // BIG OLD COMPILER ERROR
     
     //Form e - use backsolving 
     zz = yt - Cx - Du; //COMPILER ERROR
     dtrsv(&U, &T, &n, &ny, cal_R22, &ny, zz, &ny);
     e = zz;
     
     /* Find and assign G.xf */
     G.xf = x + dgemm(&T,&n,&ny,&nu,&nx,&alp, cal_R12, &ny, e, &ny, &beta,&S,&nx);
     
     /* Form bar matrix for QR */
     k = 0;
     lda = nx;
     for(i=0;i<nx;++i){
         for(j=0;j<nx;++j){
             bar[j+lda*i] = PA[k + t*nx*nx];
             k = k + 1;
         }
     }
     
     k = 0;
     lda = ny;
     for(i=0;i<nx;++i){
         for(j=nx;j<(nx+nx);++j){
             bar[j+lda*i] = Q[k + t*nx*nx];
             k = k + 1;
         }
     }
     
     /* Do QR of other thing */
     lda = nx+ny;
     for(j=0;j<lda;++j){
         for(i=lda;i>-1;--j){
         a = bar[i-1+j*lda];
         b = bar[i+j*lda];
         
         drotrg(a,b,c,s);
         drot(nx+ny, a, 1, b,1,c,s);
         }
     }

     /* Find and assign G.Pp */
     
     // Form cal_R_bar matrix
     k = 0;
     lda = ny;
     for(i=0;i<nx;++i){
         for(j=nx;j<(nx+nx);++j){
             cal_R_bar[j+lda*i] = qrB[k + t*nx*nx];
             k = k + 1;
         }
     }
     
     G.Pp = cal_R_bar;
     
     /* Find and assign G.xp */
            
     G.xp = dgemm(&n,&n,&nx,&nu,&nx,&alp, A, &nx, xf, &nx, &beta, &Ax,&nx) + dgemm(&n,&n,&nx,&nu,&ny,&alp, Bt, &nx, u, &ny,&beta, &Bu,&ny);
     
     /* Set new initial state and P1 */
     x = G.xp;
     P = cal_R_bar;
 }
}
/*-------------------------------------------------------------------------*/
double * add3d(mxArray *structure, char *name, varint size1, varint size2, varint size3) 
{
  double *ptr; 
  mwSize dims[3], numd;
  const mwSize *dimd;   
  mxArray *tmp;
  tmp = mxGetField(structure, 0, name);
  if (tmp!=NULL){
    ptr = mxGetPr(tmp); 
    numd = mxGetNumberOfDimensions(tmp);
    if (numd==3) {
      dimd = mxGetDimensions(tmp);
      if((size1!=dimd[0]) || (size2!=dimd[1]) || (size3!=dimd[2])){
        mxDestroyArray(tmp);
        dims[0] = size1; dims[1] = size2; dims[2] = size3;
        tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
        ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
      }
    } else {
      mxDestroyArray(tmp);
      dims[0] = size1; dims[1] = size2; dims[2] = size3;
      tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
      ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
    }
  }
  else {
    mxAddField(structure, name);
    dims[0] = size1; dims[1] = size2; dims[2] = size3;
    tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
    ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
  }
  return ptr;
}
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
double * add2d(mxArray *structure, char *name, varint size1, varint size2) 
{
  double *ptr;
  mxArray *tmp;
  tmp = mxGetField(structure, 0, name);
  if (tmp!=NULL){
    ptr = mxGetPr(tmp);
    if((size1!=mxGetM(tmp)) || (size2!=mxGetN(tmp))){
      mxDestroyArray(tmp);
      tmp = mxCreateDoubleMatrix(size1, size2, mxREAL);
      ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
    }
  }
  else {
    mxAddField(structure, name);
    tmp = mxCreateDoubleMatrix(size1, size2, mxREAL);
    ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
  }
  return ptr;
}
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
void printvector(varint size, const double * vec, const char * name) {
  varint i;
  if (size > 0) {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf(" = \n\n");
    for(i=0;i<size;i++){
      if (vec[i] >= 0.0){mexPrintf("     %3.5e\n", vec[i]);}
      else {mexPrintf("     %3.5e\n", vec[i]);}
    }
    mexPrintf("\n\n");
  }
  else {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf("= []");
  }
}
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
void printmatrix(varint m, varint n, const double * A, varint lda, const char * name) {
  varint i, j;
  if (m*n > 0) {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf(" = \n\n");
    for(i=0;i<m;i++){
      for(j=0;j<n;j++){
        if (A[i+j*lda]>0.0){mexPrintf("  %3.4e", A[i+j*lda]);}
        else if (A[i+j*lda]<0.0){mexPrintf(" %3.4e", A[i+j*lda]);}
        else {mexPrintf("  0     ");}
      }
      mexPrintf("\n");
    }
    mexPrintf("\n\n");
  }
  else {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf("= []");
  }
}
/*-------------------------------------------------------------------------*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  //Input variables 
  double *y, *u, *A, *B, *C, *D, *Q, *R, *P1, *x1;
  //Output variables
  double *xp, *xf, *Pp, *Pf;
  varint numda, numdb, numds, numdq, ldh, itmp1;
  mwSize dims[3], numd;
  const mwSize *dimd;
  varint nu; //Number of inputs
  varint ny; //Number of outputs
  varint nx; //Number of states
  varint N;  //Number of measurements
  mxArray *tmp, *ss, *ssg;
  /* Check that we have the correct number of arguments */
  if(nrhs != 2){
    mexErrMsgTxt("There must be two arguments to this function.");
    return;
  }
  if(nlhs > 1){
    mexErrMsgTxt("Too many output arguments.");
    return;
  }
  /* Get output Data */
  tmp = mxGetField(prhs[0], 0, "y");
  if (tmp!=NULL){
    y = mxGetPr(tmp); 
    ny = mxGetM(tmp); 
    N = mxGetN(tmp);
  }
  else {
    mexErrMsgTxt("No such field: Z.y"); return;
  }
  /* Get input Data */
  tmp = mxGetField(prhs[0], 0, "u");
  if (tmp!=NULL){
    u = mxGetPr(tmp); 
    nu = mxGetM(tmp);
  } else {
    nu=0;
  }
  if (ny<1){
    mexErrMsgTxt("According to the data, there are no outputs. Nothing to do."); return;
  }
  /* Now the model variables */
  /* First thing is to duplicate M into G, and then manipulate G */
  plhs[0] = mxDuplicateArray(prhs[1]);
  //Get pointer to G
  ss = plhs[0];
  tmp = mxGetField(ss, 0, "A");
  if (tmp!=NULL){A = mxGetPr(tmp); nx = (varint)mxGetM(tmp);}
  else {mexErrMsgTxt("No such field: M.A"); return;}
  numd = mxGetNumberOfDimensions(tmp);
  numda = (varint)numd;
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=N){
      mexErrMsgTxt("The A matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "B");
  if (tmp!=NULL){B = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.B"); return;}
  numd = mxGetNumberOfDimensions(tmp);
  numdb = (varint)numd;
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=N){
      mexErrMsgTxt("The B matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "C");
  if (tmp!=NULL){C = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.C"); return;}
  numd = mxGetNumberOfDimensions(tmp);
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=N){
      mexErrMsgTxt("The C matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "D");
  if (tmp!=NULL){D = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.D"); return;}
  numd = mxGetNumberOfDimensions(tmp);
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=N){
      mexErrMsgTxt("The D matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "Q");
  if (tmp!=NULL){Q = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.Q"); return;}
  numd = mxGetNumberOfDimensions(tmp);
  numdq = (varint)numd;
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=N){
      mexErrMsgTxt("The Q matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "R");
  if (tmp!=NULL){R = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.R"); return;}
  numd = mxGetNumberOfDimensions(tmp);
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=N){
      mexErrMsgTxt("The R matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "P1");
  if (tmp!=NULL){P1 = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.P1"); return;}
  tmp = mxGetField(ss, 0, "x1");
  if (tmp!=NULL){x1 = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.ss.x1"); return;}
  /* Add fields to various structures */
  xp = add2d(ss, "xp", nx, N+1);
  xf = add2d(ss, "xf", nx, N);
  Pp = add3d(ss, "Pp", nx, nx, N+1);
  Pf = add3d(ss, "Pf", nx, nx, N);
  
  zData zdat;
  zdat.N = N;
  zdat.A = A;
  zdat.B = B;
  zdat.C = C;
  zdat.D = D;
  zdat.xp = xp;
  zdat.x1 = x1;
  zdat.xf = xf;
  zdat.P1 = P1;
  zdat.Q = Q;
  zdat.Pf = Pf;
  zdat.Pp = Pp;
  zdat.u = u;
  zdat.y = y;
  
  /* WRITE KALMAN FILTER KFg*/
  kf(&zdat);//y,u,A,B,C,D,x1,P1,XP,XF,pP,pF
  
}