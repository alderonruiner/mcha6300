function G = timMadeMeDoThisKF(Z,M)

% initial state
x = M.x1;
%initial state covariance
P = M.P1;

%get dims
[ny,~] = size(Z.y);
nx = length(x);
% M.R and M.Q are upper triangular cholesky factors

for t=1:Z.N
    [~,r] = givensQR([M.R(:,:,t) zeros(ny,nx); P*M.C(:,:,t)' P]);
    
    cal_R11 = r(1:ny,1:ny);
    cal_R12 = r(1:ny,ny+1:end);
    cal_R22 = r(ny+1:end,ny+1:end);

    G.Pf(:,:,t) = cal_R22;
    e = cal_R11'\(Z.y(:,t) - M.C(:,:,t)*x - M.D(:,:,t)*Z.u(:,t)); 
    G.xf(:,t) = x + cal_R12'*e;
    
    [~,r] = givensQR([G.Pf(:,:,t)*M.A(:,:,t)';M.Q(:,:,t)]);
    cal_R_bar = r(1:nx,1:nx);
    
    %cal_R_bar = Pf*M.A(:,:,t)';
    G.Pp(:,:,t+1) = cal_R_bar;
    G.xp(:,t+1) = M.A(:,:,t)*G.xf(:,t) + M.B(:,:,t)*Z.u(:,t);
    
    % set new initial conditions and P
    x = G.xp(:,t);
    P = cal_R_bar;

end

