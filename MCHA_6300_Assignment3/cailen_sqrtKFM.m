function G = sqrtKFM(Z, M)
% Unpack structures
N = Z.N;
u = Z.u;
y = Z.y;
P1 = M.P1;
Qt = M.Q;
Rt = M.R;
A = M.A;
B = M.B;
C = M.C;
D = M.D;
x1 = M.x1;

% Get the correct dimensions
[ny, ~] = size(y);
nx = length(x1);

% Initialise Variables
Pf = zeros(nx, nx, N);
Pp = zeros(nx, nx, N+1);
xf = zeros(nx, N);
xp = zeros(nx, N+1);
Pp(:, :, 1) = M.P1;
xp(:, 1) = x1;

for t = 1:N
%     [~, R] = GivensRotQR([Rt(:, :, t) zeros(ny, nx); Pp(:, :, t)*C(:, :, t)' Pp(:, :, t)])
    [~, R] = qr([Rt(:, :, t) zeros(ny, nx); Pp(:, :, t)*C(:, :, t)' Pp(:, :, t)]);
    
    % Calc R matrices from R
    R11 = R(1:ny, 1:ny);
    R12 = R(1:ny, ny+1:end);
    R22 = R(ny+1:end, ny+1:end);
    
    % Calculate error and then using that error calculate the state
    % updated transition model
    Pf(:, :, t) = R22;
    e(:, t)  = R11'\(y(:,t) - C(:, :, t)*xp(:, t) - D(:, :, t)*u(:,t));
    xf(:,t) = xp(:, t) + R12'*e(:,t);
    
    % Calculate R_bar
%     [~, R_bar] = GivensRotQR([Pf(:, :, t)*A(:,:,t)'; Qt(:, :,t)]);
    [~, R_bar] = qr([Pf(:, :, t)*A(:,:,t)'; Qt(:, :,t)]);
    
    % Predicted parts
    R_bar = R_bar(1:nx, 1:nx);
    Pp(:, :, t+1) = R_bar;
    xp(:,t+1) = A(:, :, t)*xf(:,t) + B(:,:,t)*u(:,t);
    
end
G.Pp = Pp;
G.xp = xp;
G.Pf = Pf;
G.xf = xf;

end


    
