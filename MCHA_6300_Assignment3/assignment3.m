%MCHA6300 Assignment3
%Alderon Ruiner - c3279096

close all;
clear;
clc;

rng(1);

%% specs.
N = 1000;   % Number of data samples

nx = 10;    % Number of states
nu = 2;     % Number of inputs
ny = 3;     % Number of outputs
np = 2;     % Number of systems

%% true system
[A1,B1,C1,D1] = ssdata(drss(nx,ny,nu));
[A2,B2,C2,D2] = ssdata(drss(nx,ny,nu));

A  = [A1 A2];
B  = [B1 B2];
C  = [C1 C2];
D  = [D1 D2];
Q  = eye(nx)/100;
R  = eye(ny)/100;
P1 = eye(nx);

%% signals
u  = square(2*pi*linspace(0,13,N));
u  = [u;square(pi/2+2*pi*linspace(0,17,N))];
p  = (square(2*pi*linspace(0,5,N))+1)/2;
p  = [p;1-p];
q  = (sin(2*pi*linspace(0,3,N))+1)/2;
q  = [q;1-q];
w  = sqrtm(Q)*randn(nx,N);
v  = sqrtm(R)*randn(ny,N);

%% simulate system
x      = zeros(nx,N+1);
y      = zeros(ny,N);
xX = sqrtm(P1)*randn(nx,1);
x(:,1) = xX;
for k=1:N
    M.A(:,:,k) = A1*p(1,k) + A2*p(2,k);
    M.B(:,:,k) = B1*q(1,k) + B2*q(2,k);
    M.C(:,:,k) = C1*p(1,k) + C2*p(2,k);
    M.D(:,:,k) = D1*q(1,k) + D2*q(2,k);
    M.Q(:,:,k) = Q;
    M.R(:,:,k) = R;
    x(:,k+1) = M.A(:,:,k)*x(:,k) + M.B(:,:,k)*u(:,k) + w(:,k);
    y(:,k)   = M.C(:,:,k)*x(:,k) + M.D(:,:,k)*u(:,k) + v(:,k);
end

Z.u=u;
Z.y=y;
Z.N=N;
M.P1 = P1;
M.x1 = zeros(nx,1);


%Now call your own time-varying Kalman filter and plot results

%% matlab KF
figure(1);

G = timMadeMeDoThisKF(Z,M);

plot(1:N,G.xp(1,1:N),'b')
hold on
plot(1:N,G.xf(1,1:N),'r')
hold on
plot(1:N,x(1,1:N), 'g')
hold off
xlabel('Time')
ylabel('State')
title('Predicted and Filtered States')
legend('x_p (predicted)', 'x_f (filtered)','actual')

% % c code KF
% figure(2);
% 
% %G = sqrtKFGivens(Z,M);
% tic
% Gg = sqrtKFrewrap(Z,M);
% toc
% % G = sqrtKFscully(Z,M); 
% 
% plot(1:N,Gg.xp(1,1:N),'b')
% hold on
% plot(1:N,Gg.xf(1,1:N),'r')
% plot(1:N,x(1,1:N), 'g')
% hold off
% xlabel('Time')
% ylabel('State')
% title('Predicted and Filtered States')
% legend('x_p (predicted)', 'x_f (filtered)')
%     
    
    
