//mex -O sqrtKFscully.c -lmwblas -lmwlapack
#include "mex.h"
#include "math.h"
#include "blas.h"
#include "lapack.h"
#define varint mwSignedIndex
void printmatrix(varint m, varint n, const double * A, varint lda, const char * name);
void QRGivensC(double *arr, varint col, varint row);

// Create struct and fill with the parameters
typedef struct {
    int N;
    varint nx;
    varint nu;
    varint ny;
    double *u;
    double *y;
    double *A, *B, *C, *D, *x1, *P1, *xp, *xf, *Pp, *Pf;
    double *Q, *R;
} zData;
        
 
 
/*-------------------------------------------------------------------------*/
double * add3d(mxArray *structure, char *name, varint size1, varint size2, varint size3) 
{
  double *ptr;
  mwSize dims[3], numd;
  const mwSize *dimd;   
  mxArray *tmp;
  tmp = mxGetField(structure, 0, name);
  if (tmp!=NULL){
    ptr = mxGetPr(tmp); 
    numd = mxGetNumberOfDimensions(tmp);
    if (numd==3) {
      dimd = mxGetDimensions(tmp);
      if((size1!=dimd[0]) || (size2!=dimd[1]) || (size3!=dimd[2])){
        mxDestroyArray(tmp);
        dims[0] = size1; dims[1] = size2; dims[2] = size3;
        tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
        ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
      }
    } else {
      mxDestroyArray(tmp);
      dims[0] = size1; dims[1] = size2; dims[2] = size3;
      tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
      ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
    }
  }
  else {
    mxAddField(structure, name);
    dims[0] = size1; dims[1] = size2; dims[2] = size3;
    tmp = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
    ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
  }
  return ptr;
}
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
double * add2d(mxArray *structure, char *name, varint size1, varint size2) 
{
  double *ptr;
  mxArray *tmp;
  tmp = mxGetField(structure, 0, name);
  if (tmp!=NULL){
    ptr = mxGetPr(tmp);
    if((size1!=mxGetM(tmp)) || (size2!=mxGetN(tmp))){
      mxDestroyArray(tmp);
      tmp = mxCreateDoubleMatrix(size1, size2, mxREAL);
      ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
    }
  }
  else {
    mxAddField(structure, name);
    tmp = mxCreateDoubleMatrix(size1, size2, mxREAL);
    ptr = mxGetPr(tmp); mxSetField(structure, 0, name, tmp);
  }
  return ptr;
}
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
void printvector(varint size, const double * vec, const char * name) {
  varint i;
  if (size > 0) {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf(" = \n\n");
    for(i=0;i<size;i++){
      if (vec[i] >= 0.0){mexPrintf("     %3.5e\n", vec[i]);}
      else {mexPrintf("     %3.5e\n", vec[i]);}
    }
    mexPrintf("\n\n");
  }
  else {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf("= []");
  }
}
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
void printmatrix(varint m, varint n, const double * A, varint lda, const char * name) {
  varint i, j;
  if (m*n > 0) {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf(" = \n\n");
    for(i=0;i<m;i++){
      for(j=0;j<n;j++){
        if (A[i+j*lda]>0.00000001){mexPrintf("  %3.4e", A[i+j*lda]);}
        else if (A[i+j*lda]<-0.00000001){mexPrintf(" %3.4e", A[i+j*lda]);}
        else {mexPrintf("  0     ");}
      }
      mexPrintf("\n");
    }
    mexPrintf("\n\n");
  }
  else {
    mexPrintf("\n ");
    mexPrintf(name);
    mexPrintf("= []");
  }
}



// Create sqrt kalman filter ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void kf(zData *zdat){
    // Unpack variables
    varint nx = zdat->nx;
    varint ny = zdat->ny;
    varint nu = zdat->nu;
    int N = zdat->N;

    double *R = zdat->R;
    double *Q = zdat->Q;

    double *A = zdat->A;
    double *B = zdat->B;
    double *C = zdat->C;
    double *D = zdat->D;
    
    double *u = zdat->u;
    double *y = zdat->y;

    double *Pp = zdat->Pp;
    double *P1 = zdat->P1;
    double *Pf = zdat->Pf;

    double *xp = zdat->xp;
    double *x1 = zdat->x1;
    double *xf = zdat->xf;
    
    // Create the length of the array
    varint length = (nx+ny);

    // DGEMM initialisation
    double alpha = 1.0, beta = 0.0;
    varint one = 1, zero = 0;
    
    // Pre-initialse array sizes
    double *arr_Pp_C = malloc(((nx)*(ny))*sizeof(double));
    double *arr = malloc((length*length)*sizeof(double));
    double *R11 = malloc((ny*ny*sizeof(double)));
    double *R12 = malloc((ny*nx*sizeof(double)));
    double *Du = malloc((ny*sizeof(double)));
    double *Cx = malloc((ny*sizeof(double)));
    double *err = malloc((ny*sizeof(double)));
    double *R12e = malloc((nx*sizeof(double)));
    double *pA = malloc((nx*nx*sizeof(double)));
    double *arr2 = malloc(((nx+nx)*nx)*sizeof(double));
    double *Ax = malloc((nx*sizeof(double)));
    double *Bu = malloc((nx*sizeof(double)));

  
    
    // Initialise Pp with the given P1
    for(int i=0; i<nx; i++)
    {
        for(int j=0; j<nx; j++)
        {
            Pp[i + j*nx] = P1[i + j*nx];
        }
    }

    //Initialise xp with the given x1
    for(int i=0; i<nx; i++)
    {
          
      xp[i] = x1[i];
          
    }
        
    
    ////////////////////////////////////////////// TIME LOOP ///////////////////////////////////////////////////////////////////  
    for(int t=0; t<N; ++t)
    {   
        // Determine the values to offset the matrix by. Get the 'depths' of the matrix
        int offset1 = t*nx*nx;
        int offset2 = t*nx*ny;
        int offset3 = t*one*nu; 
        int offset4 = t*nu*ny; 
        int offset5 = t*nx;
        int offset6 = t*nx*nu; 
        int offset7 = (t + 1)*nx*nx;
        int offset8 = (t + 1)*nx;

        /////////////////////////////////// Make filtered Givens input array ///////////////////////////////////////////////////////////////////////////
        // Initialise QR factorisation input array = [Rt(:, :, t) zeros(ny, nx); Pp(:, :, t)*C(:, :, t)' Pp(:, :, t)]
        for(int i=0; i<(length*length); i++){
          arr[i] = 0.0;
        }

        // Calculate Pp(:, :, t)*C(:, :, t)'
        dgemm("N", "T", &nx, &ny, &nx, &alpha, Pp + offset1, &nx, C + offset2, &ny, &beta, arr_Pp_C, &nx);
                
        // Create the top left sub array of the input of the QR factorisation
        int count = 0;
        for(int i=0; i<ny; i++)
        {
            for(int j=0; j<ny; j++)
            {
                arr[j + i*length] = R[count + t*ny*ny];
                count++;
            }
        }
       
        
        // Create the bottom left sub array of the input of the QR factorisation
        count = 0;
        for(int i=0; i<ny; i++)
        {
            for(int j=0; j<nx; j++)
            {
                arr[ny + j + i*length] = arr_Pp_C[count];
                count++;
            }
        }
        
        // Create the bottom right sub array of the input of the QR factorisation
        count = 0;
        for(int i=0; i<nx; i++)
        {
            for(int j=0; j<nx; j++)
            {
                arr[ny*length + ny+ j + i*length] = Pp[count + t*nx*nx];
                count++;
            }
        }


       

        ///////////////////////////////////////////////// Filtered GivensQR ///////////////////////////////////////////////////////////////////////
        // R = GivensQR(array, number of rows of array, number of columns of array)
        QRGivensC(arr, length, length);
        
        ////////////////////////////////////////////////// Unpack Arrays ///////////////////////////////////////////////////////////////////////

        // Top left matrix - R11
        count = 0;
        for(int i=0; i<ny; i++){
            for(int j=0; j<ny; j++){
                R11[count] = arr[j + i*length];
                count++;
            }
        }
        
       // Top right matrix - R12
       count = 0;
        for(int i=ny; i<nx+ny; i++){
            for(int j=0; j<ny; j++){
                R12[count] = arr[i*length + j];
                count++;
            }
        } 

        // Bottom right matrix - R22 (Get the correct values from the depth of the matrix with the offset)
        count = 0;
        for(int i=ny; i<nx+ny; i++)
        {
            for(int j=ny; j<nx+ny; j++)
            {
                Pf[count + offset1] = arr[j + i*length];
                count++;
            }
        }  

        

        //////////////////////////////////// Calculate Error /////////////////////////////////////////////////////////////////////////////////
        // Find D*u - D(:, :, t)*u(:,t)
        dgemm("N", "N", &ny, &one, &nu, &alpha, D + offset4, &ny, u + offset3, &nu, &beta, Du, &ny);

        // Find C*xp - C(:, :, t)*xp(:, t)
        dgemm("N", "N", &ny, &one, &nx, &alpha, C + offset2, &ny, xp + offset5, &nx, &beta, Cx, &nx);

        // Calculate error - e(:, t)  = (y(:,t) - C(:, :, t)*xp(:, t) - D(:, :, t)*u(:,t))
       for(int k=0; k<ny; k++){
          err[k] = y[k+t*ny] - Cx[k] - Du[k];
        }

        
        // Calculate e = R11'\(y(:,t) - C(:, :, t)*xp(:, t) - D(:, :, t)*u(:,t))
        dtrsv("U", "T", "N", &ny, R11, &ny, err, &one);


        //////////////////////////////////// Calculate xf ////////////////////////////////////////////////////////////////////////////////////////////
        // Calculate R12'*e(:,t)
        dgemm("T", "N", &nx, &one, &ny, &alpha, R12, &ny, err, &ny, &beta, R12e, &nx);

        // Calculate xf = 
        for(int k=0; k<nx; k++){
          xf[k+t*nx] = xp[k+t*nx] + R12e[k];
        }


        /////////////////////////////// Make predicted Givens input array ///////////////////////////////////////////////////////////////////////////
        // Make matrix for second GivensRot - [Pf(:, :, t)*A(:,:,t)'; Qt(:, :,t)]
        // Create Pf*A'
        dgemm("N", "T", &nx, &nx, &nx, &alpha, Pf + offset1, &nx, A + offset1, &nx, &beta, pA, &nx);

        // Create top half of array
        count = 0;
        for(int i=0; i<nx; i++){
          for(int j =0; j<nx; j++){
            arr2[j + i*(nx+nx)] = pA[count];
            count++;
          }
        } 

        // Create bottom half of array
        count=0;
        for(int i = 0; i<nx; i++){
          for(int j=nx; j<(nx+nx); j++){
            arr2[j + i*(nx+nx)] = Q[count + offset1];
            count++;
          }

        }

		
        //////////////////////////////////////// Predicted Givens QR //////////////////////////////////////////////////////////////////////////
        // R_bar = GivensQR(array, number of rows of array, number of columns of array)
        QRGivensC(arr2, (nx+nx), nx);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        /////////////////////////////////////// Find Pp /////////////////////////////////////////////////////////////////////////////////////////
        // Extract Pp from the output of the givens matrix
        for(int i=0; i<nx; i++){
          for(int j=0; j<nx; j++){
            Pp[j+ (i*nx) + offset7] = arr2[j + i*(nx+nx)];
          }
        }
		

        ////////////////////////////////////// Calculate xp /////////////////////////////////////////////////////////////////////////////////////
        // Find A*x
        dgemm("N", "N", &nx, &one, &nx, &alpha, A + offset1, &nx, xf + offset5, &nx, &beta, Ax, &nx);

        // Calculate B*u
        dgemm("N", "N", &nx, &one, &nu, &alpha, B + offset6, &nx, u + offset3, &nu, &beta, Bu, &nx);

        // Calculate xp = A(:, :, t)*xf(:,t) + B(:,:,t)*u(:,t)
        for(int k=0; k<nx; k++){
          xp[k + offset8] = Ax[k] + Bu[k];
        }

        // Debugging
        //printmatrix(nx, one, xp+ offset8, nx, "xp");
 
        
    }
    // Debugging
    //exit:
    

    return;
}


// Create Givens QR in C
void QRGivensC(double *arr, varint num_row, varint num_col){
    // Declare size of varibles that are passed into the givens rotation
    double x, y, c, s;

    // Start from the bottom of the array and go up the rows and rotate each index of the array until you get to the top
    for(int i=0; i<num_col; i++){
        for(int j=num_row-1; j>i; j--){
            // Make the x and y 'vector' to go into the givens rotation
            x = arr[j-1 + i*num_row];
            y = arr[j + i*num_row];

            // Calculate the number of elements in input vector
            varint col_idx = num_col-i;

            // drotg sets up the rotation 
            drotg(&x, &y, &c, &s);
            
            // drot does the rotation
            drot(&col_idx, &arr[j-1 + i*num_row], &num_row, &arr[j + i*num_row], &num_row, &c, &s);
        }
    }
}


/*-------------------------------------------------------------------------*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  //Output variables
  varint numda, numdb, numds, numdq, ldh, itmp1;
  mwSize dims[3], numd;
  const mwSize *dimd;
  
  mxArray *tmp, *ss, *ssg;
  
  /* Check that we have the correct number of arguments */
  if(nrhs != 2){
    mexErrMsgTxt("There must be two arguments to this function.");
    return;
  }
  if(nlhs > 1){
    mexErrMsgTxt("Too many output arguments.");
    return;
  }
  // Structure identifiers
   zData Z;
  
  /* Get output Data */
  tmp = mxGetField(prhs[0], 0, "y");
  if (tmp!=NULL){
    Z.y = mxGetPr(tmp); 
    Z.ny = mxGetM(tmp); 
    Z.N = mxGetN(tmp);
  }
  else {
    mexErrMsgTxt("No such field: Z.y"); return;
  }
  
  /* Get input Data */
  tmp = mxGetField(prhs[0], 0, "u");
  if (tmp!=NULL){
    Z.u = mxGetPr(tmp); 
    Z.nu = mxGetM(tmp);
  } else {
    Z.nu = 0;
  }
  if (Z.ny<1){
    mexErrMsgTxt("According to the data, there are no outputs. Nothing to do."); return;
  }
  
  /* Now the model variables */
  /* First thing is to duplicate M into G, and then manipulate G */
  plhs[0] = mxDuplicateArray(prhs[1]);
  
  //Get pointer to G
  ss = plhs[0];
  tmp = mxGetField(ss, 0, "A");
  
  if (tmp!=NULL){
    Z.A = mxGetPr(tmp); 
    Z.nx = (varint)mxGetM(tmp);
  }
  else {mexErrMsgTxt("No such field: M.A"); 
    return;
  }
  numd = mxGetNumberOfDimensions(tmp);
  numda = (varint)numd;
  
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=Z.N){
      mexErrMsgTxt("The A matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  
  tmp = mxGetField(ss, 0, "B");
  
  if (tmp!=NULL){Z.B = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.B"); return;}
  
  numd = mxGetNumberOfDimensions(tmp);
  numdb = (varint)numd;
  
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=Z.N){
      mexErrMsgTxt("The B matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "C");
  
  if (tmp!=NULL){Z.C = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.C"); return;}
  
  numd = mxGetNumberOfDimensions(tmp);
  
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=Z.N){
      mexErrMsgTxt("The C matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  
  tmp = mxGetField(ss, 0, "D");
  
  if (tmp!=NULL){Z.D = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.D"); return;}
  
  numd = mxGetNumberOfDimensions(tmp);
  
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=Z.N){
      mexErrMsgTxt("The D matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  
  tmp = mxGetField(ss, 0, "Q");
  
  if (tmp!=NULL){Z.Q = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.Q"); return;}
  numd = mxGetNumberOfDimensions(tmp);
  numdq = (varint)numd;
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=Z.N){
      mexErrMsgTxt("The Q matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "R");
  if (tmp!=NULL){Z.R = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.R"); return;}
  
  numd = mxGetNumberOfDimensions(tmp);
  
  if (numd==3){
    dimd = mxGetDimensions(tmp);
    if (dimd[2]!=Z.N){
      mexErrMsgTxt("The R matrix is specified as a time varying matrix but it does not have the correct number of time entries in the 3rd dimension");
    }
  }
  tmp = mxGetField(ss, 0, "P1");
  if (tmp!=NULL){Z.P1 = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.P1"); return;}
  tmp = mxGetField(ss, 0, "x1");
  if (tmp!=NULL){Z.x1 = mxGetPr(tmp);}
  else {mexErrMsgTxt("No such field: M.ss.x1"); return;}
          
  /* Add fields to various structures */
  Z.xp = add2d(ss, "xp", Z.nx, Z.N+1);
  Z.xf = add2d(ss, "xf", Z.nx, Z.N);
  Z.Pp = add3d(ss, "Pp", Z.nx, Z.nx, Z.N+1);
  Z.Pf = add3d(ss, "Pf", Z.nx, Z.nx, Z.N);
  
  // call sqrt kalman filter - call BLAS routines GROT and GROTD
 
  kf(&Z);
  
}