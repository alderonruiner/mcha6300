function xOpt = newtonTrust(fcn,x,pars)
k = 0; % initialise k
Delta = pars.delta; %0.01
Delta_bar = pars.delta_bar; %25
eps = 0.1; % part of stopping condition
eta = 0.1;
[~, g_x, ~] = fcn(x);

while norm(g_x) > sqrt(eps)

[f_x, g_x, h_x] = fcn(x);
[f_x_p,~,~] = fcn(x+pk);
pk = trs3(h_x,g_x,D);
q_x = f_x;
q_x_p = f_x + g'*pk + 0.5*pk'*H*pk;

rhok = (f_x - f_x_p)/(q_x - q_x_p);
    if rhok < 0.25
    Delta = 0.25*abs(p);

    else 
    
        if (rhok > 0.75 && abs(norm(pk))- Delta < 1e-3)
        Delta = min(2*Delta, Delta_bar);
        
        else
        Delta = Delta;
        
        end
    
    end
    
    if rhok > eta
        x = x + pk;
    else
        x = x;
        return;
    end
k = k + 1;
end
    
    