function [p] = trs(H, g, Delta, pars)
% H - hessian
% g - gradient vector
% Delta - trust-region radius
% pars
% p - global solution
% Nocedal and Wright


m_k = @(p) g'*p + 0.5*p'*H*p;
x = [0;0];
fmincon(m_k,x,1,Delta)

   
    
    