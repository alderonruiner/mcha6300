function xOpt = newtonTrust(fcn,x,pars)
k = 0; % initialise k
eps = 0.1; % part of stopping condition
rho(k) = (f_x - f_x_k)/(m_k(0) - m_k(p)
while norm(g_x) > sqrt(eps)

    if rho(k) < 0.25
    Delta(k+1) = 0.25*abs(p);

    else 
    
        if (rho(k) > 0.75 && abs(p)==Delta(k))
        Delta(k+1) = min(2*Delta(k), Delta(k));
        
        else
        Delta(k+1) = Delta(k);
        
        end
    
    end
    
    if rho(k) > eta
        x(k+1) = x(k) + p(k);
    else
        x(k+1) = x(k);
    end
k = k + 1
end
    
    