function xOpt = newtonDescent(fcn,x,pars)
% fcn - function handle 
% x - initial condition estimate
% pars
% same algorithm as Steepest Descent, uses actual hessian where as steepest
% decscent assume hessian = I;



eps = 0.1;
[f_x, g_x, h_x] = fcn(x);
p = -h_x\g_x;
alpha = 1;
k = 1;

while norm(g_x) > sqrt(eps)

x(:,k+1) = x(k) + alpha(k)*p;

k = k + 1;
alpha = alpha/2;
X = x(:,k);
[~, g_x] = fcn(X);
p = -h_x*g_x;

if k == length(x)
    break
end
end 
xOpt = X;