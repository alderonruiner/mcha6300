
%% NEWTON DESCENT ALGORITHM
clc;
clear;

pars = 0;
x = [1,1]';

% Run optimisation
func = @(x) fcn2(x);
xOpt2 = newtonDescent(func, x, pars);
func = @(x) fcn3(x);
xOpt3 = newtonDescent(func, x, pars);
func = @(x) fcn4(x);
xOpt4 = newtonDescent(func, x, pars);

% Plot 
x1 = linspace(-10,10,100);
x2 = linspace(-10,10,100);

func = @(x) fcn2(x);
for i= 1:length(x1)
    for j = 1:length(x2)
       z(i,j) = func([x1(i); x2(j)]);
    end
end

figure(1);
surf(z)

func = @(x) fcn3(x);
for i= 1:length(x1)
    for j = 1:length(x2)
       z(i,j) = func([x1(i); x2(j)]);
    end
end

figure(2);
surf(z)

func = @(x) fcn4(x);
for i= 1:length(x1)
    for j = 1:length(x2)
       z(i,j) = func([x1(i); x2(j)]);
    end
end

figure(3);
surf(z)
%% TRUST REGION METHOD
