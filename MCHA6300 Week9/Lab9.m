clc;
clear;
close all;
%% 2. PRIMAL AND DUAL PROBLEMS
%Use matlab linprog function to solve the primal and dual problems and
%compute the duality gap between these solutions

% Formulate the Primal problem in form Ax<=b

%Malt: 175 units
%Hops: 160 units
%Yeast: 150 units

%Light beer (/L): Malt (2u), hops (3u), yeast (2u) - $10/L
%Dark beer (/L): Malt (3u), hops (1u), yeast (4u) - $6/L

c = [10 6]';
A = [2 3;
     3 1;
     2 4];
b  = [175 160 150]';

[x, fval] = linprog(-c,A,b);
profit = -fval;
%Derive the dual optimisation problem

%A'*lam = c
%-b'lam
f = b';
Aeq = A';
beq = c;
A = 0;
A = -eye(3);
b = [0 0 0]';


[x2, fval2] = linprog(f,A,b, Aeq,beq);
profit = -fval2;

%% 3. CHEBYSHEV APPROXIMATION
A = [1 -2 4;
     1 -1 1;
     1 0 0;
     1 1 1;
     1 2 4];
 
b  = [12.2 11.8 12 12.8 14.2]';
 
% [x, fval] = linprog(f,A,b,Aeq,Beq);