function tests = QRTests
tests = functiontests(localfunctions);
end

function testgivensQR(testCase)
A = [1  2  3;
     4  5  6;
     7  8  9;
     10 11 12;
     13 14 16];

[Q_bar, R_bar] = givensQR(A);
[Q, R] = qr(A);
actual_norm = norm(Q_bar*R_bar - Q*R);
expected_norm = norm(A - Q*R);
% check that norm(Q_bar*R_bar - Q*R) == norm(A - QR)
assertEqual(testCase, actual_norm, expected_norm, 'AbsTol', 10*eps, 'Expected your norm to match');

end

function testhouseholderQR(testCase)
A = [1  2  3;
     4  5  6;
     7  8  9;
     10 11 12;
     13 14 16];

[Q_bar, R_bar] = householderQR(A);
[Q, R] = qr(A);
actual_norm = norm(Q_bar*R_bar - Q*R);
expected_norm = norm(A - Q*R);
% check that norm(Q_bar*R_bar - Q*R) == norm(A - QR)
assertEqual(testCase, actual_norm, expected_norm, 'AbsTol', 10*eps, 'Expected your norm to match');
end