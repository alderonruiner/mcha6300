function [Q,R] = householderQR(A)

% Get dimensions
[m,n] = size(A);

% Initialise
Q = eye(m);
R = A;      
v=zeros(m,1);

for i=1:n
    % Reset Reflection vector v
    v(:) = 0;
    
    % Repopulate Reflection vector v
    v(i:m,1) = R(i:m,i);    
    % 
    v(i) =v(i) - norm(v);
    
    %Orthogonal transformation matrix that eliminates one element
    %below the diagonal of the matrix it is post-multiplying:
    s=norm(v);
    if s~=0
        w=v/s; 
        u=2*R'*w;
        R=R-w*u'; %Product HR
        Q=Q-2*Q*w*w'; %Product QR
    end
end
R = triu(R); % R is upper triangle (zeros off e-16 values in lower triangle)

end

% [m,~] = size(A);
% R = A; % initialise R = A
% Q = eye(m); % initialise Q matrix as identity matrix, size m
% v = zeros(m,1);
% 
% for i = 1:m-1
%     v(i:m,1) = R(i:m,i);
%     QRnorm = -sign(v(i))*norm(v);
%     v(i) = v(i) - norm(v);
%     
%     if QRnorm ~= 0
%         w = v/QRnorm; 
%         u = 2*R'*w;
%         R = R - w*u';
%         Q = Q - 2*Q*w*w';
%     end
%     
% end


% %loop to reflect each column of A matrix
% v = R(:,1);
% v(1) = v(1) - norm(v);
% Qi{1} = eye(m) - (2/(v'*v))*(v*v');
% R = Qi{1}*R;
% 
% for i = 2:m-1
%     j = j + 1;
%     v = R(i:end,i);
%     v(1) = v(1) - norm(v);
%     Qi{i} = blkdiag(eye(i-1), eye(m - j) - (2/(v'*v))*(v*v'));
%     R = Qi{i}*R;
%  
% end
% Q = prod(Qi)';
% norm(A-Q+R);
% 
% end
