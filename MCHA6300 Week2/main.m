clc;
clear;
close all;
%Golub & Van Loan

%% GIVENS ROTATION QR

% 1.1 investigate a numerically stable implementation of computing and
% appying a Given's rotation

% 1.2 Write your own QR factorisation function using Givens rotations

A = [1  2  3;
     4  5  6;
     7  8  9;
     10 11 12;
     13 14 16];
 
[Q_g, R_g] = givensQR(A);
%% HOUSEHOLDER REFLECTION QR

% 2.1 investigate a numerically stable implementation of computing and
% applying a Householder reflection

% 2.2 Write your own QR factorisation function using Householder
% reflections

[Q_h, R_h] = householderQR(A)
%% QR SOLUTION TO LEAST SQUARES PROBLEM

% 3.1 Using the A and b formed in lab 1 with the best order n, form the Q1
% and R1 matrices using both Givens  and Householder QR methods
addpath('../ENGG6200 Week1')

% 3.2 Form vector d = Q1*b and solve R1*theta = d via matlab cmd theta =
% R_1\d

% n = 8;
% A = fliplr(vander(x'));
% A = A(:,1:n);
% b=y';
% theta1 = A\b;
% H = A'*A;
% d = A'*b;
% C = chol6200(H);
% theta2 = C'\(C\d);
% scatter(x,y)
% hold on
% plot(-6:.1:6,polyval(fliplr(theta1'), -6:.1:6));
% hold off


% theta = R_1\d;