function p = trs3(H,g,D)

[Q,V]=eig(H);
l1 = min(diag(V));
v  = diag(V);
a  = Q'*g;
if l1 < 0
  lam = 1.01*abs(l1);
else
  lam = 0;
end

%Case 1 (try it on)
p = -Q*(a ./ (v + lam));%Cse 2 or hard-case
if l1<0 || norm(p)>D || abs(lam*(norm(p)-D)) > sqrt(eps)
  %Case 2
  idx = v > v(1) + 1e-8;
  c3 = sum((a(idx).^2) ./ ((v(idx)-l1).^2));
  if D^2 <= c3 || abs(a(1)) > 1e-8
    %disp('Case 2');
    for k=1:20
      pp = -a ./ (v + lam);
      dp = a ./ ((v+lam).^2);
      ff = 1/D - 1/sqrt(pp'*pp);
      gg = dp'*pp/ (pp'*pp)^(3/2);     
      lam = max(max(0,-l1)+1e-8*max(0,-l1),lam - ff/gg);  
      
      if abs(1/norm(pp)-1/D) < sqrt(eps)
        break;
      end
    end
    p = -Q*(a ./ (v + lam));
  else %Hard case
    %disp('Hard case');
    %Compute direction     
    t  = sqrt(D^2 - c3);
    p  = t*Q(:,1) - Q(:,idx)*(a(idx)./(v(idx)-l1));  
    
    if p'*g + 0.5*p'*H*p > 0
      p  = -t*Q(:,1) - Q(:,idx)*(a(idx)./(v(idx)-l1));
    end
  end
end

if p'*g + 0.5*p'*H*p > 0
  keyboard
end