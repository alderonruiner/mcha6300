function [xOpt,xs,ds] = SR1Trust_Adrian(fcn,x,pars)

x = x(:);
n = length(x);

DeltaMax = 1e3;
Delta  = 0.5;

xs = x;
ds = Delta;

h = -eye(n);

for i=1:1000
  [f,g] = fcn(x,pars);
  go = g;
  [p] = trs3(h,g,Delta);
  
  [fn,gn] = fcn(x+p,pars);
  y = gn-go;  
  
  fprintf('Iteration Number = %5i, Cost = %10.2e, Newton Decrement = %10.2e, Delta = %10.2e\n',i,f,p'*g,Delta);  
  
  if abs(p'*g) < (10*eps) && min(eig(h)) > eps,  break; end
  
  fa = fcn(x+p,pars);
  fp = f + p'*g + 0.5*p'*h*p; 
  
  rho = (f-fa) / (f - fp); 
  
  if rho < 0.25
    Delta = 0.25*norm(p);
  else 
    if rho > 0.75 && abs(Delta - norm(p))<sqrt(eps)
      Delta = min(2*Delta,DeltaMax);
    else
      Delta = 1.0*Delta;
    end
  end
  
  if rho > 0.001
    xn = x + p; 
  else
    xn = x; 
  end
  
  %Now update the hessian approximation
  if abs(p'*(y-h*p)) > sqrt(eps)*norm(p)*norm(y-h*p)
    h = h + (y-h*p)*(y-h*p)'/(p'*(y-h*p));
  end
  
  %plot([x(1) xn(1)],[x(2) xn(2)],'kd-','linewidth',2);
  x = xn;
  xs = [xs x];
  ds = [ds Delta];
end

xOpt = x;