function theta = guassNewton(fcn, y, u, theta, opt)
% Modified to handle rank (column) deficient jacobian

%Wolfe constants
c1 = opt.wolfeC1;

for i = 1:opt.maxIter
    %Get cost and gradient
    [V, pe, g, J] = feval(fcn,y,u,theta);
    
    %if J is rank deficient it isn't invertible and solution cannot be formed
    %using:
    % p = -inv(J'*J)*J'*r
    j_rank = rank(J);
    [~,n] = size(J);
    if j_rank < n % if rank is less than number of columns its rank deficient
        fprintf('Jacobian is rank deficient \n');
        
        %SVD method for rank deficient jacobian
        [U,S,v] = svd(J);
        
        %partitioning
        U1 = U(:,1:j_rank);
        %U2 = U(:,j_rank:n);

        V1 = v(:,1:j_rank);
        %V2 = V(:,j_rank:n);

        S1 = S(1:j_rank, 1:j_rank);

    % y = J*x
    % y1 = U*(S*(V'*x))
    % y2 = U1*S1*V1;*x
    % p = -V1*inv(S1)*U1'*pe(:) + V2*z2;
    p = -V1*(S1\(U1'*pe(:))); %assume 2-norm solution where z2 = 0;
    else
        %run as normal if full column rank
        %Compute the search direction p
        p = -J\pe(:);
    end
    
    %Report the progress
    fprintf('Iteration Number = %5i, 'i);
    fprintf('Cost = %10.2e, ', V);
    fprintf('Newton Decrement = %10.2e, ', p'*g);
    
    %stop if we meet the gradient norm criteria
    if abs(p'*g) < opt.gradTol
        break;
    end
    
    %Look for a step-length
    alp = 1;
    
    %Check for the second Wolfe condition
    for k = 1:52
        Vn = feval(fcn,y,u,theta + alp*p,n);
        
        if isfinite(Vn) && (Vn < V + c1*alp*p, n);
            break;
        end
        
        alp = alp/2;
    end
    
    %Take step
    theta = theta + alp*p;
    
    fprintf('Step length = %10.2e\n', alp);
end
fprintf('\n');

