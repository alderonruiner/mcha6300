% Lab 6 - Quasi-Newton methods
clc;
clear;
close all;

%% 
f = @(x,y) x*exp(-x^2 - y^2) + 0.05*(x^2 + y^2);
func = @(x) fcn4(x);
init = [0.8;0.001];
pars.delta = 0.5;
pars.delta_bar = 1e3;
xOpt = sr1Trust(func, init, pars)

