function [f,g,H] = fcn5a(z,pars)

x = z(1,:);
y = z(2,:);

f = x.*exp(-x.^2-y.^2)+(x.^2+y.^2)/20;
g = [exp(-x.^2-y.^2) - 2*x.^2.*exp(-x.^2-y.^2) + 2*x/20;
   -2*x.*y.*exp(-x.^2-y.^2) + 2*y/20];

dgdx = [-2*x*exp(-x.^2-y.^2) - (4*x*exp(-x.^2-y.^2) - 4*x^3.*exp(-x.^2-y.^2)) + 2/20;
    -2*y.*exp(-x.^2-y.^2) + 4*x^2*y*exp(-x.^2-y.^2)];

dgdy = [-2*y*exp(-x.^2-y.^2) + 4*y*x.^2.*exp(-x.^2-y.^2);
    -2*x*exp(-x.^2-y.^2) + 4*x.*y^2.*exp(-x.^2-y.^2) + 2/20];H = [dgdx dgdy];