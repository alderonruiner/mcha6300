function L = chol6200(A)

%check if symmetric
flag = issymmetric(A);
if flag == 1
    fprintf("Matrix is symmetric \n")
    eigen = eig(A);
    
    flag2 =  all(eigen > 0);
    
    if  flag2 == 1
    fprintf("Matrix is semi-positive definite \n")
    
    %Cholesky routine
    %https://www.mathworks.com/matlabcentral/answers/254931-why-is-the-built-in-cholesky-function-so-much-faster-than-my-own-implementation
    n = length(A);
    L = zeros(n,n);
    for i=1:n
        L(i,i) = sqrt(A(i,i) - L(i,:)*L(i,:)'); % L*L' - vectorised version of sum(L^2)
        for j = (i+1):n
            L(j,i) = (A(j,i) - L(i,:)*L(j,:)')/L(i,i);% L*L' - vectorised version of sum(L*L')
        end
    end
    
    else 
    fprintf("Matrix is not semi-positive definite \n")
    
    end
else 
   fprintf("Matrix is not symmetric \n")
    L = NaN;
end
end
