%% 
clear
clc

load('lab1_data.mat')

N = 8;

A = vandermonde(x,N);
b = y';

th1 = A\b

H = A'*A;
d = A'*b;

C = chol6200(H);

th2 = C'\(C\d)

hold on;
scatter(x,y);
plot(-6:.1:6,polyval(fliplr(th1'),-6:.1:6));
plot(-6:.1:6,polyval(fliplr(th2'),-6:.1:6));
hold off;

function A = vandermonde(x, n)
x = x(:);  % Column vector
A = ones(length(x), n);
for i = 2:n
  A(:, i) = x .* A(:, i-1);
end
end