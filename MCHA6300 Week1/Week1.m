clc;
clear;
%% unknown function
x = 1;
fcnUnknown(10);

%% Least squares lecture


%% ENGG6300 Week 1
% LEAST SQUARES WITH CHOLESKY
% is polynomial in form y(x) = a0 + a1*x + a2*x^2 + ... an*x^n
% estimate parameters
% implement least squares

load lab1_data.mat
figure(1)
scatter(x,y)
fun = @(a) a(1) + a(2)*x + a(3)*x^2;
x0 = [-6,2];
% lsqnonlin(fun,x0);

%% a. Write a Cholesky Factor Routine
% A = [4 12 -16; 
%     12 37 -43; 
%     -16 -43 98];
% tri = chol6200(A)
% m = chol(A)

%% b. Assuming n = 5, form coefficient matrix A and right hand side vector b
% for least squares problem in 1.1
n = 5;
X = [ones(length(x),1) (x)' (x.^2)' (x.^3)' (x.^4)']; %A is m x n 
Y = y'; %b
theta = X\Y
%x = (A'*A)\(A'*b)
 
%% c. For matrix H=A'*A and vector  d = A'*b. Compute the Cholesky factor of H =
% C'*C using your Cholesky routine. Solve for theta using theta = C\(C'\d);
H = X'*X;
C = chol6200(H);
d = X'*Y;
theta = C'\(C\d)

%% d. Compute the predicted y values using your estimated theta and plot this against
% the measured y 
x_p = linspace(-n,n,length(x));
y_p = theta(1) + theta(2)*x_p + theta(3)*x_p.^2 + theta(4)*x.^3 + theta(5)*x.^4;
figure(2)
scatter(x_p,y_p)

%% e. Experiment with the order n until predicted and measured values are close
n = 8;
A = fliplr(vander(x'));
A = A(:,1:n);
b=y';
theta1 = A\b;
H = A'*A;
d = A'*b;
C = chol6200(H);
theta2 = C'\(C\d);
scatter(x,y)
hold on
plot(-6:.1:6,polyval(fliplr(theta2'), -6:.1:6));
hold off

