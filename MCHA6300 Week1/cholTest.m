function tests = cholTest
tests = functiontests(localfunctions);
end

function testChol6200(testCase)
A = [4 12 -16; 
    12 37 -43; 
    -16 -43 98];

actual_L = chol6200(A);
expected_L = chol(A, 'lower');
assertEqual(testCase, actual_L, expected_L, 'AbsTol', 10*eps, 'Expected your cholesky to match');
end