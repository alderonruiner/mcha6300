function [x, lm] = quadProgIPc(H, f, L, k, A, b, disp)
% Predefining variables for initial conditions
[m, ~] = size(L);
[m_eq, ~] = size(A);
[n, ~] = size(H);
inp_vec = [reshape(H,[1,numel(H)]),reshape(f,[1,numel(f)]),reshape(L,[1,numel(L)]),reshape(k,[1,numel(k)]),reshape(A,[1,numel(A)]),reshape(b,[1,numel(b)])];
% Initial Conditions
% eta_inf = max(abs([H(:); f(:); L(:); k(:); A(:); b(:)]));
eta_inf = max(abs(inp_vec));
e = ones(m, 1);
gamma = 0.01;

x = zeros(n,1);
y = zeros(m_eq,1);
z = sqrt(eta_inf)*e;
s = sqrt(eta_inf)*e;

% Set exit condition variables
mu_c = 0;
% mu = 1;
e_r = 10e-8;
e_c = 10e-8;
e_mu = 10e-8;
thetap = inf;
thetad = inf;
mu = z'*s/m;
itt = 0;

while (mu>1e-6)
    itt = itt + 1;
    Z = diag(z);
    S = diag(s);
    
    % Check for empty matrices
    if (isempty(A) == 1) && (isempty(L) ~= 1)
        Ap = [H L' zeros(n, m);
              L  zeros(m, m) eye(m);
              zeros(m, n) S Z];
    end
    if (isempty(L) == 1) && (isempty(A) ~= 1)
        Ap = [H A';
              A zeros(m_eq, m_eq)];
    end  
    if (isempty(L) == 1) && (isempty(A) == 1)
        Ap = H;
    end
    if (isempty(L) ~= 1) && (isempty(A) ~= 1)
        Ap = [H A' L' zeros(n, m);
              A zeros(m_eq, m_eq) zeros(m_eq, m)  zeros(m_eq, m);
              L zeros(m, m_eq) zeros(m, m) eye(m);
              zeros(m, n) zeros(m, m_eq) S Z];
    end


    r4 = mu_c*e - Z*S*e;
    r1 = -H*x - f;
    r2 = b;
    r3 = k - L*x - s;
    if m > 0 
        r1 = r1 - L'*z;
    end
    
    if m_eq > 0
        r1 = r1 - A'*y;
        r2 = r2 - A*x;
    end
    
    yp = [r1; r2; r3; r4];
    bp = Ap\yp;
     
    % Extract the delta values from the matrix
    delta_xp = bp(1:n);
    delta_yp = bp(n+1: n+m_eq);
    delta_zp = bp(n+m_eq+1: n+m_eq+m);
    delta_sp = bp(n+m_eq+m+1:end);

    % Define the distance to boundary, alpha, along the predictor direction
    alphap = 1/max([max(-delta_zp./z), max(-delta_sp./s), 1]);
    
    % Define the adaptive 'centering' parameter
    mu = (z'*s)/m;
    sigmap = (abs(1/m*(z + alphap*delta_zp)'*(s + alphap*delta_sp) - mu_c)/mu)^3;
    
    % Obtain the combined 'centering-corrector' direction
    del_Z = diag(delta_zp);
    del_S = diag(delta_sp);

    ycc = [zeros(n, 1); 
           zeros(m_eq, 1); 
           zeros(m, 1); 
          (mu_c + sigmap*mu)*e - del_Z*del_S*e];
    bcc = Ap\ycc;
    
    % Extract the delta values from the matrix
    delta_xcc = bcc(1:n);
    delta_ycc = bcc(n+1: n+m_eq);
    delta_zcc = bcc(n+m_eq+1: n+m_eq+m);
    delta_scc = bcc(n+m_eq+m+1:end);
    
    % Define the distance to the boundary along the combined vector
    alphapcc = 1/max([max(-delta_zp - delta_zcc./z), max(-delta_sp - delta_scc./s), 1]);
    
    % Define a scaling parameter
    sigmapcc = (abs(1/m*(z + alphapcc*(delta_zp + delta_zcc))'*(s + alphapcc*(delta_sp + delta_scc)) - mu_c)/mu)^3;
    
    % Solve again to obtain predictor correction
    if sigmapcc < sigmap
        delta_x = delta_xp + delta_xcc;
        delta_y = delta_yp + delta_ycc;
        delta_z = delta_zp + delta_zcc;
        delta_s = delta_sp + delta_scc;
    else
        
        r4i = mu_c*e + sigmap*mu - Z*S*e;
        yi = [r1; r2; r3; r4i];
        bi = Ap\yi;
        
        % Extract the delta values from the matrix
        delta_x = bi(1:n);
        delta_y = bi(n+1: n+m_eq);
        delta_z = bi(n+m_eq+1: n+m_eq+m);
        delta_s = bi(n+m_eq+m+1:end);
    end
    
    % Obtain the final step length 
    alpha_z = 1/(max(1, max(-delta_z./z)));
    alpha_s = 1/(max(1, max(-delta_s./s)));
    [~, j_z] = max(-delta_z./z);
    [~, j_s] = max(-delta_s./s);
    
    alpha_f = min([alpha_z, alpha_s, 1]);
    mu_f = abs(1/m*(z + alpha_f*delta_z)'*(s + alpha_f*delta_s) - mu_c);
    
    if (min(alpha_z, alpha_s) >= 1)
        alpha = 1;
    elseif (alpha_z < alpha_s)
        alpha = ((gamma*mu_f)/(s(j_z) + alpha_f*delta_s(j_z)) - z(j_z))/delta_z(j_z);
    else
        alpha = ((gamma*mu_f)/(s(j_s) + alpha_f*delta_s(j_s)) - z(j_s))/delta_z(j_s);
    end
    
    % Set alpha
    if alpha < (1 - gamma)*alpha_f
        alpha = (1 - gamma)*alpha_f;
    else
        alpha = (1 - gamma)*alpha;
    end
    
    % Form the new iterates
    x = x + alpha*delta_x;
    y = y + alpha*delta_y;
    z = z + alpha*delta_z;
    s = s + alpha*delta_s;
    
    mu = (z'*s)/m;
    
    lm.inequality = z;
    lm.equality = y;
    
     % Check termination conditions    
    theta = max(abs([r1(:); r2(:); r3(:)]))/eta_inf;
    gamma_k = max(abs([Z*S*e - mu_c]));
    
    if ((theta <= e_r) && (abs(mu - mu_c) <= e_mu) && (gamma_k <= e_c))
        break;
    end
    thetap_old = thetap;
    thetad_old = thetad;
    
    thetap = max(abs([r2(:); r3(:)]))/eta_inf;
    thetad = max(abs(r1(:)))/eta_inf;
    
    if (thetap > e_r && thetap >= thetap_old)
        break;
    
    
    elseif (thetad > e_r && thetad >= thetad_old)
        break;
    
    
    elseif (itt > 1000)
        break;
    else
    end
    
 
end
end

