clc;
clear;

H   = 1;
f   = 1;
L   = [-1;1];
k   = [0.5;1];
A   = [];
b   = [];
disp = 0;
[U, lm, err] = quadProgIP(H,f,L,k,A,b,disp); 
[u] = quadProgIPc(H,f,L,k,A,b,disp); 