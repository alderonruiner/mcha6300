//mex -O quadProIP.c -lmwblas -lmwlapack
#define varint mwSignedIndex
#include "mex.h"
#include "math.h"
#include "blas.h"

typedef struct{
	varint nx; //num of columns
    varint ny; //num rows
	double *H; //Hessian
	double *f; 
	double *L; //Inequality constraints
	double *k; //Inequality constraints
	double *A; //Equality constraints
	double *b; //Equality constraints
}zData;

/* Define c function */
void QP
 double *H = zdat->H;
 double *f = zdat->f;
 double *L = zdat->L;
 double *k = zdat->k;
 double *A = zdat->A;
 double *b = zdat->b;
 double *x = zdat->x;
 double *error = zdat->error;
 double *y = zdat->y;
 double *z = zdat->z;
 varint n = zdat->n;
 varint disp = zdat->disp;

 
 //// CHECK FOR INVALID CONSTRAINTS?
 
 
 //initialise counting integers
 int i,j;
 
//initialise matrices
double *H = malloc((n*n)*sizeof(double));
double *f = malloc((n)*sizeof(double));
double *L = malloc((n)*sizeof(double));
double *k = malloc((n)*sizeof(double));
double *A = malloc((n)*sizeof(double));
double *b = malloc((n)*sizeof(double));


double *r1 = malloc(()*sizeof(double));
double *r2 = malloc(()*sizeof(double));
double *r3 = malloc(()*sizeof(double));
double *r4 = malloc(()*sizeof(double));
double *delta = malloc(()*sizeof(double));
double *delta_xp = malloc(()*sizeof(double));
double *delta_yp = malloc(()*sizeof(double));
double *delta_zp = malloc(()*sizeof(double));
double *delta_sp = malloc(()*sizeof(double));
double *minVec = malloc(()*sizeof(double));
double *maxVec = malloc(()*sizeof(double));
double *maxVec2 = malloc(()*sizeof(double));
double *alpha_p = malloc(()*sizeof(double));
double *alpha_pcc = malloc(()*sizeof(double));
double *alpha_f = malloc(()*sizeof(double));
double *alpha = malloc(()*sizeof(double));
double *sigma = malloc(()*sizeof(double));
double *sigma_pcc = malloc(()*sizeof(double));
double *Z = malloc(()*sizeof(double));
double *S = malloc(()*sizeof(double));
double *delta_Zp = malloc(()*sizeof(double));
double *delta_Sp = malloc(()*sizeof(double));
double *r4cc = malloc(()*sizeof(double));
double *delta_cc = malloc(()*sizeof(double));
double *delta_xcc = malloc(()*sizeof(double));
double *delta_ycc = malloc(()*sizeof(double));
double *delta_zcc = malloc(()*sizeof(double));
double *delta_scc = malloc(()*sizeof(double));
double *delta_x = malloc(()*sizeof(double));
double *delta_y = malloc(()*sizeof(double));
double *delta_z = malloc(()*sizeof(double));
double *delta_s = malloc(()*sizeof(double));
double *e = malloc(()*sizeof(double));
double *mu_f = malloc(()*sizeof(double));
double *x = malloc(()*sizeof(double));
double *y = malloc(()*sizeof(double));
double *z = malloc(()*sizeof(double));
double *s = malloc(()*sizeof(double));
double *sM = malloc(()*sizeof(double));

//initialise various things
double eps_c = 0.00000001;
double eps_r = 0.00000001;
double eps_mu = 0.00000001;
int iteration = 0;
double gamma = 0.01;
int upper_lim - 1000;
double mu_c = 0.0;
double tol = 0.000001;
int m;
double max1;
double max2;
double max3;
double max4;
int AIZ;
int AIS;
int j_s;
int j_z;
int err = 0;
double alpha_z;
double alpha_s;
double eta_inf = 0x7F800000;
double vpk = 0x7F800000;
double vdk = 0x7F800000;
double vpk_old;
double vdk_old;
double vk;
double gamma_k;


//initialiase chars
const char T = 'T';
const char U = 'U';
const char n = 'N';

double alp = 1.0, beta = 1.0;
 
// mu = z'*s/M
dgemm(&n,&n,&nx,&nu,&nx,&alp, s, &nx, M, &nx, &beta, sM,&nx); 
dtrsv(&U, &T, &n, &ny, cal_R22, &ny, zz, &ny); 

/*------------------------------Algorithm-------------------------------------*/
while(mu>tol){
	iteration = iteration +1; //keep count of iteration
	
	//Z = diag(z)
	//S = diag(s)
	
	//THIS WON'T WORK ON 1-D ARRAYS
	for(i=0;i<NUMBER;++i){
		for (j=0;j<NUMBER;++j){
			if(j==i){
				Z[i] = z[i]; 
			}
		}
	}
	
	
	//Populate matrix_p
	/*matrix_p = [H A' L' zeros(N,M);
                    A zeros(P,P)  zeros(P,M)  zeros(P,M);
                    L zeros(M,P)  zeros(M,M)  eye(M,M);
                    zeros(U,N) zeros(U,P)  S  Z];*/
					
					
	//Find r1, r2, r3, r4
	
	//Find delta, delta_xp, delta_yp, delta_zp, delta_sp
	
	//Find max1, max2, maxVec, alpha_p
	
	//mu = z'*s/m
	//sigma = ((abs((1/m)*(z+alpha_p*delta_zp)'*(s + alpha_p*delta_sp)-mu_c))/mu)^3
	
	//delta_Zp = diag(delta_zp);
    //delta_Sp = diag(delta_sp);
    //r4cc = (mu_c + sigma*mu)*e - delta_Zp*delta_Sp*e;
	
	//delta_cc = matrix_p\[zeros(N,1);zeros(P,1);zeros(M,1);r4cc];
	//delta_xcc = delta_cc(1:N,:);
    //delta_ycc = delta_cc(N+1:N+P,:);
    //delta_zcc = delta_cc(N+P+1:N+P+M,:);
    //delta_scc = delta_cc(N+P+M+1:end,:);
	
	//max3 = max((-delta_zp - delta_zcc)/z);
    //max4 = max((-delta_sp - delta_scc)/s);
    //maxVec2 = [max3';max4';1];
    //alpha_pcc = 1/(max(maxVec2));
	
	//sigma_pcc = ((abs((1/m)*(z+alpha_pcc*(delta_zp+delta_zcc))'*(s + alpha_pcc*(delta_sp + delta_scc)) - mu_c))/mu)^3;
	
	if (sigma_pcc <= sigma){
        //delta_x = delta_xp + delta_xcc;
        //delta_y = delta_yp + delta_ycc;
        //delta_z = delta_zp + delta_zcc;
        //delta_s = delta_sp + delta_scc;
		for(i=0;i<NUMBER;++i){
			delta_x[i] = delta_xp[i] + delta_xcc[i];
			delta_y[i] = delta_yp[i] + delta_ycc[i];
			delta_z[i] = delta_zp[i] + delta_zcc[i];
			delta_s[i] = delta_sp[i] + delta_scc[i];
		}
	}
	
	else{
        //r4i = mu_c*e - sigma_pcc*mu - Z*S*e;
        //delta = matrix_p\[r1;r2;r3;r4i];
        //delta_x = delta(1:N,:);
        //delta_y = delta(N+1:N+P,:);
        //delta_z = delta(N+P+1:N+P+M,:);
        //delta_s = delta(N+P+M+1:end,:);
	}
	
	AIZ = 0;
	AIS = 0;
	
	/*for j = 1: length(z)
            temp = max(1,-delta_z(j)/z(j));
            if temp > AIZ
                AIZ = temp;
                j_z = j;
            end
            temp = max(1,-delta_s(j)/s(j));
            if temp > AIS
                AIS = temp;
                j_s = j;
            end
    end*/
    alpha_z = 1/double(AIZ);
    alpha_s = 1/double(AIS);
	
	//minVec= [alpha_z'; alpha_s';1];
    //alpha_f = min(minVec);
    //mu_f = abs((1/m)*(z + alpha_f*delta_z)'*(s + alpha_f*delta_s) - mu_c);

	/*if (min(alpha_z,alpha_s)>=1)
        alpha = 1;
    elseif(alpha_z<alpha_s)
        alpha = ((gamma*mu_f)/(s(j_z) + alpha_f*delta_s(j_z)) - z(j_z))/(delta_z(j_z));
    else 
        alpha = ((gamma*mu_f)/(z(j_s) + alpha_f*delta_z(j_s)) - s(j_s))/(delta_s(j_s));
    end
    
    if(alpha < ((1-gamma)*alpha_f))
        alpha = ((1-gamma)*alpha_f);
    else
        alpha = (1-gamma)*alpha;
    end*/
	
	/*x = x + alpha*delta_x;
    y = y + alpha*delta_y;
    z = z + alpha*delta_z;
    s = s + alpha*delta_s;
    mu = z'*s/M;*/
	
	/*gamma_k = max(abs((Z*S*e - mu_c)));
    vk = (max(abs([r1(:);r2(:);r3(:)]))/eta_inf);
    vpk_old = vpk;
    vpk = max(abs([r2(:);r3(:)]))/eta_inf;
    vdk_old = vdk;
    vdk = max(abs(r1(:)))/eta_inf ;*/
	
	if ((vk <= eps_r) && (abs(mu - mu_c)<eps_mu) && (gamma_k <= eps_c)){
        err = 0;
        break;
	}
    else if( vpk > eps_r && vpk >= vpk_old){
        err = 1;
        break;
	}
    else if(vdk > eps_r && vdk >= vdk_old){
        err = 2;
        break;
	}
    else if(iteration > upper_lim){
        err = 3;
        break;
	}
    else{
	}
    
}

//lm.inequality = z;
//lm.equality = y;

///////////////////////////////////// MEX FUNCTION /////////////////////////////////////
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Variables are assumed in the following order */
  double *H, *f, *L, *k, *A, *b;
  varint disp;
  
  /* Internal variables */
  varint j, i, n, mc, me;

  double *x, *y, *error, *z;
  mxArray *tmp;
  
  if(nrhs < 2){
    mexPrintf("\nERROR: There must be at least two inputs.\n\n");
	plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[1] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[2] = mxCreateDoubleMatrix(0,0,mxREAL);
    return;
  }

  /*Get size of H and f and run some checks*/
  n = mxGetM(prhs[0]);
  
	/*Check that n is not equal to zero */
  if(n*mxGetN(prhs[0])==0){
	  plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
	  plhs[1] = mxCreateDoubleMatrix(0,0,mxREAL);
	  plhs[2] = mxCreateDoubleMatrix(0,0,mxREAL);
	  return;
  }
  
  /*Check that H is square*/
  if(n!=mxGetN(prhs[0])){
    mexPrintf("\nERROR: The H matrix must be square.\n\n");
	plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[1] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[2] = mxCreateDoubleMatrix(0,0,mxREAL);
    return;
  }

  /*Check that f has as many entries as row/column dimension of H*/
  if(n!=mxGetM(prhs[1])){
    mexPrintf("\nERROR: The f column vector must have %i entries.\n\n",n);
	plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[1] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[2] = mxCreateDoubleMatrix(0,0,mxREAL);
    return;
  }
  
  /*Get pointers to H and f*/
  H = mxGetPr(prhs[0]);
  f = mxGetPr(prhs[1]);
  
  /*Check that both L and k are supplied*/
  if(nrhs>2 && nrhs<4){
    mexPrintf("\nERROR: Both L and k must be specified.\n\n",n);
	plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[1] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[2] = mxCreateDoubleMatrix(0,0,mxREAL);
    return;
  }
  
  /*Get number of inequality constraints*/
  if(nrhs>2){mc=mxGetM(prhs[2]);}else{mc=0;}

  /*Check that L has as many columns as H has rows/columns*/
  if(mc>0 && mxGetN(prhs[2])!=n){
    mexPrintf("\nERROR: The L matrix must have %i columns.\n\n",n);
	plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[1] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[2] = mxCreateDoubleMatrix(0,0,mxREAL);
    return;
  }
  
  /*Check that L and k have the same number of rows*/
  if(mc>0 && mc!=mxGetM(prhs[3])){
    mexPrintf("\nERROR: The k column vector must have %i entries.\n\n",mc);
	plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[1] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[2] = mxCreateDoubleMatrix(0,0,mxREAL);
    return;
  }
  
  /*Get pointers to L and k*/
  if(mc>0){
    L = mxGetPr(prhs[2]);
    k = mxGetPr(prhs[3]);
  }
  
  /*Check that both A and b are supplied*/
  if(nrhs>4 && nrhs<6){
    mexPrintf("\nERROR: Both A and b must be specified.\n\n",n);
	plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[1] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[2] = mxCreateDoubleMatrix(0,0,mxREAL);
    return;
  }
  
  /*Get number of equality constraints*/
  if(nrhs>4){me=mxGetM(prhs[4]);}else{me=0;}
  
  /*Check that A has as many columns as H has rows/columns*/
  if(me>0 && mxGetN(prhs[4])!=n){
    mexPrintf("\nERROR: The A matrix must have %i columns.\n\n",n);
	plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[1] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[2] = mxCreateDoubleMatrix(0,0,mxREAL);
    return;
  }

  /*Check that A and b have the same number of rows*/
  if(me>0 && me!=mxGetM(prhs[5])){
    mexPrintf("\nERROR: The b column vector must have %i entries.\n\n",me);
	plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[1] = mxCreateDoubleMatrix(0,0,mxREAL);
	plhs[2] = mxCreateDoubleMatrix(0,0,mxREAL);
    return;
  }
  
  /*Get pointers to A and b */
  if(me>0){
    A = mxGetPr(prhs[4]);
    b = mxGetPr(prhs[5]);
  }

  /* Get display */
  if(nrhs>6){
    if(mxGetM(prhs[6])*mxGetN(prhs[6])>0){disp=(int)(((double *)mxGetPr(prhs[6]))[0]);}
    else{disp=0;}
  }else{disp=0;}


  plhs[0] = mxCreateDoubleMatrix(n,1,mxREAL);
  x = mxGetPr(plhs[0]);   //output?
  plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);
  error = mxGetPr(plhs[1]); //error output?

  plhs[2] = mxCreateStructMatrix(1, 1, 0, 0);
  mxAddField(plhs[2],"equality");
  tmp = mxCreateDoubleMatrix(me,1,mxREAL);
  y = mxGetPr(tmp); mxSetField(plhs[2],0,"equality",tmp); //langrange multiplier?
  mxAddField(plhs[2],"inequality");
  tmp = mxCreateDoubleMatrix(mc,1,mxREAL);
  z = mxGetPr(tmp); mxSetField(plhs[2],0,"inequality",tmp); //lagrange multiplier?
	
  zData zdat;
  zdat.H = H;
  zdat.f = f;
  zdat.L = L;
  zdat.k = k;
  zdat.A = A;
  zdat.b = b;
  zdat.x = x;
  zdat.disp = disp;
  zdat.error = error;
  zdat.y = y;
  zdat.z = z;
  zdat.n = n;

  //Call you function here
  // error[0]= YOUR_FUNCTION(??????)
  QP(&zdat);
}
