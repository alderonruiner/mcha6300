function [Q,R] = givensQR(A)
[m,n] = size(A);
R = A;
Q = eye(m);

  for j = 1:n
    for i = m:-1:(j+1)
      G = eye(m);
      [c,s] = givensRot( R(i-1,j),R(i,j) );
      G([i-1, i],[i-1, i]) = [c -s; s c];
      R = G'*R;
      Q = Q*G;
    end
  end
end



