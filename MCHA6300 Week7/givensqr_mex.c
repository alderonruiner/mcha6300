#include "mex.h"
#include "math.h"
#include "stdio.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    int i,j;
    int m = mxGetM(prhs[0]); //mxGetM - number of rows in mxArray
    int n = mxGetN(prhs[0]); //mxGetN - number of columns in mxArray
    
   //Inputs?
   double *A = mxGetPr(prhs[0]); //pointer to matrix
   
   //something to allocate memory?
   double *w = malloc(n*sizeof(double));
   
   //create outputs?
   plhs[0] = mxCreateDoubleMatrix(m,n,mxREAL);
   plhs[1] = mxCreateDoubleMatrix(m,n,mxREAL);
   
   
   // Givens Rotation Routine
   double R,c,s,r,b,a;
   int Q[m][m],n,i,j;
   int G[m][n];
   R = &A;
 
   //Q = eye(m) hahahaha very compact
    for(i=0;i<n;i++)
        {
        for(j=0;j<n;j++)
        {
        if(i==j)
         {
         Q[i][j]=1;
            }
        else
         {
        Q[i][j]=0;
            }
        }
    }
   
   // Start the loop
   for(j=0;j<n:j++)
   {
       for(i=0;i<(j+1):i--)
       {
           
           //Set G = eye(m), this is going to get old fast
               for(i=0;i<n;i++)
                  {
                    for(j=0;j<n;j++)
                    {
                    if(i==j)
                     {
                     G[i][j]=1;
                        }
                    else
                     {
                    G[i][j]=0;
                        }
                    }

                    //Do the rotato [c,s] = givensRot( R(i-1,j),R(i,j)), praying sqrt and abs were still a thing in the 1800s
                    // can use BLAS xROTG 
                    if(b == 0){
                        c = 1;
                        s = 0;}
                      else{
                        if(abs(b) > abs(a)){
                          r = a / b;
                          s = 1 / sqrt(1 + r^2);
                          c = s*r;}
                        else{
                          r = b / a;
                          c = 1 / sqrt(1 + r^2);
                          s = c*r;}
                      }
                    
                    //assign rotations to G?
                    G[i-1, i][i-1, i] = [c -s; s c]
                    //R = G'*R;
                    //dgemm("N", "N", &m, &n, &p, &alpha, A, &m, B, &p, &beta, C, &m) = alpha*A*B + beta*C <-- this is meaningless
                    R = dgemm(G,R)
                    //Q = Q*G;
                    Q = dgemm(Q,G)
                }
       }
   }

   plhs[0] = Q;
   plhs[1] = R;
   
   //free memory
    free(w);

}

